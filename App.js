import React from 'react';
import { View, Text, ImageBackground, StatusBar } from 'react-native';
import * as Font from 'expo-font';
import { Updates, SplashScreen, AppLoading } from 'expo';
import { Provider } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import MAINAPP from './src/MainApp';
//import Sentry from 'sentry-expo';
//Remove this once Sentry is correctly setup.
//Sentry.enableInExpoDevelopment = true;

//Sentry.config(
// 'https://790cd164db41427fb8d9a2ce774e372c@sentry.io/1487959'
// ).install();

import {
  widthPercentageToDP,
  heightPercentageToDP
} from './src/modules/MakeMeResponsive';
//import Store from "./src/redux/UnpresistStore";
import { Store, persistor } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';
//======================================================
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { Loaded: false, renderUpdate: false, dots: 1 };
  }
  UNSAFE_componentWillMount() {
    this.StartInterval();
    this._CheckingUpdate().then(() => {
      this._loadAssetsAsync().then(() => {
        this.setState({ Loaded: true });
      });
    });
  }

  componentWillUnmount() {
    this.EndInterval();
  }

  StartInterval = () => {
    this.TIMER = setInterval(() => {
      const { dots } = this.state;
      if (dots === 1) {
        this.setState({ dots: 2 });
      } else if (dots === 2) {
        this.setState({ dots: 3 });
      } else {
        this.setState({ dots: 1 });
      }
    }, 500);
  };
  
  EndInterval = () => {
    if (this.TIMER) clearInterval(this.TIMER);
  };
  _CheckingUpdate = async () => {
    try {
      const update = await Updates.checkForUpdateAsync();
      if (update.isAvailable) {
        this.setState({ renderUpdate: true });
        await Updates.fetchUpdateAsync().then(() => {
          this.EndInterval();
          Updates.reloadFromCache();
        });
      } else {
        this.setState({ renderUpdate: false });
        this.EndInterval();
      }
    } catch (e) {
      // handle or log error
    }
  };
  _loadAssetsAsync = async () =>
    await Font.loadAsync({
      Roboto: require('./assets/fonts/Roboto-Regular.ttf'),
      Arial: require('./assets/fonts/ARIALI.ttf'),
      Roboto_medium: require('./assets/fonts/Roboto-Medium.ttf'),
      space_mono: require('./assets/fonts/SpaceMono-Regular.ttf'),
      Metropolis_Regular: require('./assets/fonts/Metropolis_Regular.otf'),
      Metropolis_SemiBold: require('./assets/fonts/Metropolis_SemiBold.otf'),
      ...Ionicons.font
    });

  render() {
    const { renderUpdate, Loaded, dots } = this.state;
    return (
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(100)
        }}
      >
        {Loaded ? (
          <Provider store={Store}>
            <PersistGate loading={null} persistor={persistor}>
              <MAINAPP />
            </PersistGate>
          </Provider>
        ) : (
          <ImageBackground
            style={{
              flex: 1,
              resizeMode: 'cover',
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(100)
            }}
            resizeMode={'cover'}
            source={require('./assets/images/splash.png')}
          >
            {renderUpdate === true && (
              <View
                style={{
                  bottom: heightPercentageToDP(10),
                  left: widthPercentageToDP(10),
                  borderRadius: widthPercentageToDP(15),
                  width: widthPercentageToDP(80),
                  height: heightPercentageToDP(8),
                  backgroundColor: '#fff',
                  elevation: 4,
                  position: 'absolute',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: widthPercentageToDP(4)
                  }}
                >
                  {'Updating '}
                  {dots === 1 ? '.' : dots === 2 ? '..' : '...'}
                </Text>
              </View>
            )}
          </ImageBackground>
        )}
      </View>
    );
  }
}
