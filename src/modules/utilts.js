import { Share, Image, Platform } from 'react-native';
import * as StoreReview from 'expo-store-review';
import { parsePhoneNumber } from 'libphonenumber-js';
//===================================================
//use it for developer Mode
const developerMode = false;
//===================================================
const EMAIL_FORMAT = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
const NAME_FORMAT = /^[A-Za-z\s]+$/;
const EmailValidator = email => {
  return EMAIL_FORMAT.test(email) ? true : false;
};
const NameValidator = name => {
  return NAME_FORMAT.test(name) ? true : false;
};
const phoneNumberValidator = phoneNumber => {
  let internationalFormatcell = '',
    valid = false;
  try {
    const numberWeWant = parsePhoneNumber(phoneNumber, 'PK');
    valid = numberWeWant.isValid();
    internationalFormatcell =
      valid === true ? numberWeWant.formatInternational() : '';
  } catch (e) {
    internationalFormatcell = '';
    valid = false;
  }
  return {
    internationalFormatcell: internationalFormatcell.replace(/[a-zA-Z+ ]/g, ''),
    valid
  };
};
const numberFormator = num => {
  //const formatedNumber=num.replace(/[a-zA-z+ ]/g, "");
  const formatedNumber = num.replace('+', '');
  return formatedNumber.replace(/\s+/g, '');
};
const prefetchImage = URL => {
  if (URL !== undefined && URL != '') {
    Image.prefetch(URL);
  }
};
const makeWorkingHtml = partialHtml =>
  `<html><body>${partialHtml.replace(/\n/g, '')}</body></html>`;

const monthNames = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];
const containsObject = async (id, array) => {
  let i,
    { length } = array;
  for (i = 0; i < length; i++) {
    if (array[i].variant._id === id) {
      return i;
    }
  }
  return -1;
};
const getCartItemQuantity = Arr => {
  let Totalitem = 0;
  if (Arr.length)
    Arr.map((item, index) => {
      Totalitem = Totalitem + item.quantity;
    });
  return Totalitem;
};
const GetTotalPrice = Arr => {
  let TotalPrice = 0;
  if (Arr.length)
    Arr.map((item, index) => {
      TotalPrice = TotalPrice + item.quantity * item.Price;
    });
  return TotalPrice;
};
const checkWishlistItemExist = (wishlistArray, itemToBeCheck) => {
  let index = 0,
    foundAt = -1;
  const { length } = wishlistArray;
  const { product, variant } = itemToBeCheck;
  if (length !== 0)
    for (index; index < length; index++) {
      if (
        wishlistArray[index].product._id === product._id &&
        wishlistArray[index].variant._id === variant._id
      ) {
        foundAt = index;
        break;
      }
    }

  return foundAt;
};
const sorts = [
  { _id: 'Relevance', name: 'No order', val: null },
  { _id: 'Whats New', name: 'New first', val: '-createdAt' },
  {
    _id: 'Price low to high',
    name: 'Price low to high',
    val: 'variants.price'
  },
  {
    _id: 'Price high to low',
    name: 'Price high to low',
    val: '-variants.price'
  }
];
const isEmpty = obj => {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};
const stringMaker = AppliedFilters => {
  let FilterString = '';
  for (let key in AppliedFilters) {
    if (AppliedFilters.hasOwnProperty(key)) {
      if (key === 'Sort') {
        if (AppliedFilters[key].content)
          FilterString = '&sort=' + AppliedFilters[key].content;
      } else if (key === 'price') {
        FilterString =
          FilterString + '&' + key + '=' + AppliedFilters[key].content;
      } else {
        if (AppliedFilters[key].content.length)
          FilterString =
            FilterString + '&' + key + '=' + AppliedFilters[key].content;
      }
    }
  }
  return FilterString;
};
const stringMakerForSearch = AppliedFilters => {
  let FilterString = '';
  for (let key in AppliedFilters) {
    if (AppliedFilters.hasOwnProperty(key)) {
      if (key === 'Sort') {
        if (AppliedFilters[key].content)
          FilterString = 'sort=' + AppliedFilters[key].content;
      } else if (key === 'price') {
        FilterString =
          FilterString !== ''
            ? FilterString + '&' + key + '=' + AppliedFilters[key].content
            : key + '=' + AppliedFilters[key].content;
      } else {
        if (AppliedFilters[key].content.length)
          FilterString =
            FilterString !== ''
              ? FilterString + '&' + key + '=' + AppliedFilters[key].content
              : key + '=' + AppliedFilters[key].content;
      }
    }
  }
  return FilterString;
};
const tConvert = time => {
  // Check correct time format and split into components
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [
    time
  ];

  if (time.length > 1) {
    // If time format correct
    time = time.slice(1); // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join(''); // return adjusted time or original string
};
const ExtractDateAndTime = FullDateTime => {
  const Splited = FullDateTime.split('T');
  const dDate = Splited[0];
  const Convert24Clock = Splited[1].substring(0, 8);
  const dTime = tConvert(Convert24Clock);
  return { dDate, dTime };
};

const TimeStatus = STATUS => {
  let dateString = '';

  if (STATUS !== 'Delivered') {
    let dt = new Date();
    if (STATUS === 'Placed') dt.setDate(dt.getDate() + 5);
    else if (STATUS === 'Processing' || STATUS === 'Shipped')
      dt.setDate(dt.getDate() + 3);
    else if (STATUS === 'Dispatched') dt.setDate(dt.getDate() + 2);
    const date = dt.getDate();
    const month = dt.getMonth();
    const year = dt.getFullYear();
    dateString = date + ' ' + monthNames[month];
  }

  return dateString;
};

const firstLetterCapital = string =>
  string.charAt(0).toUpperCase() + string.slice(1);
const onRate = () => StoreReview.requestReview();

const onShare = async () => {
  const LINK =
    Platform.OS === 'android'
      ? 'https://play.google.com/store/apps/details?id=com.clicky.pk'
      : 'https://apps.apple.com/pk/app/clicky-online-shopping/id1448160644';
  try {
    const result = await Share.share({
      title: 'Share with Love',
      message:
        'Hey I have found this exciting app for online shopping in Pakistan\n' +
        LINK,
      dialogTitle: 'SHARE'
    });

    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    alert(error.message);
  }
};
const onShareItem = async ID => {
  const LINK = 'https://www.clicky.pk/itemdetail?id=' + ID;
  try {
    const result = await Share.share({
      title: 'Share with Love',
      message: 'Hey Check this item on Clicky:\n' + LINK,
      dialogTitle: 'SHARE'
    });

    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    alert(error.message);
  }
};

const CategoryExtractor = STR => {
  const res = STR.slice(15);
  const index = res.indexOf('?');
  return index !== -1 ? res.slice(0, index) : res;
};
/*let str = "https://beta.clicky.pk/women-tops-tunics?Color=Black%20Wood%20Red%20Word&page=1";
 const index = str.indexOf("?");
document.getElementById("demo").innerHTML =index !== -1 ? str.slice(index+1, str.length) : "NO";*/
const FilterExtract = STR => {
  const index = STR.indexOf('?');
  return index !== -1 ? STR.slice(index + 1, STR.length) : '';
};
const FilterMaker = (sizes, sort, brands, Color, price) => {
  let Filter = '';
  sizes !== undefined && (Filter = 'sizes=' + sizes);
  sort !== undefined &&
    (Filter = Filter + Filter !== '' ? '&sort=' : 'sort=' + sort);
  brands !== undefined &&
    (Filter = Filter + Filter !== '' ? '&brands=' : 'brands=' + brands);
  Color !== undefined &&
    (Filter = Filter + Filter !== '' ? '&color' : 'color' + Color);
  price !== undefined &&
    (Filter = Filter + Filter !== '' ? '&price=' : 'price=' + price);
  return Filter;
};

const InfoLogger = (title, Detail) => {
  developerMode && console.log(title + ' :\n', Detail);
};
const WarningLogger = (title, Detail) => {
  developerMode && console.warn('WARNING (' + title + ') :\n', Detail);
};
const ErrorLogger = (title, Detail) => {
  developerMode && console.error('ERROR (' + title + ') :\n', Detail);
};
const ExpoTokenExtractor = ExpoToken => {
  let ExtractedET = ExpoToken.replace('ExponentPushToken[', '');
  ExtractedET = ExtractedET.replace(']', '');
  return ExtractedET;
};
const timeLefter = timeto => {
  const timeDiff = new Date(timeto) - Date.now();
  let dateTimeString = '';
  let delta = timeDiff / 1000;
  if (delta > 1) {
    const days = Math.floor(delta / 86400);
    delta -= days * 86400;
    const hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;
    const minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;
    const seconds = delta % 60;
    dateTimeString = days + ' Days ' + hours + ' hrs ' + minutes + ' mins';
  } else {
    dateTimeString = 'Expire';
  }
  return dateTimeString;
};
//console.log(timeLefter('2019-09-21T00:00:00.000Z'));
const validURL = str => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ); // fragment locator
  return pattern.test(str);
};
export {
  makeWorkingHtml,
  sorts,
  isEmpty,
  GetTotalPrice,
  getCartItemQuantity,
  containsObject,
  checkWishlistItemExist,
  stringMaker,
  stringMakerForSearch,
  tConvert,
  ExtractDateAndTime,
  TimeStatus,
  firstLetterCapital,
  onRate,
  onShare,
  onShareItem,
  CategoryExtractor,
  FilterMaker,
  FilterExtract,
  prefetchImage,
  phoneNumberValidator,
  InfoLogger,
  WarningLogger,
  ErrorLogger,
  numberFormator,
  EmailValidator,
  NameValidator,
  ExpoTokenExtractor,
  timeLefter,
  validURL
};
