import { Dimensions, PixelRatio, Platform, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");
let StatusBarShow = false;

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

let isIPhoneX = false;

if (Platform.OS === "ios" && !Platform.isPad && !Platform.isTVOS) {
  isIPhoneX =
    (width === X_WIDTH && height === X_HEIGHT) ||
    (width === XSMAX_WIDTH && height === XSMAX_HEIGHT);
}

const roundToNearestPixel = (Sc, El) =>
  PixelRatio.roundToNearestPixel((Sc * El) / 100);

const widthPercentageToDP = widthPercent =>
  roundToNearestPixel(width, parseFloat(widthPercent));

const heightPercentageToDP = heightPercent => {
  const windowHeightPer = roundToNearestPixel(
    height,
    parseFloat(heightPercent)
  );
  return StatusBarShow === true
    ? windowHeightPer - getStatusBarHeight(false)
    : windowHeightPer;
};

const getStatusBarHeight = skipAndroid =>
  Platform.select({
    ios: isIPhoneX ? 44 : 20,
    android: skipAndroid ? 0 : StatusBar.currentHeight,
    default: 0
  });
const setStatusBarVisibility = SWITCH => {
  StatusBarShow = SWITCH;
};
export {
  widthPercentageToDP,
  heightPercentageToDP,
  getStatusBarHeight,
  setStatusBarVisibility
};
