import AXIOS from 'axios';
import { WarningLogger, InfoLogger } from '../modules/utilts';
const HOST = 'https://app.clicky.pk';
const HostAPI = HOST + '/api';
const MegaCategoryMenu = HostAPI + '/categories/megamenu',
  SubCatContent = HostAPI + '/promotions/search/null?categories=',
  SearchedContent = HostAPI + '/promotions/search/',
  ProductDetail = HostAPI + '/products/',
  SignUp = HostAPI + '/users/',
  SignIn = HostAPI + '/auth/local/',
  AddorRemovetoWishlist = HostAPI + '/wishlists/',
  myWhishList = HostAPI + '/wishlists/my',
  myAddress = HostAPI + '/addresses/my',
  editMyAddressUrl = HostAPI + '/addresses/',
  addMyAddress = HostAPI + '/addresses',
  placeOrder = HostAPI + '/orders',
  myOrders = HostAPI + '/orders/my',
  myfilters = HostAPI + '/products/applied/', //null?categories=,
  getCartCONST = HostAPI + '/cart',
  AddorRemoveCartCONST = getCartCONST + '/add',
  GetGroupDetail = HostAPI + '/products/groupItems/',
  forgotPassword = HostAPI + '/users/forgot/',
  GetterForMoneyMethods = HostAPI + '/payment-methods/active',
  PathToExpoToken = HostAPI + '/tokens',
  SETTINGs = HostAPI + '/settings',
  CheckFBUserRegistered = HostAPI + '/users/facebook-login?email=',
  similarProducts = HostAPI + '/products/similar/',
  featuredLanding = HostAPI + '/categories/featured_landing/',
  categoryWidget = HostAPI + '/categories/homepage_category_widget/';
//-------------------------------------------------------------------
const NewHOST = 'http://api2.clicky.pk';
const testSignUp = NewHOST + '/users',
  testSignIn = NewHOST + '/authentication',
  verifyPhone = NewHOST + '/phone',
  getFreebies = NewHOST + '/freebie',
  applyFreebies = NewHOST + '/applyfreebie',
  smsAuth = NewHOST + '/authentication',
  sendSms = NewHOST + '/sms',
  otpVerify = NewHOST + '/otp-verify',
  userManagement = NewHOST + '/user-management';
//===============================================================
const HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'Cache-Control': 'no-cache'
};

let SpecialHEADER1 = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'Cache-Control': 'no-store',
  Authorization: ''
};
const CheckerFBUserExist = async EMAIL => {
  let ReturnData = null;
  await fetch(CheckFBUserRegistered + EMAIL)
    .then(Checker => Checker.json())
    .then(JSONChecker => {
      InfoLogger('Checker', JSONChecker);
      if (JSONChecker.hasOwnProperty('token')) {
        ReturnData = JSONChecker;
      }
    });
  return ReturnData;
};
//===============================================================
const getSmsAuth = async () => {
  let result = null;
  await AXIOS.post(
    smsAuth,
    JSON.stringify({
      email: 'admin@omnisell.pk',
      password: 'wordpass2019',
      strategy: 'local'
    }),
    {
      headers: HEADERS
    }
  )
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        result = data;
      } else {
        InfoLogger('smsAuthStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('smsAuthError', e);
    });
  return result;
};

const sendSmsVerification = async (phone, token, verificationCode) => {
  let result = null;
  HEADERS.Authorization = 'Bearer ' + token;
  await AXIOS.post(
    sendSms,
    JSON.stringify({
      mobileNo: phone,
      message: 'Your verification Code is ' + verificationCode,
      type: 'open'
    }),
    {
      headers: HEADERS
    }
  )
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        result = data;
      } else {
        InfoLogger('sendSmsVerificationStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('sendSmsVerificationError', e);
    });
  return result;
};
const otpVerification = async (phone, otp) => {
  let result = null;
  await AXIOS.post(
    otpVerify,
    JSON.stringify({
      phone,
      otp
    }),
    {
      headers: HEADERS
    }
  )
    .then(response => {
      const { data, status } = response;
      if (status === 200 || status === 201) {
        result = data;
      } else {
        InfoLogger('otpVerificationStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('otpVerificationVerificationError', e);
    });
  return result;
};
const sendForResetorResent = async (option, payload) => {
  let result = null;
  await AXIOS.post(
    userManagement,
    JSON.stringify({
      option,
      ...payload
    }),
    {
      headers: HEADERS
    }
  )
    .then(response => {
      const { data, status } = response;
      if (status === 200 || status === 201) {
        result = data;
      } else {
        InfoLogger(option + 'Status', status);
      }
    })
    .catch(e => {
      InfoLogger(option + 'Error', e);
    });
  return result;
};
//-----------------------------------------------------------------------------------------
const getFreebiesFunc = async Token => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let result = null;
  await AXIOS.get(getFreebies, {
    headers: SpecialHEADER1
  })
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        result = data;
      } else {
        InfoLogger('getFreebiesStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('getFreebiesError', e);
    });
  return result;
};

const applyFreebiesFunc = async (id, Token) => {
  let result = null;
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  await AXIOS.post(
    applyFreebies,
    JSON.stringify({
      id
    }),
    {
      headers: SpecialHEADER1
    }
  )
    .then(response => {
      const { data, status } = response;
      if (status === 200 || status === 201) {
        result = data;
      } else {
        InfoLogger('applyFreebiesStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('applyFreebiesError', e);
    });
  return result;
};
//==================================[APIcalls]===================
const GetMegaCategoryMenu = async () => {
  let response = await fetch(MegaCategoryMenu, {
    method: 'GET',
    headers: HEADERS
  });
  return await response.json();
};
const GetSettings = async () => {
  let response = await fetch(SETTINGs, {
    method: 'GET',
    headers: HEADERS
  });
  return await response.json();
};

const GetMoneyMethods = async () => {
  let result = [];
//  console.log('MoneyMethodsURL', GetterForMoneyMethods);
  await AXIOS.get(GetterForMoneyMethods)
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('MoneyMethods', data);
        result = data;
      } else {
        InfoLogger('MoneyMethodsStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('MoneyMethodsError', e);
    });
  return result;
};
const GetSubCategoryContent = async (searchFor, page, FilterorSortString) => {
//  console.log('FullString: ', searchFor + '?' + FilterorSortString);
  const response = await fetch(
    SubCatContent + searchFor + FilterorSortString + '&page=' + page,
    {
      method: 'GET',
      headers: HEADERS
    }
  );
  return await response.json();
};

const GetContentForSearch = async (searchFor, page, FilterorSortString) => {
  InfoLogger('FullString', searchFor + '?' + FilterorSortString);
  const response = await fetch(
    SearchedContent + searchFor + '?' + FilterorSortString + '&page=' + page,
    {
      method: 'GET',
      headers: HEADERS
    }
  );
  return await response.json();
};

const getFilters = async (SearchString, categorySlug) => {
  const StringToSend =
    SearchString != null
      ? myfilters + SearchString
      : myfilters + SearchString + '?categories=' + categorySlug;
  // console.log("Search String", StringToSend);
  const response = await fetch(StringToSend, {
    method: 'GET',
    headers: HEADERS
  });
  return await response.json();
};

const GetProductDetail = async ProductID => {
  let result = null;
  const PreparedUrl = ProductDetail + ProductID;
  InfoLogger('URL', PreparedUrl);
  await AXIOS.get(PreparedUrl)
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('ProductDetail', data);
        result = data;
      } else {
        InfoLogger('ProductDetailStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('ProductDetailError', e);
    });
  return result;
};
//=============================================Newest API================
const postSignUpWithPhone = async OBJECT => {
  let JsonConverted = null;
  await AXIOS.post(testSignUp, JSON.stringify(OBJECT), {
    headers: HEADERS
  })
    .then(response => {
      InfoLogger('SignUPWithPhone Response', response.data);
      JsonConverted = response.data;
    })
    .catch(e => {
      const { response } = e;
      const { data, status } = response;
      InfoLogger('ERROR status', status);
      InfoLogger('ERROR Response', data);
      JsonConverted = data;
    });
  return JsonConverted;
};

const postSignInWithPhone = async OBJECT => {
  let JsonConverted = null;
  OBJECT.strategy = 'phone';
  await AXIOS.post(testSignIn, JSON.stringify(OBJECT), {
    headers: HEADERS
  })
    .then(response => {
      const { data } = response;
      JsonConverted = data;
    })
    .catch(e => {
      const { response } = e;
      const { data, status } = response;
      InfoLogger('ERROR status', status);
      InfoLogger('ERROR RESPonse', data);
      JsonConverted = data;
    });

  return JsonConverted;
};
//========================================================================
const postSignUp = async OBJECT => {
  let JsonConverted = null;
  await AXIOS.post(SignUp, JSON.stringify(OBJECT), {
    headers: HEADERS
  })
    .then(response => {
      InfoLogger('SignUP Response', response.data);
      JsonConverted = response.data;
    })
    .catch(e => {
      const { response } = e;
      const { data, status } = response;
      InfoLogger('ERROR status', status);
      InfoLogger('ERROR Response', data);
      JsonConverted = data;
    });

  return JsonConverted;
};

const postSignIn = async OBJECT => {
  let JsonConverted = null;
  await AXIOS.post(SignIn, JSON.stringify(OBJECT), {
    headers: HEADERS
  })
    .then(response => {
      const { data } = response;
      JsonConverted = data;
    })
    .catch(e => {
      const { response } = e;
      const { data, status } = response;
      InfoLogger('ERROR status', status);
      InfoLogger('ERROR RESPonse', data);
      JsonConverted = data;
    });

  return JsonConverted;
};

const getGroupDetail = async ID => {
  let JsonConverted = null;
  await AXIOS.get(GetGroupDetail + ID).then(response => {
    InfoLogger('Group Reponse', response.data);
    JsonConverted = response.data;
  });

  return JsonConverted;
};
//==================================[WishList]=================================
const postAddorRemoveinWishlist = async (OBJECT, Token) => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let result = null;
  InfoLogger('URL4UpdateWishList', AddorRemovetoWishlist);
  //console.log("ObjectToBeSend:", OBJECT);
  await AXIOS.post(AddorRemovetoWishlist, JSON.stringify(OBJECT), {
    headers: SpecialHEADER1
  })
    .then(response => {
      const { data, status } = response;
      if (status === 200 || status === 202) {
        InfoLogger('UpdateWishList', data);
        result = data;
      } else {
        InfoLogger('UpdateWishListStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('UpdateWishListError', e);
    });
  return result;
};

const getMyWishList = async Token => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let result = [];
 // console.log('URL4WishList:', myWhishList);
  await AXIOS.get(myWhishList, {
    headers: SpecialHEADER1
  })
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('MyWishList', data);
        result = data;
      } else {
        InfoLogger('MyWishListStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('MyWishListError', e);
    });
  return result;
};
//==============================================================================

const getMyAddress = async Token => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let response = await fetch(myAddress, {
    method: 'GET',
    headers: SpecialHEADER1
  });
  try {
    return await response.json();
  } catch {
    return '' + response;
  }
};
const getMyOrders = async Token => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let response = await fetch(myOrders, {
    method: 'GET',
    headers: SpecialHEADER1
  });
  try {
    return await response.json();
  } catch {
    return '' + response;
  }
};
const getMyOrderDetail = async (orderID, Token) => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let JsonConverted = null;
  await AXIOS.get(myOrders + '/' + orderID, {
    headers: SpecialHEADER1
  }).then(response => {
    InfoLogger('Geting oderDetail Response', response.data);
    JsonConverted = response.data;
  });

  return JsonConverted;
};
const postMyAddress = async (Token, addressObject) => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let response = await fetch(addMyAddress, {
    method: 'POST',
    headers: SpecialHEADER1,
    body: JSON.stringify(addressObject)
  });
  try {
    return await response.json();
  } catch {
    return '' + response;
  }
};
const editMyAddress = async (Token, addressObject, addressID) => {
  SpecialHEADER1.Authorization = 'Bearer ' + Token;
  let response = await fetch(editMyAddressUrl + addressID, {
    method: 'PUT',
    headers: SpecialHEADER1,
    body: JSON.stringify(addressObject)
  });
  try {
    return await response.json();
  } catch {
    return '' + response;
  }
};

const deleteMyAddress = async (token, addressID) => {
  SpecialHEADER1.Authorization = 'Bearer ' + token;
  let response = await fetch(editMyAddressUrl + addressID, {
    method: 'DELETE',
    headers: SpecialHEADER1
  });
  try {
    return await response.json();
  } catch {
    return '' + response;
  }
};
//===============Get My Cart===========================================
const getMyCart = async Token => {
  HeaderWithToken = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + Token
  };
  let JsonConverted = null;
  await AXIOS.get(getCartCONST, {
    headers: Token ? HeaderWithToken : HEADERS
  }).then(response => {
    InfoLogger('Geting Cart Response', response.data);
    JsonConverted = response.data;
  });

  return JsonConverted;
};
//===============Update Cart==============================================
const updateMyCart = async (Token, Payload) => {
  HeaderWithToken = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + Token
  };
  InfoLogger('Sending ADD to CARD', Payload);
  InfoLogger('Token at time of Sending ADD to CARD', Token);
  let JsonConverted = null;
  await AXIOS.post(AddorRemoveCartCONST, JSON.stringify(Payload), {
    headers: Token ? HeaderWithToken : HEADERS
  }).then(response => {
    InfoLogger('Sending ADD to CARD Response: ', response.data);
    JsonConverted = response.data;
  });

  return JsonConverted;
};

//===============ORDER PLACING=============================================
const sendOrder = async (Token, addressObj) => {
  InfoLogger('Sending Orders Address Object', addressObj.address);
  InfoLogger('Token at time of Sending Placing Order', Token);
  HeaderWithToken = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + Token,
    'Cache-Control': 'no-cache'
  };
  let JsonConverted = null;
  await AXIOS.post(placeOrder, JSON.stringify(addressObj), {
    headers: HeaderWithToken
  }).then(response => {
    InfoLogger('Sending ADD to CARD Response', response.data);
    JsonConverted = response.data;
  });
  InfoLogger('OrderResponse', JsonConverted);
  return JsonConverted;
};

const updatePassword = async email => {
  let TobeReturn = '';
  InfoLogger('starting Password Recovery Data');
  await fetch(forgotPassword, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email
    })
  })
    .then(res => res.json())
    .then(json => {
      InfoLogger('Password Recovery results', json);
      if (json) {
        TobeReturn = json;
      }
    });
  return TobeReturn;
};

const SendExpoTokenToServer = async token => {
  let result = false;
  await fetch(PathToExpoToken, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      token
    })
  })
    .then(res => res.json())
    .then(json => {
      InfoLogger('ExpoTokenSend', json);
      result = true;
    });
  return result;
};
const SendExpoTokenToServerForUser = async (ID, expoPushToken, Token) => {
  let result = false;
  await AXIOS.put(SignUp + ID, JSON.stringify({ expoPushToken }), {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + Token,
      'Cache-Control': 'no-cache'
    }
  })
    .then(response => {
      InfoLogger('ExpoTokenSendForUser', response.data);
      result = true;
    })
    .catch(e => {
      InfoLogger('ExpoTokenSendForUserError', e);
    });
  return result;
};
const getSimilarProducts = async ProductID => {
  let result = [];
  const PreparedUrl = similarProducts + ProductID;
  //console.log('URL4GetSimilarProduct', PreparedUrl);
  await AXIOS.get(PreparedUrl)
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('SimilarProduct', data);
        result = data;
      } else {
        InfoLogger('SimilarProductStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('SimilarProductError', e);
    });
  return result;
};

const getFeaturedLanding = async SLUG => {
  let result = [];
  const PreparedUrl = featuredLanding + SLUG;
  InfoLogger('URL4getFeaturedLanding', PreparedUrl);
  await AXIOS.get(PreparedUrl)
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('FeaturedLanding', data);
        result = data;
      } else {
        InfoLogger('FeaturedLandingStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('FeaturedLandingError', e);
    });
  return result;
};
const getCategoryWidget = async SLUG => {
  let result = [];
  const PreparedUrl = categoryWidget + SLUG;
  InfoLogger('URL4getCategoryWidget', PreparedUrl);
  await AXIOS.get(PreparedUrl)
    .then(response => {
      const { data, status } = response;
      if (status === 200) {
        InfoLogger('CategoryWidget', data);
        result = data.categories;
      } else {
        InfoLogger('CategoryWidgetStatus', status);
      }
    })
    .catch(e => {
      InfoLogger('CategoryWidgetError', e);
    });
  return result;
};
//=================================[Exporter]===============================
const APIFuncs = {
    GetMegaCategoryMenu,
    GetSubCategoryContent,
    GetContentForSearch,
    GetProductDetail,
    postSignUp,
    postSignIn,
    postAddorRemoveinWishlist,
    getMyWishList,
    getMyAddress,
    editMyAddress,
    postMyAddress,
    deleteMyAddress,
    sendOrder,
    getMyOrders,
    getMyCart,
    updateMyCart,
    getFilters,
    getMyOrderDetail,
    getGroupDetail,
    updatePassword,
    GetMoneyMethods,
    SendExpoTokenToServer,
    SendExpoTokenToServerForUser,
    CheckerFBUserExist,
    GetSettings,
    getSimilarProducts,
    getFeaturedLanding,
    getCategoryWidget,
    //----------------------------------------
    postSignUpWithPhone,
    postSignInWithPhone,
    //----------------------------------------
    getSmsAuth,
    sendSmsVerification,
    otpVerification,
    sendForResetorResent,
    //----------------------------------------
    getFreebiesFunc,
    applyFreebiesFunc
  },
  APIConst = {
    HOST
  };
export { APIConst, APIFuncs };
