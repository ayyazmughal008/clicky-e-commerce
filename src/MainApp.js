import React from 'react';
import { View, Modal, Platform, Alert } from 'react-native';
import { Notifications, Linking } from 'expo';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import NAVIGATOR, { navigationService } from './navigators';
import LOADER from './component/LOADER';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from './modules/MakeMeResponsive';
import { LOADExpoToken, SendTokenForUnAuthorizeUser } from './redux/action';
import { CategoryExtractor, FilterExtract, InfoLogger } from './modules/utilts';
const prefix = Linking.makeUrl('/');
class MainApp extends React.Component {
  static propTypes = {
    Loading: PropTypes.bool,
    LOADExpoToken: PropTypes.func,
    SendTokenForUnAuthorizeUser: PropTypes.func,
    ExpoSate: PropTypes.object
  };

  componentDidMount() {
    Linking.addEventListener('url', this._handleDeepLink);
    const { LOADExpoToken, ExpoSate } = this.props;
    const { tokenRegisteredInAll, ExpoToken } = ExpoSate;
    if (tokenRegisteredInAll) {
      InfoLogger('ExpoPushToken', ExpoToken);
      //Alert.alert("ExpoTokken:", ExpoToken);
    } else {
      LOADExpoToken(tokenRegisteredInAll);
    }

    Linking.getInitialURL().then(url => {
      if (url) this._handleDeepLink({ url });
    });

    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleDeepLink);
  }
  //sort=-createdAt&sizes=14.5,One%20Size&brands=Oxford&Color=Bamboo%20Wood%20Red%20Word&price=1,104033&page=1
  _handleDeepLink = event => {
    let { url } = event;
    let { path, queryParams } = Linking.parse(url);
    let {
      id,
      categories,
      page,
      sort,
      sizes,
      brands,
      Color,
      price
    } = queryParams;
    // const Filter = FilterMaker(sizes, sort, brands, Color, price);
    let PathCategory = CategoryExtractor(path);
    let Filter = FilterExtract(url);
    if (id !== undefined) {
      navigationService.navigate('ProductPage', {
        ProductID: id
      });
    } else if (PathCategory !== '' && Filter === '') {
      // alert("slug: " + PathCategory);
      navigationService.navigate('CategoryPage', {
        slug: PathCategory
      });
    } else if (PathCategory !== '' && Filter !== '') {
      //alert("slug: " + PathCategory + "FilterString :" + Filter);
      navigationService.navigate('CategoryPage', {
        URL: url,
        slug: PathCategory,
        FilterString: Filter
      });
    } else {
      // alert("XXFilterString :" + Filter);
      navigationService.navigate('HomeScreen');
    }
  };

  _handleNotification = notification => {
    const { data } = notification;
    const {
      notification_type,
      notification_pid,
      notification_url,
      notification_cid
    } = data;
    console.log('NotificationData:', JSON.stringify(data));
    if (notification_type === 'P' && notification_pid)
      navigationService.navigate('ProductPage', {
        ProductID: notification_pid
      });
    else if (notification_type === 'C' && notification_cid)
      navigationService.navigate('CategoryPage', {
        slug: notification_cid
      });
    else if (notification_type === 'U' && notification_url) {
      const Cutted = notification_url.split('?');
      navigationService.navigate('CategoryPage', {
        URL: notification_url,
        slug: Cutted[0],
        FilterString: Cutted[1]
      });
    } else {
      navigationService.navigate('HomeScreen');
    }
  };

  render() {
    const { Loading } = this.props;
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <Modal
          animationType="fade"
          transparent={true}
          visible={Loading}
          onRequestClose={() => {}}
          presentationStyle="overFullScreen"
        >
          <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(100),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            {Loading && <LOADER />}
          </View>
        </Modal>
        <NAVIGATOR
          //uriPrefix={prefix}
          ref={navigatorRef => {
            navigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  Loading: state.APPSTATE.FetchingAndGettingLoading,
  ExpoSate: state.ExpoTokenState
});
export default connect(
  mapStateToProps,
  { LOADExpoToken, SendTokenForUnAuthorizeUser }
)(MainApp);
