import PullandBear from "./PullandBear";
import React, { Component } from "react";
export default class ParentCartItems extends Component {
  render() {
    const {
      navigation,
      productName,
      productAmount,
      productDetail,
      productDetailAmount,
      QTY,
      IMG,
      itemsQtyFunc,
      token,
      pid,
      vid
    } = this.props;
    return (
      <PullandBear
        navigation={navigation}
        productName={productName}
        productAmount={productAmount}
        productDetail={productDetail}
        productDetailAmount={productDetailAmount}
        Qty={QTY}
        IMG={IMG}
        itemsQtyFunc={itemsQtyFunc}
        token={token}
        pid={pid}
        vid={vid}
      />
    );
  }
}
