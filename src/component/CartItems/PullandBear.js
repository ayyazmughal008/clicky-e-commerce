import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import { Button, Icon, Image } from "react-native-elements";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
export default class CartItems extends Component {
  render() {
    const {
      navigation,
      productName,
      productAmount,
      productDetail,
      productDetailAmount,
      Qty,
      IMG,
      itemsQtyFunc,
      token,
      pid,
      vid
    } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            flexWrap: "wrap",
            alignItems: "flex-start",
            marginTop: heightPercentageToDP(2),
            marginBottom: heightPercentageToDP(2)
          }}
        >
          <View
            style={{
              width: "30%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: heightPercentageToDP(20),
                height: widthPercentageToDP(20)
              }}
              resizeMode="contain"
              source={{ uri: IMG }}
            />
          </View>

          <View style={{ width: "45%" }}>
            <Text style={{ fontWeight: "300", fontSize: 13 }} numberOfLines={1}>
              {productName}
            </Text>

            <Text style={{ fontWeight: "500", marginLeft: 5, fontSize: 13 }}>
              {"Qty: " + Qty}
            </Text>
          </View>
          <View style={{ width: "20%", marginLeft: widthPercentageToDP(2) }}>
            <Text style={{ fontWeight: "300", fontSize: 13 }}>
              {"PKR " + productDetailAmount}
            </Text>
          </View>
        </View>
        <View
          style={{
            width: "45%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            position: "absolute",
            bottom: "0%",
            marginLeft: widthPercentageToDP(30)
          }}
        >
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                itemsQtyFunc(
                  {
                    pid,
                    vid,
                    qty: -1
                  },
                  token
                );
              }}
            >
              <View
                style={{
                  width: widthPercentageToDP(8),
                  height: widthPercentageToDP(8),
                  borderRadius: widthPercentageToDP(4),
                  backgroundColor: "#000",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 21,
                    fontWeight: "400",
                    color: "#FFF",
                    textAlign: "right",
                    marginLeft: 5,
                    marginRight: 5
                  }}
                >
                  {"-"}
                </Text>
              </View>
            </TouchableOpacity>

            <Text
              style={{
                fontSize: 11,
                fontWeight: "400",
                color: "#000",
                textAlign: "right",
                marginLeft: 5,
                marginRight: 5
              }}
            >
              {Qty}
            </Text>

            <TouchableOpacity
              onPress={() => {
                itemsQtyFunc(
                  {
                    pid,
                    vid,
                    qty: +1
                  },
                  token
                );
              }}
            >
              <View
                style={{
                  width: widthPercentageToDP(8),
                  height: widthPercentageToDP(8),
                  borderRadius: widthPercentageToDP(4),
                  backgroundColor: "#e3e627",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 19,
                    fontWeight: "400",
                    color: "#FFF",
                    textAlign: "right",
                    marginLeft: 5,
                    marginRight: 5
                  }}
                >
                  {"+"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <Button
            onPress={() => {
              itemsQtyFunc(
                {
                  pid,
                  vid,
                  qty: -Qty
                },
                token
              );
            }}
            buttonStyle={{ marginLeft: widthPercentageToDP(7) }}
            type="clear"
            title="Remove"
            titleStyle={{
              marginTop: 5,
              color: "#000",
              fontSize: widthPercentageToDP(4)
            }}
            icon={<Icon name="delete" size={20} />}
          />
        </View>
      </View>
    );
  }
}
