import React from "react";
import { View, Platform } from "react-native";
import Lottie from "lottie-react-native";
import AnimationJson from "../../../assets/animation/1129-loader-spinner.json";
import { widthPercentageToDP } from "../../modules/MakeMeResponsive";
export default class Loader extends React.PureComponent {
  render() {
    return (
      <View>
        <Lottie
          ref={animation => {
            this.animation = animation;
          }}
          style={
            Platform.OS === "ios"
              ? {
                  width: widthPercentageToDP(15),
                  height: widthPercentageToDP(15)
                }
              : {
                  width: widthPercentageToDP(20),
                  height: widthPercentageToDP(20)
                }
          }
          source={AnimationJson}
        />
      </View>
    );
  }
  componentDidMount() {
    this.animation.play();
  }
}
