import React from "react";
import { View, Text, ScrollView, TouchableOpacity, Image } from "react-native";
import { MaterialIcons as Icon } from "@expo/vector-icons";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";

export default class OrderList extends React.Component {
  render() {
    const { status, qty, date, products, onPress } = this.props;

    return (
      <View
        style={{
          height: heightPercentageToDP(30),
          width: widthPercentageToDP(100),
          backgroundColor: "#fff",
          borderBottomWidth: widthPercentageToDP(0.4),
          borderBottomColor: "#cccccc",
          padding: widthPercentageToDP(4)
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(4.3),
                fontWeight: "300",
                color: "#A9A9A9",
                marginLeft: widthPercentageToDP(1)
              }}
            >
              {status}
            </Text>
            {status !== "Refunded" && (
              <Text
                style={{
                  fontSize: widthPercentageToDP(4.3),
                  fontWeight: "300",
                  color: "#A9A9A9",
                  marginLeft: widthPercentageToDP(1)
                }}
              >
                by {date.dDate}
              </Text>
            )}
            <Text
              style={{
                fontSize: widthPercentageToDP(4.5),
                fontWeight: "500",
                color: "pink",
                marginLeft: widthPercentageToDP(1)
              }}
            >
              |
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(4.3),
                fontWeight: "300",
                color: "#A9A9A9",
                marginLeft: widthPercentageToDP(1)
              }}
            >
              {qty} item
            </Text>
          </View>

          <Icon
            name="keyboard-arrow-right"
            color="#000"
            size={widthPercentageToDP(5)}
          />
        </View>

        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          {products.map((item, index) => (
            <TouchableOpacity key={"ITEMS" + index} onPress={onPress}>
              <Image
                style={{
                  height: widthPercentageToDP(40),
                  width: widthPercentageToDP(40),
                  marginLeft: widthPercentageToDP(3),
                  marginRight: widthPercentageToDP(3),
                  marginTop: widthPercentageToDP(3)
                }}
                resizeMode="contain"
                source={{ uri: item.img.medium }}
              />
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}
