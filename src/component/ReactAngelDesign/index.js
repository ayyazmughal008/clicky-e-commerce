import React, { Component } from "react";
import { View, TouchableOpacity, Text, ActivityIndicator } from "react-native";
import { widthPercentageToDP } from "../../modules/MakeMeResponsive";
import { ProgressiveImage } from "../progressive";
const Placeholder = require("../../../assets/images/placeholder.png");
export default class RoundImages extends Component {
  render() {
    const { text, imageUri, clickHandler } = this.props;
    return (
      <View
        style={{
          height: widthPercentageToDP(20),
          width: widthPercentageToDP(50),
          alignItems: "center",
          marginRight: widthPercentageToDP(0.5),
          marginBottom: widthPercentageToDP(0.5),
          backgroundColor: "#fff"
        }}
      >
        <TouchableOpacity
          onPress={clickHandler}
          style={{
            height: widthPercentageToDP(20),
            width: widthPercentageToDP(50),
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "#b2b6b5",
              fontSize: widthPercentageToDP(4),
              fontWeight: "300",
              marginLeft: widthPercentageToDP(2),
              width:widthPercentageToDP(20)
            }}
          >
            {text}
          </Text>
          <ProgressiveImage
            style={{
              height: widthPercentageToDP(15),
              width: widthPercentageToDP(15),
              borderRadius: widthPercentageToDP(7.5),
              marginRight: widthPercentageToDP(2)
            }}
            thumbnailSource={Placeholder}
            resizeMode="cover"
            source={imageUri}
            //PlaceholderContent={<ActivityIndicator />}
          />
          {/* <Image
            resizeMode="cover"
            style={{
              height: widthPercentageToDP(15),
              width: widthPercentageToDP(15),
              borderRadius: widthPercentageToDP(7.5),
              marginRight: widthPercentageToDP(2)
            }}
            PlaceholderContent={<ActivityIndicator />}
            source={imageUri}
          /> */}
        </TouchableOpacity>
      </View>
    );
  }
}
