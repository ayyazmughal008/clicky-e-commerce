import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../modules/MakeMeResponsive';
export default class FilterSection extends Component {
  state = { Opened: false };
  onPress = () => {
    this.setState({
      Opened: !this.state.Opened
    });
  };

  render = () => {
    const { title, children } = this.props;
    const { Opened } = this.state;
    return (
      <View style={styles.containerStyles}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.headerStyles}
          onPress={this.onPress}
        >
          <Text style={styles.leftText}>{title}</Text>
          <View style={styles.textBackground}>
            <Icon
              name={Opened ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}
              color="#333"
              size={widthPercentageToDP(6)}
            />
          </View>
        </TouchableOpacity>
        {Opened === true ? (
          <View style={styles.expandedStyle}>{children}</View>
        ) : (
          <View />
        )}
      </View>
    );
  };
}

const styles = {
  containerStyles: {
    flex: 1,
    width: widthPercentageToDP(100),
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerStyles: {
    opacity: 1,
    width: widthPercentageToDP(95),
    borderWidth: StyleSheet.hairlineWidth,
    height: heightPercentageToDP(6),
    borderColor: '#ddd',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 10
  },
  textBackground: {
    height: 40,
    width: 40,
    right: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  leftText: {
    fontSize: 18
  },
  textStyle: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  expandedStyle: {
    flex: 1,
    width: widthPercentageToDP(95)
  }
};
