import React from "react";
import Davago from "./Davago";
export default class ParentHeaders extends React.Component {
  render() {
    const {
      backgroundColor,
      leftClickHandler,
      LeftIcon,
      title,
      Right1ClickHandler,
      Right1con,
      Right2ClickHandler,
      Right2con
    } = this.props;

    return (
      <Davago
        backgroundColor={backgroundColor}
        leftClickHandler={leftClickHandler}
        LeftIcon={LeftIcon}
        title={title}
        Right1ClickHandler={Right1ClickHandler}
        Right1con={Right1con}
        Right2ClickHandler={Right2ClickHandler}
        Right2con={Right2con}
      />
    );
  }
}
