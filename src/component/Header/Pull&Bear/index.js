import React from "react";
import { Appbar } from "react-native-paper";

export default class Header extends React.Component {
  render() {
    const {
      backgroundColor,
      leftClickHandler,
      LeftIcon,
      title,
      ScreenHeader,
      Right1ClickHandler,
      Right1con,
      Right2ClickHandler,
      Right2con
    } = this.props;

    return (
      <Appbar.Header
        style={{
          backgroundColor: backgroundColor
        }}
      >
        <Appbar.Action
          icon={LeftIcon}
          color="#fff"
          onPress={leftClickHandler}
        />
        <Appbar.Content
          title={title}
          color="#fff"
          titleStyle={{
            fontSize: 20,
            fontFamily: "Metropolis_Regular",
            fontWeight: "400"
          }}
        />
        <Appbar.Action
          icon={Right1con}
          color="#fff"
          onPress={Right1ClickHandler}
        />
        <Appbar.Action
          icon={Right2con}
          color="#fff"
          onPress={Right2ClickHandler}
        />
      </Appbar.Header>
    );
  }
}
