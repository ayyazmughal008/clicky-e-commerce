import React from 'react';
import { Header } from 'react-native-elements';

class Headers extends React.Component {
  render() {
    const {
      leftClickHandler,
      LeftIcon,
      title,
      Right1ClickHandler,
      Right1con,
      backgroundColor,
      ScreenHeader,
      Right2ClickHandler,
      Right2con
    } = this.props;

    return LeftIcon !== undefined ? (
      <Header
        leftComponent={{
          icon: LeftIcon,
          color: '#000',
          onPress: leftClickHandler
        }}
        centerComponent={{ text: title, style: { color: '#000' } }}
        rightComponent={{ icon: Right1con, onPress: Right1ClickHandler }}
        containerStyle={{
          backgroundColor: '#fff',
          justifyContent: 'space-around',
          borderBottomWidth: 0.5,
          borderColor: '#aaa'
        }}
      />
    ) : (
      <Header
        centerComponent={{ text: title, style: { color: '#000' } }}
        rightComponent={{ icon: Right1con, onPress: Right1ClickHandler }}
        containerStyle={{
          backgroundColor: '#fff',
          justifyContent: 'space-around',
          borderBottomWidth: 0.5,
          borderColor: '#aaa'
        }}
      />
    );
  }
}

export default Headers;
