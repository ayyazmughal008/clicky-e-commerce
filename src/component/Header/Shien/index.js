import React from "react";
import { Header } from "react-native-elements";

class Headers extends React.Component {
  render() {
    const {
      backgroundColor,
      leftClickHandler,
      LeftIcon,
      title,
      ScreenHeader,
      Right1ClickHandler,
      Right1con,
      Right2ClickHandler,
      Right2con
    } = this.props;

    return (
      <Header
        leftComponent={{
          icon: LeftIcon,
          color: "#fff",
          onPress: leftClickHandler
        }}
        centerComponent={{ text: title, style: { color: "#fff" } }}
        rightComponent={{ icon: Right1con, color: "#fff" }}
        containerStyle={{
          backgroundColor: "#000",
          justifyContent: "space-around",
          borderBottomWidth: 0.5,
          borderColor: "#fff"
        }}
      />
    );
  }
}

export default Headers;
