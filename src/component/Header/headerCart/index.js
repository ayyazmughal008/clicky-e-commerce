import React from "react";
import { View, TouchableOpacity, Image, Text, StyleSheet } from "react-native";
import { MaterialIcons as Icon } from "@expo/vector-icons";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../../modules/MakeMeResponsive";
import { InfoLogger } from "../../../modules/utilts";
const IconSize = widthPercentageToDP(6);
export default class Header extends React.Component {
  render() {
    const {
      leftClickHandler,
      leftIcon,
      centerText,
      right1ClickHandler,
      right1Icon,
      right2ClickHandler,
      right2Icon,
      image,
      cart
    } = this.props;
    let cartQty = 0;
    InfoLogger("cart", cart);
    if (cart) {
      const { qty } = cart;
      cartQty = qty;
    }
    return (
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(8),
          marginTop: heightPercentageToDP(4),
          borderBottomWidth: StyleSheet.hairlineWidth,
          borderBottomColor: "#cccccc",
          alignItems: "center",
          justifyContent: "space-around",
          flexDirection: "row"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            paddingLeft: widthPercentageToDP(2),
            width: widthPercentageToDP(32),
            height: heightPercentageToDP(8)
          }}
        >
          <TouchableOpacity
            style={{
              justifyContent: "center",
              width: widthPercentageToDP(10),
              height: "100%"
            }}
            onPress={leftClickHandler}
          >
            <Icon name={leftIcon} color="#000" size={IconSize} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: widthPercentageToDP(32),
            height: heightPercentageToDP(8),
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {image !== undefined ? (
            <Image
              style={{
                width: widthPercentageToDP(32),
                height: heightPercentageToDP(8)
              }}
              source={image}
              resizeMode="contain"
            />
          ) : (
            <Text style={{ fontSize: widthPercentageToDP(5) }}>
              {centerText !== undefined ? centerText : ""}
            </Text>
          )}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            width: widthPercentageToDP(32),
            height: heightPercentageToDP(8)
          }}
        >
          <TouchableOpacity
            style={{
              justifyContent: "center",
              width: widthPercentageToDP(10),
              height: "100%"
            }}
            onPress={right1ClickHandler}
          >
            {cartQty > 0 && (
              <View
                style={{
                  position: "absolute",
                  left: IconSize - widthPercentageToDP(3),
                  top: "20%",
                  zIndex: 3,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "red",
                  width: widthPercentageToDP(4.5),
                  height: widthPercentageToDP(4.5),
                  borderRadius: widthPercentageToDP(2.25)
                }}
              >
                <Text
                  style={{
                    fontSize: widthPercentageToDP(2),
                    color: "#FFF"
                  }}
                >
                  {cartQty}
                </Text>
              </View>
            )}

            <Icon name={right1Icon} color="#000" size={IconSize} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              width: widthPercentageToDP(10),
              height: "100%"
            }}
            onPress={right2ClickHandler}
          >
            <Icon name={right2Icon} color="#000" size={IconSize} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
