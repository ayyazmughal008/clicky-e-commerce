'use strict';
import React from 'react';
import { View, Modal, TouchableOpacity, Text } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { MaterialCommunityIcons } from '@expo/vector-icons';
// const AlertColor = {
//   error: '#F33951',
//   info: '#35C4FC',
//   success: '#75cf0f',
//   warning: '#FFB70A'
// };
const AlertColor = {
  error: '#FFF',
  info: '#FFF',
  success: '#FFF',
  warning: '#FFF'
};
const IconName = ['alert-circle', 'information', 'check-circle'];
export default ({ alertContent, onSwitch, toggaleFunc }) => {
  let { header, message, type } = alertContent;
  type = type === undefined ? 'info' : type;
  const chosenName =
    type === undefined ? 0 : type === 'info' ? 1 : type === 'success' ? 2 : 0;
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={onSwitch}
      onRequestClose={() => {}}
    >
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(100),
          backgroundColor: 'rgba(0, 0, 0, 0.3)',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <View
          style={{
            width: widthPercentageToDP(85),
            height: heightPercentageToDP(18),
            backgroundColor: AlertColor[type],
            borderRadius: widthPercentageToDP(3),
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 5
          }}
        >
          <View
            style={{
              width: widthPercentageToDP(80),
              height: heightPercentageToDP(16),
              flexDirection: 'row'
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(12),
                height: heightPercentageToDP(16),
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'
              }}
            >
              <MaterialCommunityIcons
                size={widthPercentageToDP(8)}
                // color={'#FFF'}
                color={'#000'}
                name={IconName[chosenName]}
              />
            </View>
            <View
              style={{
                width: widthPercentageToDP(68),
                height: heightPercentageToDP(16),
                justifyContent: 'center'
                //alignItems: 'center'
              }}
            >
              <Text
                style={{
                  //color: '#FFF',
                  color: '#000',
                  fontSize: widthPercentageToDP(6),
                  fontWeight: 'bold'
                }}
              >
                {header}
              </Text>
              <Text
                style={{
                  //color: '#FFF',
                  color: '#000',
                  fontSize: widthPercentageToDP(5),
                  fontWeight: 'bold'
                }}
              >
                {message}
              </Text>
              <TouchableOpacity
                style={{
                  width: widthPercentageToDP(10),
                  height: widthPercentageToDP(10),
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'absolute',
                  bottom: 0,
                  right: 0
                }}
                onPress={() => {
                  toggaleFunc(false);
                }}
              >
                <Text adjustsFontSizeToFit={true}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};
