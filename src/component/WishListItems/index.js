import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { MaterialIcons as Icon } from "@expo/vector-icons";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import { checkWishlistItemExist } from "../../modules/utilts";
export default class WishListItems extends Component {
  render() {
    const { Product, Variant, RemoveFunc, navigation, token } = this.props;
    const { _id, imgA } = Product;
    return (
      <View
        style={{
          width: widthPercentageToDP(50),
          height: widthPercentageToDP(50),
          marginBottom: heightPercentageToDP(2)
        }}
      >
        <View
          style={{
            width: widthPercentageToDP(7),
            height: widthPercentageToDP(7),
            borderRadius: widthPercentageToDP(3.5),
            backgroundColor: "#e3e627",
            position: "absolute",
            right: "2%",
            top: "1%",
            zIndex: 5,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Icon
            name="close"
            color="#fff"
            size={widthPercentageToDP(5)}
            onPress={() => {
              const { wishlist } = this.props;
              const wishlistItem = {
                product: {
                  _id
                },
                variant: {
                  _id: Variant._id
                }
              };
              const FoundAt = checkWishlistItemExist(wishlist, wishlistItem);
              if (FoundAt != -1) {
                wishlist.splice(FoundAt, 1);
                RemoveFunc(wishlistItem, wishlist, token);
              }
            }}
          />
        </View>

        <TouchableOpacity
          style={{
            width: widthPercentageToDP(50),
            height: widthPercentageToDP(50)
          }}
          onPress={() =>
            navigation.navigate("ProductPage", {
              ProductID: _id
            })
          }
        >
          {imgA.length !== 0 && (
            <Image
              style={{
                width: widthPercentageToDP(50),
                height: widthPercentageToDP(50)
              }}
              source={{ uri: imgA[0].medium }}
              resizeMode="contain"
            />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
