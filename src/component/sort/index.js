import React, { Component } from "react";
import {
  View,
  Dimensions,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Text,
  FlatList,
  TouchableHighlight,
  ScrollView,
  StatusBar,
  Image,
  Slider
} from "react-native";
import { Icon, CheckBox } from "react-native-elements";
import { widthPercentageToDP } from "../MakeMeResponsive";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import { sorts as sortData } from "../../modules/utilts";
class sort extends Component {
  state = {
    modalVisible: false,
    sortString: ""
  };

  componentWillReceiveProps(props) {
    this.setState({ currentSort: props.sortString });
  }

  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const { currentSort } = this.state;
    const { currentSlug, ApplyFunc, sortString, sortFunc } = this.props;
    return (
      <View style={styles.containerView}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
          <View style={styles.modalHeaderView}>
            <View style={{ flexDirection: "row" }}>
              <Icon
                name="arrow-back"
                color="#000"
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              />
              <View style={styles.modalHeaderTitleView}>
                <Text style={styles.modalHeaderTitle}>{"Sort"}</Text>
              </View>
            </View>

            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                transparent
                style={styles.footerBtn}
                onPress={() => {
                  sortFunc("");
                  this.setModalVisible(false);
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#777",
                    marginRight: 15
                  }}
                >
                  Clear all
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              height: "100%",
              width: "100%",
              alignItems: "center",
              backgroundColor: "#fff"
            }}
          >
            <View style={{ height: "60%", width: "100%" }}>
              <FlatList
                style={{ marginTop: 5, marginBottom: 5 }}
                keyExtractor={this._keyExtractor}
                showsHorizontalScrollIndicator={false}
                data={sortData}
                extraData={this.state}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      flex: 1,
                      borderBottomWidth: 1,
                      borderColor: "#f1f1f1",
                      justifyContent: "center",
                      marginLeft: 10,
                      marginRight: 10
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        alignItems: "center",
                        justifyContent: "space-between"
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: "400",
                          fontFamily: "Metropolis_Regular",
                          padding: 10
                        }}
                      >
                        {item.name}
                      </Text>

                      <CheckBox
                        checkedColor="red"
                        checked={sortString === item.val}
                        onPress={() => {
                          sortFunc(item.val);
                        }}
                      />
                    </View>
                  </View>
                )}
              />
            </View>

            <TouchableOpacity
              style={{
                height: (deviceHeight * 2) / 8.5 - 100,
                width: (deviceWidth * 2) / 2.2,
                borderRadius: 45,
                borderWidth: 3,
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                bottom: "15%"
              }}
              onPress={() => {
                ApplyFunc(currentSlug);
                this.setModalVisible(false);
              }}
            >
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: "400",
                  fontFamily: "Metropolis_Regular",
                  padding: 10
                }}
              >
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <View style={styles.modalInitView}>
          <TouchableOpacity
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <Text style={styles.modalInitBtnText}>{"Sort"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    height: "100%",
    width: "49%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  modalHeaderView: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#F9F9F9",
    height: "9%",
    borderBottomColor: "#aaa",
    borderBottomWidth: 0.5
  },
  modalHeaderIcon: {
    color: "#000",
    fontSize: 15,
    marginLeft: 0,
    marginBottom: -3,
    justifyContent: "center"
  },
  modalHeaderTitleView: {
    justifyContent: "center"
  },
  modalHeaderTitle: {
    fontSize: 20,
    fontWeight: "500",
    color: "#777",
    textAlign: "center",
    marginLeft: 20
  },
  gridLeftCol: {
    backgroundColor: "#fff",
    borderRightColor: "#C0C0C0"
  },
  footer: {
    shadowColor: "#999",
    shadowRadius: 1,
    shadowOpacity: 0.2
  },
  footerBtn: {
    alignSelf: "center",
    justifyContent: "center"
  },
  modalInitView: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalInitBtnText: {
    fontSize: 17,
    fontFamily: "Metropolis_Regular",
    fontWeight: "500",
    color: "#555",
    textDecorationLine: "underline",
    padding: 10
  }
});

export default sort;
