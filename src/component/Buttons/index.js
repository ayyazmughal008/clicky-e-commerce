import React from 'react';
import { View, TouchableOpacity, Text, ActivityIndicator } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
const ClickyButton = ({
  onPress,
  text,
  loading,
  buttonColor,
  textColor,
  buttonWidth
}) => {
  return (
    <TouchableOpacity
      onPress={onPress !== undefined ? onPress : () => {}}
      style={{
        height: heightPercentageToDP(7),
        width:
          buttonWidth !== undefined ? buttonWidth : widthPercentageToDP(90),
        borderRadius: widthPercentageToDP(1),
        elevation: 2,
        backgroundColor: buttonColor !== undefined ? buttonColor : '#000',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Text
        style={{
          textAlign: 'center',
          fontSize: widthPercentageToDP(4),
          fontWeight: '300',
          color: textColor !== undefined ? textColor : '#fff'
        }}
      >
        {text !== undefined ? text : ''}
      </Text>
      {loading !== undefined && loading === true && (
        <ActivityIndicator
          style={{ position: 'absolute', zIndex: 2, right: '2%' }}
          size="large"
          color="#fff"
        />
      )}
    </TouchableOpacity>
  );
};
const RadioButton = ({ selected, text, onPress }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{ flexDirection: 'row', alignItems: 'center' }}
    >
      <View
        style={[
          {
            height: widthPercentageToDP(6),
            width: widthPercentageToDP(6),
            borderRadius: widthPercentageToDP(3),
            borderWidth: widthPercentageToDP(0.3),
            borderColor: '#000',
            alignItems: 'center',
            justifyContent: 'center'
          }
        ]}
      >
        <View
          style={{
            height: widthPercentageToDP(4),
            width: widthPercentageToDP(4),
            borderRadius: widthPercentageToDP(2),
            backgroundColor: selected ? 'rgba(0, 0, 0, 1)' : 'rgba(0, 0, 0, 0)'
          }}
        />
      </View>
      <Text
        style={{
          fontSize: widthPercentageToDP(4),
          marginLeft: widthPercentageToDP(2),
          color: '#000'
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export { ClickyButton, RadioButton };
