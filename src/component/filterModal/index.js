"use strict";
import React, { Component } from "react";
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Modal,
  Button as NButton
} from "react-native";
import ParentHeaders from "../Header";
import Rheostat, { RheostatThemeProvider } from "react-native-rheostat";
import { Icon } from "react-native-elements";
import { heightPercentageToDP, widthPercentageToDP } from "../../modules/MakeMeResponsive";
import FilterSections from "./FilterSections";
import { sorts } from "../../modules/utilts";
import { APIFuncs } from "../../modules/APIs";
import _ from "lodash";

export default class FiltersModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      APIOptionFilters: [],
      selectedFilters: null,
      InitialFilter: null,
      currentIndex: -1,
      EnableView: false
    };
  }

  componentWillMount() {
    APIFuncs.getFilters(this.props.CategorySlug).then(filters => {
      const APIOptionFilters = [];
      const selectedFilters = {};
      APIOptionFilters.push({
        key: "Sort",
        content: sorts,
        features: false
      });
      selectedFilters["Sort"] = {
        content: null,
        features: false,
        name: "No order"
      };

      for (let key in filters) {
        if (filters.hasOwnProperty(key)) {
          if (key !== "features") {
            APIOptionFilters.push({
              key: key,
              content: filters[key],
              features: false
            });
            selectedFilters[key] = {
              content: key !== "price" ? [] : filters[key],
              features: false
            };
          } else {
            filters[key].map(ITEM => {
              const { name, options } = ITEM;

              if (name) {
                APIOptionFilters.push({
                  key: name,
                  content: options,
                  features: true
                });

                selectedFilters[name] = {
                  content: [],
                  features: false
                };
              }
            });
          }
        }
      }
      this.setState({
        APIOptionFilters,
        selectedFilters,
        InitialFilter: selectedFilters,
        EnableView: true
      });
    });
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  PriceView = ({ value, content, onRheostatValUpdated }) => (
    <RheostatThemeProvider theme={{ themeColor: "#000000", grey: "#fafafa" }}>
      <Rheostat
        values={value}
        min={content[0]}
        max={content[1]}
        onValuesUpdated={onRheostatValUpdated}
      />
    </RheostatThemeProvider>
  );

  SetIndex = currentIndex =>
    this.setState({
      currentIndex
    });

  triggerApply = () => {
    const deep = _.cloneDeep(this.state.selectedFilters);
    this.props.triggerApply(deep);
    this.props.closeFunc();
  };

  ModalClearAllFilter = () => {
    const deep = _.cloneDeep(this.state.InitialFilter);
    //console.log("DEleteBtn Pressed", deep);
    this.setState({ selectedFilters: deep, currentIndex: -1 });
  };
  CloseModal = () => {
    this.props.closeFunc();
  };
  //=================================================================
  putFilter = (key, word, nameForSort) => {
    const FilterForApplied = this.state.selectedFilters;
    if (key === "Sort") {
      FilterForApplied["Sort"].content = word;
      FilterForApplied["Sort"].name = nameForSort;
    } else {
      const FindAt = FilterForApplied[key].content.indexOf(word);
      if (FindAt === -1) {
        FilterForApplied[key].content.push(word);
      } else {
        FilterForApplied[key].content.splice(FindAt, 1);
      }
    }
    this.setState({
      selectedFilters: FilterForApplied
    });
  };

  onRheostatValUpdated = ({ values }) => {
    const { selectedFilters } = this.state;
    selectedFilters["price"].content = values;
    this.setState({
      selectedFilters
    });
  };
  //==================================================================

  render() {
    const { ModalOn, CloseButMaintainAppliedFilter } = this.props;
    const {
      PriceView,
      onRheostatValUpdated,
      putFilter,
      triggerApply,
      CloseModal
    } = this;
    const {
      selectedFilters,
      currentIndex,
      InitialFilter,
      APIOptionFilters
    } = this.state;
    console.log("SelectedFilters:\n", selectedFilters);
    console.log("InitialFilters:\n", InitialFilter);
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalOn}
          onRequestClose={CloseModal}
        >
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={CloseModal}
            LeftIcon="close"
            title="FILTER"
            Right1ClickHandler={this.ModalClearAllFilter}
            Right1con="delete"
          />
          <NButton
            title="TEST"
            onPress={() => {
              console.log("StateAtThisTime", {
                Applied: this.state.selectedFilters,
                InitialFilter: this.state.InitialFilter
              });
            }}
          />
          <View style={styles.container}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
              {selectedFilters !== null &&
                APIOptionFilters.map((item, index) => {
                  const { content, key, features } = item;
                  return (
                    <FilterSections
                      key={"Filter" + index}
                      currentIndex={currentIndex}
                      SetIndex={this.SetIndex}
                      title={key === "Sort" ? selectedFilters[key].name : key}
                      sectionkey={key}
                      number={index}
                      expanded={currentIndex === index}
                    >
                      {!features && key !== "price" ? (
                        key === "Sort" ? (
                          <View style={styles.filterContent}>
                            <FlatList
                              keyExtractor={this._keyExtractor}
                              showsVerticalScrollIndicator={false}
                              data={content}
                              extraData={this.state}
                              renderItem={({ item }) => {
                                const { name, val } = item;

                                const Selected =
                                  selectedFilters[key].content === val;
                                return (
                                  <TouchableOpacity
                                    onPress={() => {
                                      putFilter(key, val, name);
                                    }}
                                  >
                                    <Text
                                      style={{
                                        padding: widthPercentageToDP(2),
                                        fontSize: widthPercentageToDP(4),
                                        fontWeight: Selected ? "bold" : "normal"
                                      }}
                                    >
                                      {name}
                                    </Text>
                                  </TouchableOpacity>
                                );
                              }}
                            />
                          </View>
                        ) : (
                          <View style={styles.filterContent}>
                            <FlatList
                              keyExtractor={this._keyExtractor}
                              showsVerticalScrollIndicator={false}
                              data={content}
                              extraData={this.state}
                              numColumns={2}
                              renderItem={({ item }) => {
                                const { name } = item;
                                const Selected =
                                  selectedFilters[key].content.indexOf(name) !==
                                  -1;
                                return (
                                  <TouchableOpacity
                                    style={{
                                      width: "50%",
                                      flexDirection: "row"
                                      //alignItems: "center"
                                    }}
                                    onPress={() => {
                                      putFilter(key, name, name);
                                    }}
                                  >
                                    {Selected && (
                                      <Icon
                                        name="brightness-1"
                                        iconStyle={{ marginVertical: 5 }}
                                        color="#333"
                                      />
                                    )}
                                    <Text
                                      style={{
                                        padding: widthPercentageToDP(2),
                                        fontSize: widthPercentageToDP(4),
                                        fontWeight: Selected ? "bold" : "normal"
                                      }}
                                    >
                                      {name}
                                    </Text>
                                  </TouchableOpacity>
                                );
                              }}
                            />
                          </View>
                        )
                      ) : features ? (
                        <View style={styles.filterContent}>
                          <FlatList
                            keyExtractor={this._keyExtractor}
                            showsVerticalScrollIndicator={false}
                            numColumns={2}
                            data={content}
                            extraData={this.state}
                            renderItem={({ item }) => {
                              const { name } = item;

                              const Selected =
                                selectedFilters[key].content.indexOf(name) !==
                                -1;
                              return (
                                <TouchableOpacity
                                  style={{
                                    width: "50%",
                                    flexDirection: "row"
                                  }}
                                  onPress={() => {
                                    putFilter(key, name, name);
                                  }}
                                >
                                  {Selected && (
                                    <Icon
                                      name="brightness-1"
                                      iconStyle={{ marginVertical: 5 }}
                                      color="#333"
                                    />
                                  )}
                                  <Text
                                    style={{
                                      padding: widthPercentageToDP(2),
                                      fontSize: widthPercentageToDP(4),
                                      fontWeight: Selected ? "bold" : "normal"
                                    }}
                                  >
                                    {name}
                                  </Text>
                                </TouchableOpacity>
                              );
                            }}
                          />
                        </View>
                      ) : (
                        key === "price" && (
                          <View style={styles.headerText}>
                            <PriceView
                              value={selectedFilters[key].content}
                              content={content}
                              onRheostatValUpdated={onRheostatValUpdated}
                            />
                            <View
                              style={{
                                flexDirection: "row",
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "space-between",
                                paddingHorizontal: "5%",
                                paddingTop: "5%"
                              }}
                            >
                              <Text>
                                {"Min:"} {selectedFilters[key].content[0]}
                              </Text>
                              <Text>
                                {"Max:"} {selectedFilters[key].content[1]}
                              </Text>
                            </View>
                          </View>
                        )
                      )}
                    </FilterSections>
                  );
                })}
              {/* COlor collapas start here  */}
              {/* 
            <TouchableOpacity
              style={styles.touchableStyle}
              onPress={this.toggleExpanded5}
            >
              <View style={styles.header}>
                <Text style={styles.headerText}>Color</Text>
                <Icon
                  name={
                    this.state.collapsed5
                      ? "keyboard-arrow-down"
                      : "keyboard-arrow-up"
                  }
                  iconStyle={{ marginTop: 5 }}
                  color="#00aced"
                />
              </View>
            </TouchableOpacity>
            <Collapsible collapsed={this.state.collapsed5} align="center">
              <View style={styles.filterContent}>
                <FlatList
                  keyExtractor={this._keyExtractor}
                  numColumns={5}
                  showsVerticalScrollIndicator={false}
                  data={colors}
                  renderItem={({ item }) => (
                    <FilterCOlors
                      navigation={this.props.navigation}
                      colors={item.coler}
                      //clickHandler={() => }
                    />
                  )}
                />
              </View>
            </Collapsible> */}
            </ScrollView>

            <View
              style={{ alignItems: "center", marginBottom: 10, marginTop: 10 }}
            >
              <TouchableOpacity
                onPress={triggerApply}
                style={{
                  height: heightPercentageToDP(8),
                  width: widthPercentageToDP(90),
                  backgroundColor: "#000",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 18,
                    fontWeight: "300",
                    color: "#fff"
                  }}
                >
                  {"APPLY"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
    //paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: "center",
    fontSize: 22,
    fontWeight: "300",
    marginBottom: 20
  },
  header: {
    backgroundColor: "#FFF",
    padding: 10,
    justifyContent: "space-between",
    flexDirection: "row"
  },
  headerText: {
    textAlign: "left",
    fontSize: 16,
    fontWeight: "500",
    padding: 10
  },
  content: {
    padding: 20,
    backgroundColor: "#fff"
  },
  filterContent: {
    flex: 1,
    padding: 10,
    backgroundColor: "#fff"
  },
  active: {
    backgroundColor: "rgba(255,255,255,1)"
  },
  inactive: {
    backgroundColor: "rgba(245,252,255,1)"
  },
  selectors: {
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "center"
  },
  selector: {
    backgroundColor: "#F5FCFF",
    padding: 10
  },
  activeSelector: {
    fontWeight: "bold"
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: "500",
    padding: 10
  },
  multipleToggle: {
    flexDirection: "row",
    justifyContent: "center",
    marginVertical: 30,
    alignItems: "center"
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8
  },
  touchableStyle: {
    borderBottomWidth: 0.5,
    borderBottomColor: "#000"
  }
});
