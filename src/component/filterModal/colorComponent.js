import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
export default class FilterCOlors extends Component {
  render() {
    const { colors, clickHandler } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={clickHandler} style={{ marginBottom: 5 }}>
          <View
            style={{
              borderWidth: 0.5,
              borderColor: colors,
              justifyContent: "center",
              alignItems: "center",
              height: heightPercentageToDP(5),
              width: widthPercentageToDP(9),
              marginLeft: 20
            }}
          >
            <View
              style={{
                height: heightPercentageToDP(4),
                width: widthPercentageToDP(8),
                backgroundColor: colors
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
