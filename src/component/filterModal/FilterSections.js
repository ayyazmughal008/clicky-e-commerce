import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { MaterialIcons as ICON } from "@expo/vector-icons";
import { firstLetterCapital } from "../../modules/utilts";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
export default class FilterSection extends Component {
  onPress = () => {
    const { SetIndex, number, currentIndex } = this.props;
    SetIndex(number === currentIndex ? -1 : number);
  };
  RenderExpanded = props => {
    const { expanded, children } = props;
    if (expanded) {
      return <View style={styles.expandedStyle}>{children}</View>;
    }
    return <View />;
  };

  render() {
    const { expanded, title, sectionkey, children } = this.props;
    const { RenderExpanded } = this;
    const container = expanded ? styles.containerStyles : {};
    return (
      <View>
        <TouchableOpacity style={styles.headerStyles} onPress={this.onPress}>
          <Text style={styles.leftText}>{firstLetterCapital(title)}</Text>
          <View style={styles.textBackground}>
            {sectionkey !== "Sort" ? (
              <ICON
                name={expanded ? "remove" : "add"}
                color="#333"
                size={widthPercentageToDP(5)}
              />
            ) : (
              <ICON
                name={expanded ? "expand-less" : "expand-more"}
                color="#333"
                size={widthPercentageToDP(5)}
              />
            )}
          </View>
        </TouchableOpacity>
        <RenderExpanded
          expanded={expanded}
          style={container}
          children={children}
        />
      </View>
    );
  }
}

const styles = {
  containerStyles: {
    paddingVertical: heightPercentageToDP(1)
  },
  headerStyles: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#cccccc",
    height: heightPercentageToDP(8),
    position: "relative",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 10
  },
  textBackground: {
    height: 40,
    width: 40,
    right: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  leftText: {
    fontSize: 18
  },
  textStyle: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
    backgroundColor: "transparent"
  },
  expandedStyle: {
    padding: 0
  }
};
