import React, { Component } from "react";
import { View, TouchableOpacity, ActivityIndicator, Text } from "react-native";
import { Card, Button, Image } from "react-native-elements";
class CategoryCard extends Component {
  render() {
    const {
      clickHandler,
      marginTop,
      borderRadius,
      MarginRight,
      ImageWidth,
      imageHeight,
      marginLeft,
      marginRight,
      resizeMode,
      sourceImage,
      title,
      heading,
      realAmount,
      discountRate,
      discountAmount
    } = this.props;
    return (
      <TouchableOpacity onPress={clickHandler}>
        <Card
          containerStyle={{
            // width: widthPercentageToDP(48),
            // height: heightPercentageToDP(30)
            padding: 5,
            alignItems: "center"
          }}
        >
          {/* <View
            style={{
              flex: 1
            }}
          > */}
          <View style={{ alignItems: "center" }}>
            <Image
              style={{
                marginTop,
                borderRadius,
                marginRight: MarginRight,
                width: ImageWidth,
                height: imageHeight,
                marginLeft,
                marginRight
              }}
              resizeMode={resizeMode}
              source={sourceImage}
              PlaceholderContent={<ActivityIndicator />}
            />
          </View>
          <View style={{ padding: 10, flexDirection: "row" }}>
            <Text
              numberOfLines={3}
              style={{
                fontSize: 13,
                fontFamily: "Metropolis_Regular",
                fontWeight: "500",
                textAlign: "center",
                flex: 1,
                overflow: "hidden"
                // width:50
              }}
            >
              {title}
            </Text>
          </View>
          <Text
            numberOfLines={2}
            style={{
              fontSize: 13,
              fontFamily: "Metropolis_Regular",
              fontWeight: "300",
              color: "#cccccc"
            }}
          >
            {heading}
          </Text>

          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontSize: 13,
                fontFamily: "Metropolis_Regular",
                fontWeight: "300",
                textDecorationLine: "line-through"
              }}
            >
              PKR {realAmount}
            </Text>
            {discountRate !== 0 && (
              <Text
                style={{
                  fontSize: 13,
                  fontFamily: "Metropolis_Regular",
                  fontWeight: "300",
                  marginLeft: 3,
                  color: "green"
                }}
              >
                {discountRate}
                {"% OFF"}
              </Text>
            )}
          </View>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Metropolis_Regular",
                fontWeight: "300"
              }}
            >
              PKR {discountAmount}
            </Text>
            <Button
              title="Add"
              type="clear"
              titleStyle={{ color: "#0bc6d9" }}
            />
          </View>
          {/* </View> */}
        </Card>
      </TouchableOpacity>
    );
  }
}

export default CategoryCard;
