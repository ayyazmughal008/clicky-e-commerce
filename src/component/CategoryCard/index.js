import Davago from "./Davago";
import Omega from "./Omega";
import Shien from "./Shien";
//import PullandBear from './PullandBear'
import React from "react";
import { templateCofig } from "../../config.json";
export default class ParentCategoryItems extends React.Component {
  render() {
    const {
      navigation,
      sourceImage,
      title,
      heading,
      realAmount,
      discountRate,
      discountAmount,
      clickHandler,
      marginTop,
      borderRadius,
      MarginRight,
      ImageWidth,
      imageHeight,
      marginLeft,
      marginRight,
      data
    } = this.props;
    const { listingTemplate } = templateCofig;
    const { listingItems } = listingTemplate;
    const { design } = listingItems;
    return design === "Davago" ? (
      <Davago
        navigation={navigation}
        sourceImage={sourceImage}
        title={title}
        heading={heading}
        realAmount={realAmount}
        discountRate={discountRate}
        discountAmount={discountAmount}
        clickHandler={clickHandler}
        marginTop={marginTop}
        borderRadius={borderRadius}
        MarginRight={MarginRight}
        ImageWidth={ImageWidth}
        imageHeight={imageHeight}
        marginLeft={marginLeft}
        marginRight={marginRight}
      />
    ) : design === "Shien" ? (
      <Shien
        navigation={navigation}
        data={data}
        sourceImage={sourceImage}
        title={title}
        heading={heading}
        realAmount={realAmount}
        discountRate={discountRate}
        discountAmount={discountAmount}
        clickHandler={clickHandler}
        marginTop={marginTop}
        borderRadius={borderRadius}
        MarginRight={MarginRight}
        ImageWidth={ImageWidth}
        imageHeight={imageHeight}
        marginLeft={marginLeft}
        marginRight={marginRight}
      />
    ) : (
      <Omega
        navigation={navigation}
        sourceImage={sourceImage}
        title={title}
        heading={heading}
        realAmount={realAmount}
        discountRate={discountRate}
        discountAmount={discountAmount}
        clickHandler={clickHandler}
        marginTop={marginTop}
        borderRadius={borderRadius}
        MarginRight={MarginRight}
        ImageWidth={ImageWidth}
        imageHeight={imageHeight}
        marginLeft={marginLeft}
        marginRight={marginRight}
      />
    );
  }
}
