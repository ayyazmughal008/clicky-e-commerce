import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  Text,
  Image
} from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
class CategoryCard extends Component {
  render() {
    const {
      clickHandler,
      sourceImage,
      title,
      heading,
      discountAmount
    } = this.props;
    return (
      <TouchableOpacity
        onPress={clickHandler}
        style={{
          justifyContent: "center",
          marginBottom: 20,
          alignItems: "center"
        }}
      >
        <Image
          style={{
            width: widthPercentageToDP(50),
            height: heightPercentageToDP(20),
            marginRight: 4
          }}
          resizeMode="cover"
          source={sourceImage}
        />
        <View style={{ alignItems: "center" }}>
          <View style={{ padding: 15, flexDirection: "row" }}>
            <Text
              numberOfLines={3}
              style={{
                fontSize: 12,
                fontFamily: "Metropolis_Regular",
                fontWeight: "500",
                textAlign: "center",
                flex: 1,
                overflow: "hidden"
                // width:50
              }}
            >
              {title}
            </Text>
          </View>
          <Text
            style={{
              fontSize: 12,
              fontWeight: "300",
              color: "#555",
              textAlign: "center",
              marginTop: 3
            }}
          >
            {heading}
          </Text>
          <Text
            style={{
              fontSize: 12,
              fontWeight: "300",
              color: "#555",
              textAlign: "center",
              marginTop: 3
            }}
          >
            Rs {discountAmount}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CategoryCard;
