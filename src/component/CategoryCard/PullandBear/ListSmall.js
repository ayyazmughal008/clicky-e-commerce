import React from "react";
import { TouchableOpacity, Text } from "react-native";
import { ProgressiveImage } from "../../progressive";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../../modules/MakeMeResponsive";
const Placeholder = require("../../../../assets/images/placeholder.png");
export default props => {
  const { clickHandler, sourceImage, realAmount } = props;
  return (
    <TouchableOpacity
      onPress={clickHandler}
      style={{ justifyContent: "center", marginBottom: 20 }}
    >
      <ProgressiveImage
        style={{
          marginRight: 4,
          marginBottom: 4,
          width: widthPercentageToDP(33),
          height: heightPercentageToDP(20)
        }}
        thumbnailSource={Placeholder}
        resizeMode="cover"
        source={sourceImage}
        //PlaceholderContent={<ActivityIndicator />}
      />
      <Text
        style={{
          fontSize: 13,
          fontFamily: "Metropolis_Regular",
          fontWeight: "300",
          padding: 3,
          textAlign: "center"
        }}
      >
        PKR {realAmount}
      </Text>
    </TouchableOpacity>
  );
};
