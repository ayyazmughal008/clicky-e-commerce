import React, { useState } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Text
} from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../../modules/MakeMeResponsive";
const Placeholder = require("../../../../assets/images/placeholder.png");
export default props => {
  const [ImageReady, setImageReady] = useState(false);
  const {
    sourceImage,
    title,
    realAmount,
    discountAmount,
    disoff,
    ProductID,
    navigation,
    clickHandler
  } = props;
  return (
    <TouchableOpacity
      onPress={clickHandler}
      style={{
        justifyContent: "center",
        marginBottom: 20,
        alignItems: "center"
      }}
    >
      <Image
        onLoad={() => {
          setImageReady(true);
        }}
        source={sourceImage}
        style={{ width: 1, height: 1 }}
        resizeMode="contain"
      />
      <ImageBackground
        style={{
          width: widthPercentageToDP(50),
          height: heightPercentageToDP(30),
          marginRight: widthPercentageToDP(2)
        }}
        resizeMode="cover"
        source={ImageReady ? sourceImage : Placeholder}
        //loadingIndicatorSource={Placeholder}
        //defaultSource={Placeholder}
      >
        {disoff != 0 && (
          <View
            style={{
              position: "absolute",
              height: heightPercentageToDP(3),
              width: widthPercentageToDP(10),
              backgroundColor: "red",
              bottom: "4%",
              left: "0%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                fontSize: 11,
                fontWeight: "300",
                color: "#fff",
                textAlign: "center",
                marginTop: 3
              }}
            >
              {disoff + "%"}
            </Text>
          </View>
        )}
      </ImageBackground>
      <View style={{ alignItems: "center" }}>
        <View style={{ padding: 3, flexDirection: "row" }}>
          <Text
            numberOfLines={1}
            style={{
              fontSize: 12,
              fontFamily: "Metropolis_Regular",
              fontWeight: "500",
              textAlign: "center",
              flex: 1,
              overflow: "hidden"
              // width:50
            }}
          >
            {title}
          </Text>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text
            style={{
              fontSize: 12,
              fontWeight: "300",
              color: "#555",
              textAlign: "center",
              marginTop: 3
            }}
          >
            PKR {discountAmount}
          </Text>
          {disoff != 0 && (
            <Text
              style={{
                fontSize: 12,
                fontWeight: "300",
                color: "red",
                textAlign: "center",
                marginTop: 3,
                marginLeft: widthPercentageToDP(3),
                textDecorationLine: "line-through"
              }}
            >
              {realAmount}
            </Text>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};
