import React, { PureComponent } from "react";
import {
  View,
  ActivityIndicator,
  FlatList,
  Platform,
  Animated
} from "react-native";
import ListThumb from "./ListThumb";
import ListSmall from "./ListSmall";
import ListingHeader from "./ListingHeader";
const PlaceHolder = require("../../../../assets/images/placeholder.png");
import { connect } from "react-redux";
class WholeListSCreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFirst: true,
      offset: 0,
      Distance: 0,
      Enabled: true
    };
  }
  onChange = Setter =>
    this.setState({
      isFirst: Setter
    });

  _keyExtractor = (item, index) => "unique" + index;

  renderFooter = () => {
    const { LOADER } = this.props;
    console.log("ListLoader", LOADER);
    return LOADER ? (
      <View style={{ paddingVertical: 20 }}>
        <ActivityIndicator animating size="large" color="#000000" />
      </View>
    ) : (
      <View />
    );
  };

  render() {
    const { isFirst } = this.state;
    //ModalOpener={OpenSearchModal}
    const { ModalOpener, navigation, clearfun, SearchString } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <ListingHeader
          switchfunc={isFirst => {
            this.setState({
              isFirst
            });
          }}
          clearfunc={clearfun}
          searchFunc={ModalOpener}
          func={this.onChange}
          navigation={navigation}
          SearchString={SearchString}
        />
        <View style={{ flex: 1 }}>
          {isFirst ? (
            <FlatList
              scrollEventThrottle={1}
              ListFooterComponent={this.renderFooter}
              key={"f1Flatlist"}
              style={{ marginLeft: 5, marginRight: 5 }}
              keyExtractor={this._keyExtractor}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              onEndReachedThreshold={0.1}
              onEndReached={this.props.onEndReached}
              refreshing={this.props.refreshing}
              extraData={this.props.extraData}
              data={this.props.data}
              //onRefresh={this.props.onRefresh}
              renderItem={({ item }) => {
                const { price, mrp } = item.variants[0];
                const DisOff = (((mrp - price) / mrp) * 100).toFixed(0);
                const { _id, imgUrls, imgA, name, brandName } = item;
                const { navigation } = this.props;
                const { length } = imgA;
                return (
                  <ListThumb
                    navigation={navigation}
                    sourceImage={
                      length !== 0 ? { uri: imgA[0].medium } : PlaceHolder
                    }
                    title={name}
                    heading={brandName}
                    realAmount={mrp.toFixed(0)}
                    discountAmount={price.toFixed(0)}
                    disoff={DisOff}
                    clickHandler={() =>
                      navigation.navigate("ProductPage", {
                        name: name,
                        ProductID: _id,
                        DisOff: DisOff,
                        price: price.toFixed(0),
                        mrp: mrp.toFixed(0)
                      })
                    }
                  />
                );
              }}
            />
          ) : (
            <FlatList
              scrollEventThrottle={1}
              ListFooterComponent={this.renderFooter}
              key={"f2Flatlist"}
              style={{ marginLeft: 5, marginRight: 5 }}
              keyExtractor={this._keyExtractor}
              numColumns={3}
              showsVerticalScrollIndicator={false}
              onEndReachedThreshold={0.1}
              onEndReached={this.props.onEndReached}
              refreshing={this.props.refreshing}
              extraData={this.props.extraData}
              data={this.props.data}
              //onRefresh={this.props.onRefresh}
              renderItem={({ item }) => {
                const { price, mrp } = item.variants[0];
                const DisOff = (((mrp - price) / mrp) * 100).toFixed(0);
                const { _id, imgUrls, imgA, name, brandName } = item;
                const { navigation } = this.props;
                const { length } = imgA;
                return (
                  <ListSmall
                    navigation={navigation}
                    sourceImage={
                      length !== 0 ? { uri: imgA[0].medium } : PlaceHolder
                    }
                    realAmount={price.toFixed(0)}
                    clickHandler={() =>
                      navigation.navigate("ProductPage", {
                        name: name,
                        ProductID: _id,
                        DisOff: DisOff,
                        price: price.toFixed(0),
                        mrp: mrp.toFixed(0)
                      })
                    }
                  />
                );
              }}
            />
          )}
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  LOADER: state.APPSTATE.ListLoader
});
export default connect(
  mapStateToProps,
  {}
)(WholeListSCreen);
