import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
  Text
} from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../../modules/MakeMeResponsive';

export default props => {
  const [MORE, setMORE] = useState(false);
  const { switchfunc, searchFunc, clearfunc, SearchString } = props;
  return (
    <View
      style={{
        height:
          Platform.OS == 'ios'
            ? heightPercentageToDP(8)
            : heightPercentageToDP(6),
        width: widthPercentageToDP(100),
        flexDirection: 'row',
        alignItems: 'center',
        borderTopWidth: widthPercentageToDP(0.1),
        borderTopColor: '#fafafa'
      }}
    >
      <View
        style={{
          height:
            Platform.OS == 'ios'
              ? heightPercentageToDP(8)
              : heightPercentageToDP(6),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around'
        }}
      >
        <TouchableOpacity
          style={{
            height:
              Platform.OS == 'ios'
                ? heightPercentageToDP(8)
                : heightPercentageToDP(6),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingLeft: widthPercentageToDP(2)
          }}
          onPress={searchFunc}
        >
          <Icon name="search" color="#000" size={widthPercentageToDP(6)} />
          <TextInput
            editable={false}
            // onChangeText={searchFor => this.setState({ searchFor })}
            value={SearchString}
            style={{
              width: widthPercentageToDP(55),
              marginLeft: widthPercentageToDP(2)
            }}
            placeholder="I'm looking for"
            autoCapitalize="none"
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginLeft: widthPercentageToDP(2) }}
          onPress={clearfunc}
        >
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: '#000'
            }}
          >
            {'CLEAR'}
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={{
          position: 'absolute',
          right: '5%',
          width: widthPercentageToDP(6),
          height: heightPercentageToDP(4)
        }}
        onPress={() => {
          switchfunc(MORE);
          setMORE(!MORE);
          //this.MORE = !MORE;
        }}
      >
        <Image
          style={{
            width: widthPercentageToDP(6),
            height: heightPercentageToDP(4)
          }}
          source={
            MORE
              ? require('../../../../assets/images/single_icon.png')
              : require('../../../../assets/images/multi_icons2.png')
          }
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  );
};
