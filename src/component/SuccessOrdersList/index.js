import React from "react";
import { View, Text, Image } from "react-native";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import { TimeStatus } from "../../modules/utilts";
export default class OrderSuccessList extends React.Component {
  render() {
    const { image, status, title, size, qty, price } = this.props;
    return (
      <View
        style={{
          width: widthPercentageToDP(80),
          height: heightPercentageToDP(20),
          flexDirection: "row",
          padding: widthPercentageToDP(4.3),
          justifyContent: "center"
          //alignItems: 'center'
        }}
      >
        <View
          style={{
            borderWidth: widthPercentageToDP(0.2),
            borderColor: "#cccccc",
            borderRadius: widthPercentageToDP(1)
          }}
        >
          <Image
            style={{
              height: widthPercentageToDP(25),
              width: widthPercentageToDP(20),
              marginRight: widthPercentageToDP(3)
            }}
            resizeMode="contain"
            source={{ uri: image }}
          />
          <View
            style={{
              position: "absolute",
              bottom: 0,
              width: "100%",
              height: heightPercentageToDP(3.5),
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#D3D3D3"
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(3),
                color: "#696969",
                fontWeight: "300"
              }}
            >
              {TimeStatus(status) !== ""
                ? "Arrives " + TimeStatus(status)
                : TimeStatus(status)}
            </Text>
          </View>
        </View>
        <View
          style={{
            width: widthPercentageToDP(45),
            marginLeft: widthPercentageToDP(4)
          }}
        >
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: "#696969",
              fontWeight: "300"
            }}
            numberOfLines={2}
          >
            {title}
          </Text>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: "#cccccc",
              fontWeight: "300"
            }}
            numberOfLines={2}
          >
            Size: {size}
          </Text>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: "#cccccc",
              fontWeight: "300"
            }}
            numberOfLines={2}
          >
            Quantity: {qty}
          </Text>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: "#696969",
              fontWeight: "300"
            }}
          >
            PKR {price}
          </Text>
        </View>
      </View>
    );
  }
}
