import React, { Component } from "react";
import { TouchableOpacity, ActivityIndicator, Text } from "react-native";
import { Image } from "react-native-elements";

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { imageStatus: true };
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.clickHandler}>
        <Image
          style={{
            marginTop: this.props.marginTop,
            borderRadius: this.props.borderRadius,
            marginRight: this.props.MarginRight,
            width: this.props.ImageWidth,
            height: this.props.imageHeight,
            marginLeft: this.props.marginLeft,
            marginRight: this.props.marginRight
          }}
          resizeMode="contain"
          source={this.props.sourceImage}
          PlaceholderContent={<ActivityIndicator />}
          // onLoadStart={() => this.setState({ imageStatus: true })}
          // onLoadEnd={() => this.setState({ imageStatus: false })}
        />
        <Text
          style={{
            fontSize: 15,
            fontFamily: "Metropolis_Regular",
            fontWeight: "300",
            padding: 5,
            textAlign: "center",
            marginBottom: 5
          }}
        >
          {this.props.text}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default ImageComponent;
