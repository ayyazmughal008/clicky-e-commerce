import React, { Component } from "react";
import { Animated, Easing } from "react-native";
import PropTypes from "prop-types";
/*
USAGE:
  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
      <RotatingView
          style={{height: 200, width: 200,}}
          duration={3000}
          onFinishedAnimating={( (status) => {console.log(status)} )}
      >
          <Image
              style={{height:'100%', width: '100%', resizeMode: 'contain'}}
              resizeMode='contain'
              source={require("image.png")}/>
      </RotatingView>
  </View>
*/

export default class RotatingView extends Component {
  static propTypes = Object.assign({}, Animated.View.propTypes, {
    duration: PropTypes.number, //How long
    toValue: PropTypes.number, //How much rotation, eg. 0.5 will rotate 180deg
    onFinishedAnimating: PropTypes.func, //What to do when animation finishes, see Aminated docs
    loop: PropTypes.bool // Should animation loop?
  });
  static defaultProps = {
    duration: 2000,
    onFinishedAnimating: () => {
      console.log("Finished animation");
    },
    loop: false,
    toValue: 1
  };
  state = {
    spinValue: new Animated.Value(0),
    toValue: this.props.toValue
  };

  componentDidMount() {
    this.animate();
  }

  animate = () => {
    Animated.timing(
      this.state.spinValue, // The animated value to drive
      {
        toValue: this.state.toValue, // Animate to 360/value
        duration: this.props.duration || 2000, // Make it take a while
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start(this.handleFinishedAnimating); // Starts the animation
  };

  handleFinishedAnimating = () => {
    const { loop, onFinishedAnimating, toValue } = this.props;
    loop
      ? this.setState(
          {
            spinValue: new Animated.Value(this.state.toValue),
            toValue: this.state.toValue + toValue
          },
          this.animate
        )
      : onFinishedAnimating();
  };

  render() {
    let spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"]
    });
    return (
      <Animated.View
        style={{
          ...this.props.style,
          transform: [{ rotate: spin }] // Bind rotation to animated value
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}
