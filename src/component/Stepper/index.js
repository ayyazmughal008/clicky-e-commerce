import React, { PureComponent } from "react";
import { View, Text } from "react-native";
class Stepper extends PureComponent {
  render() {
    const {
      stepOneBgColor,
      stepOnetextColor,
      stepTwoBgColor,
      stepTwotextColor,
      stepThreeBgColor,
      stepThreetextColor,
      stepFourBgColor,
      stepFourtextColor
    } = this.props;
    return (
      <View>
        <View style={{ height: "5%" }} />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginHorizontal: 30
          }}
        >
          <View
            style={{
              width: "100%",
              height: "1%",
              borderWidth: 0.5,
              borderColor: "#000"
            }}
          />

          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              borderWidth: 1.5,
              borderColor: "#000",
              backgroundColor: stepOneBgColor,
              position: "absolute",
              left: "0%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "300",
                color: stepOnetextColor
              }}
            >
              1
            </Text>
          </View>

          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              borderWidth: 1.5,
              borderColor: "#000",
              backgroundColor: stepTwoBgColor,
              position: "absolute",
              left: "30%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "300",
                color: stepTwotextColor
              }}
            >
              2
            </Text>
          </View>
          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              borderWidth: 1.5,
              borderColor: "#000",
              backgroundColor: stepThreeBgColor,
              position: "absolute",
              right: "30%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "300",
                color: stepThreetextColor
              }}
            >
              3
            </Text>
          </View>
          <View
            style={{
              height: 30,
              width: 30,
              borderRadius: 15,
              borderWidth: 1.5,
              borderColor: "#000",
              backgroundColor: stepFourBgColor,
              position: "absolute",
              right: "0%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "300",
                color: stepFourtextColor
              }}
            >
              4
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            marginLeft: 35,
            marginRight: 30,
            justifyContent: "space-between",
            marginTop: 20
          }}
        >
          <View>
            <Text>Bag</Text>
          </View>
          <View>
            <Text>Delivery</Text>
          </View>
          <View>
            <Text>Payment</Text>
          </View>
          <View>
            <Text>Done!</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default Stepper;
