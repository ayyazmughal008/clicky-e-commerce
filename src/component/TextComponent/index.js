import React, { PureComponent } from "react";
import { View, Text } from "react-native";
export default class TextComponent extends PureComponent {
  render() {
    const { navigation, text1, makeMeBold } = this.props;
    return (
      <View
        style={{
          marginTop: 30,
          marginLeft: 10,
          borderBottomWidth: 0.5,
          borderColor: "#cccccc"
        }}
      >
        <Text
          style={{
            fontSize: 19,
            fontFamily: "Metropolis_Regular",
            fontWeight: makeMeBold ? "900" : "300",
            padding: 3,
            marginLeft: 10
          }}
        >
          {text1}
        </Text>
      </View>
    );
  }
}
