import React from "react";
import { View, TouchableOpacity, Text, ActivityIndicator } from "react-native";
import { widthPercentageToDP } from "../../modules/MakeMeResponsive";
import { ProgressiveImage } from "../progressive";
const Placeholder = require("../../../assets/images/placeholder.png");
export default class RoundImages extends React.Component {
  render() {
    const { text, imageUri, clickHandler } = this.props;
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          marginLeft: widthPercentageToDP(2),
          marginRight: widthPercentageToDP(2)
        }}
      >
        <TouchableOpacity
          onPress={clickHandler}
          style={{
            height: widthPercentageToDP(20),
            width: widthPercentageToDP(20),
            borderRadius: widthPercentageToDP(10)
          }}
        >
          <ProgressiveImage
            style={{
              height: widthPercentageToDP(20),
              width: widthPercentageToDP(20),
              borderRadius: widthPercentageToDP(10)
            }}
            thumbnailSource={Placeholder}
            resizeMode="cover"
            source={imageUri}
          />
        </TouchableOpacity>
        <Text
          style={{
            color: "#b2b6b5",
            fontSize: widthPercentageToDP(3.5),
            fontWeight: "300"
          }}
        >
          {text}
        </Text>
      </View>
    );
  }
}
