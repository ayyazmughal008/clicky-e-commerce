import React, { Component } from "react";
import { View, Dimensions,Text } from "react-native";
//import { Text } from "native-base";
// import { connect } from "react-redux";
// import { getOrderStatus } from "../../redux/actions";
// import moment from "moment";

class OrderStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: true
    };
  }

  // componentWillMount() {
  //   getOrderStatus(this.props.orderID, "");
  // }

  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });

  convertDate(num) {
    var newDate = moment(num).format("ll");
    console.log(newDate);
    return newDate;
  }

  render() {
    const navigate = this.props.navigattion;
    //const { ordersStatus } = this.props.user;

    // if (
    //   ordersStatus.response &&
    //   ordersStatus.response === "No Record Found"
    // ) {
    //   return (
    //     <Modal
    //       animationType={"slide"}
    //       transparent={true}
    //       visible={this.state.isModalVisible}
    //       onRequestClose={() => {
    //         console.log("Modal close");
    //       }}
    //     >
    //       <View
    //         style={{
    //           flex: 1,
    //           flexDirection: "column",
    //           justifyContent: "flex-end",
    //           alignItems: "center",
    //           alignContent: "center"
    //         }}
    //       >
    //         <View
    //           style={{
    //             height: 100,
    //             width: dwidth,
    //             backgroundColor: "#fff",
    //             alignItems: "center",
    //             shadowRadius: 1,
    //             borderRadius: 10
    //           }}
    //         >
    //           <Text
    //             style={{
    //               fontSize: 16,
    //               fontWeight: "200",
    //               color: "green",
    //               marginTop: 20,
    //               marginBottom: 10
    //             }}
    //           >
    //             No Record Found.!
    //           </Text>

    //           <TouchableHighlight
    //             transparent
    //             style={{
    //               backgroundColor: "#00c497",
    //               height: 40,
    //               width: 40,
    //               borderRadius: 100,
    //               justifyContent: "center",
    //               alignItems: "center"
    //             }}
    //             onPress={() => {
    //               this._hideModal();
    //             }}
    //           >
    //             <Text
    //               style={{
    //                 fontSize: 15,
    //                 fontWeight: "700",
    //                 color: "#fff",
    //                 textAlign: "center",
    //                 textAlign: "center"
    //               }}
    //             >
    //               OK
    //             </Text>
    //           </TouchableHighlight>
    //         </View>
    //       </View>
    //     </Modal>
    //   );
    // } else if (
    //   ordersStatus.current_order_status === "Received" ||
    //   ordersStatus.current_order_status === "Placed" ||
    //   ordersStatus.current_order_status === "Packed" ||
    //   ordersStatus.current_order_status === "Dispatched" ||
    //   ordersStatus.current_order_status === "Received" ||
    //   ordersStatus.current_order_status === "Delivered" ||
    //   ordersStatus.current_order_status === "Returned" ||
    //   ordersStatus.current_order_status === "Refused"
    // ) {
    return (
      <View
        style={{
          justifyContent: "flex-start",
          backgroundColor: "#fff",
          marginBottom: 3
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={{ justifyContent: "center", marginLeft: 20 }}>
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 100 / 2,
                backgroundColor: "#3be067",
                // ordersStatus.timeline.ordered
                //   ? "#3be067"
                //   : "#f2f2f2",
                borderColor: "#3be067"
              }}
            />
            <View
              style={{
                height: 27,
                marginLeft: 7,
                borderLeftWidth: 2,
                borderLeftColor: "#000"
              }}
            />
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 100 / 2,
                backgroundColor: "#3be067",
                // ordersStatus.timeline.processed
                //   ? "#3be067"
                //   : "#f2f2f2",
                borderColor: "#3be067"
              }}
            />
            <View
              style={{
                height: 27,
                marginLeft: 7,
                borderLeftWidth: 2,
                borderLeftColor: "#000"
              }}
            />
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 100 / 2,
                backgroundColor: "#3be067",
                // ordersStatus.timeline.packed
                //   ? "#3be067"
                //   : "#f2f2f2",
                borderColor: "#3be067"
              }}
            />
            <View
              style={{
                height: 27,
                marginLeft: 7,
                borderLeftWidth: 2,
                borderLeftColor: "#000"
              }}
            />
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 100 / 2,
                backgroundColor: "#3be067",
                // ordersStatus.timeline.shipped
                //   ? "#3be067"
                //   : "#f2f2f2",
                borderColor: "#3be067"
              }}
            />
            <View
              style={{
                height: 27,
                marginLeft: 7,
                borderLeftWidth: 2,
                borderLeftColor: "#000"
              }}
            />
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 100 / 2,
                backgroundColor: "#3be067",
                // ordersStatus.timeline.delivered
                //   ? "#3be067"
                //   : "#f2f2f2",
                borderColor: "#3be067"
              }}
            />
          </View>

          <View style={{ justifyContent: "flex-start", marginTop: 9 }}>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                marginBottom: 2,
                fontSize: 14,
                fontWeight: "500"
              }}
            >
              Ordered{" "}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                fontSize: 12,
                fontWeight: "300",
                marginBottom: 2
              }}
            >
              1 jan 2019
              {/* {ordersStatus.timeline.ordered
                  ? this.convertDate(ordersStatus.timeline.ordered)
                  : ""}{" "} */}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                marginBottom: 2,
                marginTop: 2,
                fontSize: 14,
                fontWeight: "500"
              }}
            >
              Processed{" "}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                fontSize: 12,
                fontWeight: "300",
                marginBottom: 2
              }}
            >
              1 jan 2019
              {/* {ordersStatus.timeline.processed
                  ? this.convertDate(ordersStatus.timeline.processed)
                  : ""}{" "} */}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                marginBottom: 2,
                marginTop: 2,
                fontSize: 14,
                fontWeight: "500"
              }}
            >
              Packed{" "}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                fontSize: 12,
                fontWeight: "300",
                marginBottom: 2
              }}
            >
              1 jan 2019
              {/* {ordersStatus.timeline.packed
                  ? this.convertDate(ordersStatus.timeline.packed)
                  : ""}{" "} */}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                marginBottom: 2,
                marginTop: 2,
                fontSize: 14,
                fontWeight: "500"
              }}
            >
              Shipped{" "}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                fontSize: 12,
                fontWeight: "300",
                marginBottom: 2
              }}
            >
              1 jan 2019
              {/* {ordersStatus.timeline.shipped
                  ? this.convertDate(ordersStatus.timeline.shipped)
                  : ""}{" "} */}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                marginBottom: 2,
                marginTop: 2,
                fontSize: 14,
                fontWeight: "500"
              }}
            >
              Delivered{" "}
            </Text>
            <Text
              style={{
                marginLeft: 30,
                textAlign: "left",
                fontSize: 12,
                fontWeight: "300",
                marginBottom: 2
              }}
            >
              1 jan 2019
              {/* {ordersStatus.timeline.delivered
                  ? this.convertDate(ordersStatus.timeline.delivered)
                  : ""} */}
            </Text>
          </View>
        </View>
      </View>
      //   );
      // } else if (ordersStatus.current_order_status === "Canceled") {
      //   return (
      //     <View
      //       style={{
      //         justifyContent: "flex-start",
      //         backgroundColor: "#fff",
      //         marginBottom: 3
      //       }}
      //     >
      //       <View style={{ flexDirection: "row" }}>
      //         <View style={{ justifyContent: "center", paddingLeft: 10 }}>
      //           <View
      //             style={{
      //               width: 15,
      //               height: 15,
      //               borderRadius: 100 / 2,
      //               backgroundColor: "#3be067"
      //             }}
      //           />
      //           <View
      //             style={{
      //               height: 30,
      //               marginLeft: 7,
      //               borderLeftWidth: 2,
      //               borderLeftColor: "#000"
      //             }}
      //           />
      //           <View
      //             style={{
      //               width: 15,
      //               height: 15,
      //               borderRadius: 100 / 2,
      //               backgroundColor: "#F80C4B"
      //             }}
      //           />
      //         </View>

      //         <View style={{ justifyContent: "flex-start", marginTop: 9 }}>
      //           <Text
      //             style={{
      //               marginLeft: 30,
      //               textAlign: "left",
      //               marginBottom: 2,
      //               fontSize: 14,
      //               fontWeight: "500"
      //             }}
      //           >
      //             Ordered{" "}
      //           </Text>
      //           <Text
      //             style={{
      //               marginLeft: 30,
      //               textAlign: "left",
      //               fontSize: 12,
      //               fontWeight: "300",
      //               marginBottom: 2
      //             }}
      //           >
      //             {ordersStatus.timeline.ordered
      //               ? this.convertDate(ordersStatus.timeline.ordered)
      //               : ""}{" "}
      //           </Text>
      //           <Text
      //             style={{
      //               marginLeft: 30,
      //               marginTop: 2,
      //               textAlign: "left",
      //               marginBottom: 2,
      //               fontSize: 14,
      //               fontWeight: "500"
      //             }}
      //           >
      //             Cancelled{" "}
      //           </Text>
      //           <Text
      //             style={{
      //               marginLeft: 30,
      //               textAlign: "left",
      //               fontSize: 12,
      //               fontWeight: "300",
      //               marginBottom: 2
      //             }}
      //           >
      //             {ordersStatus.timeline.canceled
      //               ? this.convertDate(ordersStatus.timeline.canceled)
      //               : ""}{" "}
      //           </Text>
      //         </View>
      //       </View>
      //     </View>
      //   );
      // } else {
      //   return (
      //     <View>
      //       <Text>Hi am ayyaz</Text>
      //     </View>
      //   );
      // }
      // return (
      //   <View>
      //     <Text>Hi am ayyaz</Text>
      //   </View>
      // );
    );
  }
}
// const mapStateToProps = state => ({
//   user: state.user,
//   loader: state.loader
// });

// export default connect(mapStateToProps, { getOrderStatus })(OrderStatus);

export default OrderStatus;
