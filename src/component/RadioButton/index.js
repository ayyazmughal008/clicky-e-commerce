'use strict';
import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { widthPercentageToDP } from '../../modules/MakeMeResponsive';

export default ({ options, onChange, selected }) => (
  <View
    style={{
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      flex: 1
    }}
  >
    {options.map((item, index) => {
      const { text, value } = item;
      return (
        <TouchableOpacity
          key={'radio' + index}
          activeOpacity={1}
          onPress={() => {
            onChange(value);
          }}
          style={{ flexDirection: 'row', alignItems: 'center' }}
        >
          <View
            style={[
              {
                height: widthPercentageToDP(6),
                width: widthPercentageToDP(6),
                borderRadius: widthPercentageToDP(3),
                borderWidth: widthPercentageToDP(0.3),
                borderColor: '#000',
                alignItems: 'center',
                justifyContent: 'center'
              }
            ]}
          >
            <View
              style={{
                height: widthPercentageToDP(4),
                width: widthPercentageToDP(4),
                borderRadius: widthPercentageToDP(2),
                backgroundColor:
                  selected === value ? 'rgba(0, 0, 0, 1)' : 'rgba(0, 0, 0, 0)'
              }}
            />
          </View>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              marginLeft: widthPercentageToDP(2),
              color: '#000'
            }}
          >
            {text}
          </Text>
        </TouchableOpacity>
      );
    })}
  </View>
);
