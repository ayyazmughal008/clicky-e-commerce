import React from 'react';
import { View, Text, Image } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { MaterialIcons as Icon } from '@expo/vector-icons';
export default class OrderDetailList extends React.Component {
  render() {
    // startDate={StartAt.dDate}
    // udate={UpdateAt.dDate}
    const {
      image,
      discountPrice,
      orginalPrice,
      title,
      size,
      qty,
      bgStatusColor,
      statusIcon,
      status,
      udate,
      startDate
    } = this.props;
    return (
      <View
        style={{
          width: widthPercentageToDP(90),
          height: heightPercentageToDP(30),
          //padding: widthPercentageToDP(1.5),
          backgroundColor: '#fff',
          marginBottom: heightPercentageToDP(2)
        }}
      >
        <View
          style={{
            width: widthPercentageToDP(90),
            height: heightPercentageToDP(20),
            flexDirection: 'row',
            //padding: widthPercentageToDP(3),
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Image
            style={{
              height: heightPercentageToDP(20),
              width: widthPercentageToDP(25),
              marginRight: widthPercentageToDP(3)
            }}
            resizeMode="contain"
            source={{ uri: image }}
          />
          <View
            style={{
              width: widthPercentageToDP(45),
              marginLeft: widthPercentageToDP(4)
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                color: '#696969',
                fontWeight: '300'
              }}
              numberOfLines={2}
            >
              {title}
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                color: '#b8b8b8',
                fontWeight: '300'
              }}
              numberOfLines={2}
            >
              Size: {size}
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                color: '#b8b8b8',
                fontWeight: '300'
              }}
              numberOfLines={2}
            >
              Quantity: {qty}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  color: '#000',
                  fontWeight: '500'
                }}
              >
                PKR {discountPrice}
              </Text>
              {discountPrice < orginalPrice && (
                <Text
                  style={{
                    fontSize: widthPercentageToDP(4),
                    color: '#b8b8b8',
                    fontWeight: '300',
                    marginLeft: widthPercentageToDP(1.5),
                    textDecorationLine: 'line-through'
                  }}
                >
                  PKR {orginalPrice}
                </Text>
              )}
            </View>
          </View>
        </View>
        <View
          style={{
            //padding: widthPercentageToDP(3),
            marginTop: heightPercentageToDP(1)
          }}
        >
          <View
            style={{
              height: heightPercentageToDP(6),
              width: widthPercentageToDP(50),
              borderWidth: widthPercentageToDP(0.3),
              borderColor: '#b8b8b8',
              borderRadius: widthPercentageToDP(8),
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'center'
            }}
          >
            <View
              style={{
                height: widthPercentageToDP(8),
                width: widthPercentageToDP(8),
                borderRadius: widthPercentageToDP(8) / 2,
                backgroundColor: bgStatusColor,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Icon
                name={statusIcon}
                color="#fff"
                size={widthPercentageToDP(5)}
              />
            </View>
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                color: '#b8b8b8',
                fontWeight: '300',
                marginLeft: widthPercentageToDP(0.5)
              }}
            >
              {status} {'on'} {udate}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
