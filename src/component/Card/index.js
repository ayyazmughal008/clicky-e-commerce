import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Card } from "react-native-elements";

class CardComponent extends Component {
  render() {
    const navigate = this.props.navigattion;
    return (
      <Card>
        <View
          style={{
            flex: 1,
            alignItems: "center"
          }}
        >
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Metropolis_Regular",
              fontWeight: "500"
            }}
          >
            {this.props.title}
          </Text>
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Metropolis_Regular",
              fontWeight: "300",
              color: "#cccccc"
            }}
          >
            {this.props.heading}
          </Text>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Image
              style={{
                marginTop: this.props.marginTop,
                borderRadius: this.props.borderRadius,
                marginRight: this.props.MarginRight,
                width: this.props.ImageWidth,
                height: this.props.imageHeight,
                marginLeft: this.props.marginLeft,
                marginRight: this.props.marginRight
              }}
              source={this.props.sourceImage}
              resizeMode="contain"
            />
            <View
              style={{
                width: 40,
                height: 21,
                borderRadius: 3,
                backgroundColor: "green"
              }}
            >
              <Text
                style={{
                  fontSize: 13,
                  fontWeight: "400",
                  color: "#fff",
                  textAlign: "center"
                }}
              >
                {this.props.rating}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Metropolis_Regular",
                fontWeight: "500"
              }}
            >
              {this.props.amount}
            </Text>
            <Text
              style={{
                fontSize: 13,
                fontFamily: "Metropolis_Regular",
                fontWeight: "400",
                marginLeft: 3,
                textDecorationLine: "line-through"
              }}
            >
              {this.props.discountRate}
            </Text>
          </View>
        </View>
      </Card>
    );
  }
}

export default CardComponent;
