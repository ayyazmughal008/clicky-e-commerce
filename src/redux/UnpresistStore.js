import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import REDUCER from "./reducer";
const Store = createStore(REDUCER, {}, applyMiddleware(thunk));

export default Store;
