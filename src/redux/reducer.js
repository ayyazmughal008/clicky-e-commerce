import { combineReducers } from 'redux';
import { ActionType } from './action';
//========================================[X    -_-    X]===========================
const initialUserState = {
  isLoggedIn: false,
  verified: false,
  userDetailAvailable: false,
  checkOutStarted: false,
  quickbuy: null,
  loginError: null,
  signupError: null,
  lastOrder: null,
  orderDetail: null,
  cart: null,
  userId: '',
  profileId: '',
  token: '',
  customerEmail: '',
  email: '',
  firstName: '',
  lastName: '',
  gender: 'male',
  customerPicUrl: '',
  profileDP: '',
  userProfile: '',
  userOfficeAddress: '',
  wishListLength: 0,
  currentAddressIndex: 0,
  orders_qty: 0,
  address: [],
  userHomeAddress: [],
  wishlist: [],
  userOrders: []
};

const USERReducer = (state = initialUserState, action) => {
  if (action.type === ActionType.ADDorREMOVEfromWISHLIST) {
    console.log('wishlist update');
    return {
      ...state,
      wishlist: action.payload,
      wishListLength: action.payload.length
    };
  }
  if (action.type === ActionType.LOG_OUT) {
    console.log('Loging Out');
    return {
      ...initialUserState
    };
  }
  if (
    action.type === ActionType.ADDorREMOVEfromCart ||
    action.type === ActionType.EMPTY_CART ||
    action.type === ActionType.GET_CART
  ) {
    return { ...state, cart: action.payload };
  }
  if (action.type === ActionType.ADD_USER_With_SIGNUP) {
    console.log('USER updated Through SignUp');
    return {
      ...state,
      isLoggedIn: true,
      token: action.payload.token,
      gender: action.payload.user.gender,
      email: action.payload.user.email,
      lastName: action.payload.user.lastName,
      firstName: action.payload.user.firstName
    };
  }
  if (action.type === ActionType.ADD_USER_With_SIGNIN) {
    console.log('USER updated Through SignIn');
    const {
      verified,
      _id,
      gender,
      email,
      lastName,
      firstName,
      phone
    } = action.payload.user;
    return {
      ...state,
      verified,
      isLoggedIn: true,
      userId: _id,
      token: action.payload.token,
      gender,
      email,
      phone,
      lastName,
      firstName
    };
  }
  if (action.type === ActionType.GET_WISHLIST) {
    console.log('Wislist Is Here');
    return {
      ...state,
      wishlist: action.payload,
      wishListLength: action.payload.length
    };
  }
  if (action.type === ActionType.GET_ADDRESS) {
    console.log('Address Is Here');
    return {
      ...state,
      userDetailAvailable: action.payload.length !== 0 ? true : false,
      address: action.payload
    };
  }
  if (action.type === ActionType.CHOOSE_ADDRESS) {
    return {
      ...state,
      currentAddressIndex: action.payload
    };
  }
  if (action.type === ActionType.VERIFY_PHONE) {
    return {
      ...state,
      verified: action.payload
    };
  }
  if (action.type === ActionType.POST_ADDRESS) {
    console.log('New Address Is Here', action.payload);
    const AddressArr = state.address;
    AddressArr.splice(0, 0, action.payload);
    return {
      ...state,
      userDetailAvailable: true,
      address: AddressArr,
      currentAddressIndex: 0
    };
  }
  if (action.type === ActionType.GET_ORDER) {
    console.log('Order getted');
    return {
      ...state,
      orders_qty: action.payload.length,
      userOrders: action.payload
    };
  }
  if (action.type === ActionType.POST_ORDER) {
    console.log('Order Pushed');
    const OrderArr = state.userOrders;
    OrderArr.splice(0, 0, action.payload);
    return {
      ...state,
      orders_qty: OrderArr.length,
      userOrders: OrderArr
    };
  }
  if (action.type === ActionType.GET_CART) {
    console.log('Cart Update ');
    return {
      ...state,
      orders_qty: OrderArr.length,
      userOrders: OrderArr
    };
  }

  return state;
};
//========================================================

const InitialAppState = {
  AuthLoading: false,
  FetchingAndGettingLoading: false,
  ListLoader: false
};
const AppStateReducer = (state = InitialAppState, { type, payload }) => {
  if (type === ActionType.AUTH_LOADING) {
    return {
      ...state,
      AuthLoading: payload
    };
  }
  if (type === ActionType.GET_POST_LOADING) {
    return {
      ...state,
      FetchingAndGettingLoading: payload
    };
  }
  if (type === ActionType.GET_LIST_LOADING) {
    return {
      ...state,
      ListLoader: payload
    };
  }
  return state;
};

//========================================================

const InitialMegaMenuData = {
  WholeMegaMenu: [],
  DrawerContentData: [],
  PayMethods: [],
  promotionData: null
};
const MegaMenuReducer = (state = InitialMegaMenuData, { type, payload }) => {
  switch (type) {
    case ActionType.LOAD_MEGAMENU: {
      const { WholeMegaMenu, DrawerContentData } = payload;
      return {
        ...state,
        WholeMegaMenu,
        DrawerContentData
      };
    }
    case ActionType.GET_PAYMETHODS: {
      return {
        ...state,
        PayMethods: payload
      };
    }
    case ActionType.GET_PROMOTION: {
      return {
        ...state,
        promotionData: payload
      };
    }
    default:
      return state;
  }

  // const { type, payload } = action;
  // if (type === ActionType.LOAD_MEGAMENU) {
  //   const { WholeMegaMenu, DrawerContentData } = payload;
  //   return {
  //     ...state,
  //     WholeMegaMenu,
  //     DrawerContentData
  //   };
  // }
  // if (type === ActionType.GET_PAYMETHODS) {
  //   return {
  //     ...state,
  //     PayMethods: payload
  //   };
  // }
  // return state;
};

//========================================================
const InitialExpoData = {
  ExpoToken: '',
  tokenRegisteredInAll: false,
  tokenRegisteredInProfile: false
};
const ExpoTokenReducer = (state = InitialExpoData, action) => {
  const { type, payload } = action;
  if (type === ActionType.LOAD_EXPO_TOKEN) {
    const { ExpoToken } = payload;
    return {
      ...state,
      ExpoToken
    };
  }
  if (type === ActionType.EXPO_AUTHENTICATE_TIME_TOKEN_SENT) {
    const { tokenRegisteredInProfile } = payload;
    return {
      ...state,
      tokenRegisteredInProfile
    };
  }
  if (type === ActionType.EXPO_FIRST_TIME_TOKEN_SENT) {
    const { tokenRegisteredInAll } = payload;
    return {
      ...state,
      tokenRegisteredInAll
    };
  }
  return state;
};
//========================================================
export default combineReducers({
  USER: USERReducer,
  APPSTATE: AppStateReducer,
  MEGAMENU: MegaMenuReducer,
  ExpoTokenState: ExpoTokenReducer
});
