import { APIFuncs } from '../modules/APIs';
import {
  prefetchImage,
  ExpoTokenExtractor,
  InfoLogger
} from '../modules/utilts';
import { Alert } from 'react-native';
import * as Facebook from 'expo-facebook';
import registerForPushNotificationsAsync from './../modules/FCM_Notification';
const JWT = require('jwt-decode');
//================Action Types Constants
const LOADING = 'LOADING',
  LOADING_COMPLETE = 'LOADING_COMPLET',
  AUTH_LOADING = 'AUTH_LOADING',
  GET_POST_LOADING = 'GET_POST_LOADING',
  ADDorREMOVEfromWISHLIST = 'ADDorREMOVEfromWISHLIST',
  ADDorREMOVEfromCart = 'ADDorREMOVEfromCart',
  ADD_USER_With_SIGNUP = 'ADD_USER_With_SIGNUP',
  ADD_USER_With_SIGNIN = 'ADD_USER_With_SIGNIN',
  GET_WISHLIST = 'GET_WISHLIST',
  GET_ADDRESS = 'GET_ADDRESS',
  PUT_ADDRESS = 'PUT_ADDRESS',
  POST_ADDRESS = 'POST_ADDRESS',
  POST_ORDER = 'POST_ORDER',
  EMPTY_CART = 'EMPTY_CART',
  GET_ORDER = 'GET_ORDER',
  PUSH_ORDERS = 'PUSH_ORDERS',
  GET_CART = 'GET_CART',
  LOG_OUT = 'LOG_OUT',
  GET_LIST_LOADING = 'GET_LIST_LOADING',
  LOAD_MEGAMENU = 'LOAD_MEGAMENU',
  GET_PAYMETHODS = 'GET_PAYMETHODS',
  LOAD_EXPO_TOKEN = 'LOAD_EXPO_TOKEN',
  EXPO_FIRST_TIME_TOKEN_SENT = 'FIRST_TIME_TOKEN_SENT',
  EXPO_AUTHENTICATE_TIME_TOKEN_SENT = 'EXPO_AUTHENTICATE_TIME_TOKEN_SENT',
  CHOOSE_ADDRESS = 'CHOOSE_ADDRESS',
  GET_PROMOTION = 'GET_PROMOTION',
  VERIFY_PHONE = 'VERIFY_PHONE';
//================Action Creators

const FaceBookLogger = ExpoToken => async dispatch => {
  try {
    const expoPushToken = ExpoTokenExtractor(ExpoToken);
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions
    } = await Facebook.logInWithReadPermissionsAsync('1689424238051599', {
      permissions: ['public_profile', 'email', 'user_gender']
    });
    if (type === 'success') {
      //console.log("FBToken", token);
      fetch(
        `https://graph.facebook.com/v3.3/me?fields=id,email,name,first_name,last_name,picture.type(large)&access_token=${token}`
      )
        .then(FBRes => FBRes.json())
        .then(async JSONRes => {
          const {
            email,
            gender,
            name,
            first_name,
            last_name,
            picture
          } = JSONRes;
          const { data } = picture;
          const { url } = data;
          //console.log(JSON.stringify(JSONRes));
          /*{
             "id": "2367875159946360",
             "email": "imrannoor1992@outlook.com",
             "gender": "male",
             "name": "Imran Noor",
             "first_name": "Imran",
             "last_name": "Noor",
             "picture": {
                 "data": {
                     "height": 200,
                     "is_silhouette": false,
                     "url": "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2367875159946360&height=200&width=200&ext=1561791341&hash=AeTYYkQbjqfnxnjR",
                     "width": 200
                 }
             }
              }*/
          const Check = await APIFuncs.CheckerFBUserExist(email);
          //const Check =await APIFuncs.CheckerFBUserExist("sunnybukhari92@gmail.com")
          if (Check !== null) {
            dispatch({ type: AUTH_LOADING, payload: true });
            if (typeof Check === 'object') {
              const { token, cart, user } = Check;
              if (token) {
                APIFuncs.getMyAddress(token).then(RESAddress => {
                  if (Array.isArray(RESAddress)) {
                    console.log('Address GETTING ');
                    dispatch({ type: GET_ADDRESS, payload: RESAddress });
                  } else {
                    console.log('Address GETTING Bug');
                  }
                  dispatch({ type: ADD_USER_With_SIGNIN, payload: Check });
                });
                APIFuncs.getMyOrders(token).then(RESOrder => {
                  if (Array.isArray(RESOrder)) {
                    console.log('Address GETTING ');
                    dispatch({ type: GET_ORDER, payload: RESOrder });
                  } else {
                    console.log('Order GETTING Bug');
                  }
                });
                APIFuncs.getMyWishList(token).then(RESWishlist => {
                  if (typeof RESWishlist === 'object') {
                    console.log('Wishlist GETTING ');
                    dispatch({ type: GET_WISHLIST, payload: RESWishlist });
                    dispatch({ type: AUTH_LOADING, payload: false });
                  } else {
                    console.log('Wishlist GETTING Bug');
                  }
                });

                if (cart) {
                  const { items } = cart;
                  dispatch({
                    type: GET_CART,
                    payload:
                      items !== undefined && items !== '' ? cart : { items: [] }
                  });
                }
                console.log('CARTWhileLogin', cart);
                APIFuncs.SendExpoTokenToServerForUser(
                  user._id,
                  expoPushToken,
                  token
                ).then(RESX => {
                  dispatch({
                    type: EXPO_AUTHENTICATE_TIME_TOKEN_SENT,
                    payload: { tokenRegisteredInProfile: RESX }
                  });
                });
              } else {
                dispatch({ type: AUTH_LOADING, payload: false });
                console.log('token Not Present');
              }
            } else {
              Alert.alert('Alert', Check);
              dispatch({ type: AUTH_LOADING, payload: false });
            }
          } else {
            dispatch({ type: AUTH_LOADING, payload: true });
            APIFuncs.postSignUp({
              expoPushToken: ExpoToken,
              firstName: first_name,
              lastName: last_name,
              email,
              phone: '',
              gender: gender !== undefined ? gender : 'male',
              role: 'user',
              username: name,
              avatar: url,
              provider: 'facebook',
              facebook: JSONRes,
              platform: 'app'
              //password: "facebookByPass",
              //expoPushToken
            }).then(RES => {
              typeof RES === 'object'
                ? RES.token
                  ? (console.log('USER SIGNUP'),
                    dispatch({ type: ADD_USER_With_SIGNUP, payload: RES }),
                    dispatch({
                      type: AUTH_LOADING,
                      payload: false
                    }))
                  : (console.log('token Not Present'),
                    Alert.alert('Something is wrong!'))
                : //console.log(RES),
                  (Alert.alert(RES + ' !'),
                  dispatch({ type: AUTH_LOADING, payload: false }));
            });
          }
        });
    } else {
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
};
//=================================================
const signUpUserWithPhone = OBJECT => async dispatch => {
  let result = { error: false, message: '' };
  // dispatch({ type: AUTH_LOADING, payload: true });
  await APIFuncs.postSignUpWithPhone({ ...OBJECT, platform: 'app' }).then(
    RES => {
      //console.log('signUpUserWithPhoneRes', RES);
      if (typeof RES === 'object') {
        const { name, message, errors } = RES;
        if (errors === undefined) {
          result = { error: false, message: name };
        } else {
          result = { error: true, message: name };
        }
        //console.log('USER SIGNUP');
        //dispatch({ type: ADD_USER_With_SIGNUP, payload: RES });
      }
      //dispatch({ type: AUTH_LOADING, payload: false });
    }
  );
  return result;
};

const signInUserWithPhone = OBJECT => async dispatch => {
  //dispatch({ type: AUTH_LOADING, payload: true });
  await APIFuncs.postSignInWithPhone({ ...OBJECT, platform: 'app' }).then(
    RES => {
      if (typeof RES === 'object') {
        const { token, cart, user } = RES;
        // console.log('RESULT', RES);
        if (token !== undefined) {
          //console.log('JWT:', JWT(token));
          //console.log('TOKEN', token);
          //APIFuncs.getMyCart(token).then(RESCart => {
          //   console.log('CART:', RESCart);
          //dispatch({
          // type: GET_CART,
          // payload: items !== undefined ? { items } : { items: [] }
          //});
          //  });
          APIFuncs.getMyAddress(token).then(RESAddress => {
            if (Array.isArray(RESAddress)) {
              console.log('Address GETTING ');
              dispatch({ type: GET_ADDRESS, payload: RESAddress });
            } else {
              console.log('Address GETTING Bug');
            }
            dispatch({ type: ADD_USER_With_SIGNIN, payload: RES });
          });
          APIFuncs.getMyOrders(token).then(RESOrder => {
            if (Array.isArray(RESOrder)) {
              console.log('Address GETTING ');
              dispatch({ type: GET_ORDER, payload: RESOrder });
            } else {
              console.log('Order GETTING Bug');
            }
          });
          APIFuncs.getMyWishList(token).then(RESWishlist => {
            if (typeof RESWishlist === 'object') {
              console.log('Wishlist GETTING ');
              dispatch({ type: GET_WISHLIST, payload: RESWishlist });
              dispatch({ type: AUTH_LOADING, payload: false });
            } else {
              console.log('Wishlist GETTING Bug');
            }
          });
          //APIFuncs.getMyCart(token).then(RESCart => {
          //console.log("Getting Cart ");
          //dispatch({ type: GET_CART, payload: cart ? cart : { items: [] } });
          if (cart) {
            const { items } = cart;
            dispatch({
              type: GET_CART,
              payload:
                items !== undefined && items !== '' ? cart : { items: [] }
            });
          }

          console.log('CARTWhileLogin', cart);
          //dispatch({ type: GET_CART, payload: { items: [] } });
          // dispatch({ type: AUTH_LOADING, payload: false }); //
          //});
          APIFuncs.SendExpoTokenToServerForUser(
            user._id,
            ExpoTokenExtractor(OBJECT.expoPushToken),
            token
          ).then(RESX => {
            dispatch({
              type: EXPO_AUTHENTICATE_TIME_TOKEN_SENT,
              payload: { tokenRegisteredInProfile: RESX }
            });
          });
        } else {
          // dispatch({ type: AUTH_LOADING, payload: false });
          Alert.alert('Username or password is not correct');
        }
      } else {
        Alert.alert('Alert', RES);
        // dispatch({ type: AUTH_LOADING, payload: false });
      }
    }
  );
};
//=================================================
const signUpUser = OBJECT => async dispatch => {
  dispatch({ type: AUTH_LOADING, payload: true });
  APIFuncs.postSignUp(OBJECT).then(RES => {
    typeof RES === 'object'
      ? RES.token
        ? (console.log('USER SIGNUP'),
          dispatch({ type: ADD_USER_With_SIGNUP, payload: RES }),
          dispatch({ type: AUTH_LOADING, payload: false }),
          APIFuncs.SendExpoTokenToServerForUser(
            RES._id,
            ExpoTokenExtractor(OBJECT.expoPushToken),
            RES.token
          )).then(RESX => {
            dispatch({
              type: EXPO_AUTHENTICATE_TIME_TOKEN_SENT,
              payload: { tokenRegisteredInProfile: RESX }
            });
          })
        : (console.log('token Not Present'), Alert.alert('Something is wrong!'))
      : //console.log(RES),
        (Alert.alert(RES + ' !'),
        dispatch({ type: AUTH_LOADING, payload: false }));
  });
};

// const signInUser = OBJECT => async dispatch => {
//   APIFuncs.postSignIn(OBJECT).then(RES => {
//     console.log("SigninDump :", RES);
//   });
// };
const signInUser = OBJECT => async dispatch => {
  dispatch({ type: AUTH_LOADING, payload: true });
  APIFuncs.postSignIn(OBJECT).then(RES => {
    if (typeof RES === 'object') {
      const { token, cart, user } = RES;
      //console.log('SignIn: \n', RES);
      if (token) {
        APIFuncs.getMyAddress(token).then(RESAddress => {
          if (Array.isArray(RESAddress)) {
            console.log('Address GETTING ');
            dispatch({ type: GET_ADDRESS, payload: RESAddress });
          } else {
            console.log('Address GETTING Bug');
          }
          dispatch({ type: ADD_USER_With_SIGNIN, payload: RES });
        });
        APIFuncs.getMyOrders(token).then(RESOrder => {
          if (Array.isArray(RESOrder)) {
            console.log('Address GETTING ');
            dispatch({ type: GET_ORDER, payload: RESOrder });
          } else {
            console.log('Order GETTING Bug');
          }
        });
        APIFuncs.getMyWishList(token).then(RESWishlist => {
          if (typeof RESWishlist === 'object') {
            console.log('Wishlist GETTING ');
            dispatch({ type: GET_WISHLIST, payload: RESWishlist });
            dispatch({ type: AUTH_LOADING, payload: false });
          } else {
            console.log('Wishlist GETTING Bug');
          }
        });
        // APIFuncs.getMyCart(token).then(RESCart => {
        // console.log("Getting Cart ");
        console.log('CARTWhileLogin', cart);
        if (cart) {
          const { items } = cart;
          dispatch({
            type: GET_CART,
            payload: items !== undefined && items !== '' ? cart : { items: [] }
          });
        }
        // dispatch({ type: AUTH_LOADING, payload: false }); //
        //});

        APIFuncs.SendExpoTokenToServerForUser(
          user._id,
          ExpoTokenExtractor(OBJECT.expoPushToken),
          token
        ).then(RESX => {
          dispatch({
            type: EXPO_AUTHENTICATE_TIME_TOKEN_SENT,
            payload: { tokenRegisteredInProfile: RESX }
          });
        });
      } else {
        dispatch({ type: AUTH_LOADING, payload: false }); //
        console.log('token Not Present');
      }
    } else {
      Alert.alert('Alert', RES);
      dispatch({ type: AUTH_LOADING, payload: false });
    }
  });
};
const AddorRemoveWishList = (OBJECT, LocalObject, Token) => async dispatch => {
  APIFuncs.postAddorRemoveinWishlist(OBJECT, Token).then(RES => {
    console.log('Wishlist Updated');
    dispatch({ type: ADDorREMOVEfromWISHLIST, payload: LocalObject });
    //console.log('CurrentWishlist :', LocalObject);
  });
};

const getMyWishlist = Token => async dispatch => {
  APIFuncs.getMyWishList(Token).then(RES => {
    if (typeof RES === 'object') {
      console.log('Wishlist GETTING ');
      dispatch({ type: GET_WISHLIST, payload: RES });
    } else {
      console.log('Wishlist GETTING Bug');
    }
  });
};
const getMyOrders = Token => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  const Returner = await APIFuncs.getMyOrders(Token).then(RESOrder => {
    if (Array.isArray(RESOrder)) {
      console.log('Address GETTING ');
      dispatch({ type: GET_ORDER, payload: RESOrder });
      dispatch({ type: GET_POST_LOADING, payload: false });
      return RESOrder;
    } else {
      console.log('Order GETTING Bug');
      dispatch({ type: GET_POST_LOADING, payload: false });
      return false;
    }
  });
  return Returner;
};
const getMyCart = Token => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  const Returner = await APIFuncs.getMyCart(Token).then(cart  => {
    //dispatch({ type: GET_CART, payload: RESCart });
    if (cart) {
      const { items } = cart;
      dispatch({
        type: GET_CART,
        payload: items !== undefined && items !== '' ? cart : { items: [] }
      });
    }
    
    dispatch({ type: GET_POST_LOADING, payload: false });
    return RESCart;
  });
  return Returner;
};

const addAddress = (OBJECT, token) => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  let REZ = false;
  await APIFuncs.postMyAddress(token, OBJECT).then(RES => {
    if (typeof RES === 'object') {
      dispatch({ type: POST_ADDRESS, payload: RES });
      REZ = true;
      dispatch({ type: GET_POST_LOADING, payload: false });
    } else {
      dispatch({ type: GET_POST_LOADING, payload: false });
      // console.log(RES);
    }
  });
  return REZ;
};
const SelectAddressIndex = INDEX => async dispatch => {
  dispatch({ type: CHOOSE_ADDRESS, payload: INDEX });
};

const firstAddAddressThenPostOrder = (
  addressObject,
  token,
  navigation
) => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  let addressResult = false,
    orderResult = null;
  await APIFuncs.postMyAddress(token, addressObject).then(addressRES => {
    if (typeof addressRES === 'object') {
      dispatch({ type: POST_ADDRESS, payload: addressRES });
      addressResult = true;
    } else {
      //console.log(RES);
    }
  });
  await APIFuncs.sendOrder(token, addressObject).then(orderRES => {
    if (typeof orderRES === 'object') {
      dispatch({ type: POST_ORDER, payload: orderRES });
      //console.log(orderRES);
      dispatch({ type: EMPTY_CART, payload: [] });
      orderResult = orderRES;
      navigation.replace('orderput', {
        orderNumber: orderRES.orderNo,
        isSuccess: true
      });
      dispatch({ type: GET_POST_LOADING, payload: false });
    } else {
      //  console.log(orderRES);
      dispatch({ type: GET_POST_LOADING, payload: false });
      navigation.replace('orderput', {
        isSuccess: false
      });
    }
  });

  return { addressRez: addressResult, orderRez: orderResult };
};

//===============Action For Update Cart==============================================
const ADDorREMOVECART = (objectForAPI, Token) => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  console.log('cart action');
  const Returner = await APIFuncs.updateMyCart(Token, objectForAPI).then(
    Res => {
      //alert(Res)
      dispatch({ type: ADDorREMOVEfromCart, payload: Res });
      dispatch({ type: GET_POST_LOADING, payload: false });
      return Res;
    }
  );
  return Returner;
};

//===============Action For Placing Order==============================================
const sendOrder = (OBJECT, token, navigation) => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  let REZ = false;
  await APIFuncs.sendOrder(token, OBJECT).then(RES => {
    if (typeof RES === 'object') {
      dispatch({ type: POST_ORDER, payload: RES });
      //console.log(RES);
      dispatch({ type: EMPTY_CART, payload: null });
      navigation.replace('orderput', {
        orderNumber: RES.orderNo,
        isSuccess: true
      });
      REZ = true;
      dispatch({ type: GET_POST_LOADING, payload: false });
    } else {
      //console.log(RES);
      dispatch({ type: GET_POST_LOADING, payload: false });
      navigation.replace('orderput', {
        isSuccess: false
      });
    }
  });
  return REZ;
};
/*const orderId = navigation.getParam("orderId", "1234");
  const amount = navigation.getParam("amount", "1234");
  const phone = navigation.getParam("phone", "1234");
  const email = navigation.getParam("email", "abc@gmail.com");*/
const sendOrderWithTheme = (
  OBJECT,
  token,
  navigation,
  TYPE
) => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: true });
  let REZ = false;
  await APIFuncs.sendOrder(token, OBJECT).then(RES => {
    if (typeof RES === 'object') {
      dispatch({ type: POST_ORDER, payload: RES });
      //console.log(RES);
      dispatch({ type: EMPTY_CART, payload: null });
      if (TYPE === 'COD') {
        navigation.replace('orderput', {
          orderNumber: RES.orderNo,
          OrderDetail: RES,
          isSuccess: true
        });
      } else if (TYPE === 'Easypaisa') {
        const { orderNo, amount, phone, email } = RES;
        navigation.replace('Easypay', {
          orderId: orderNo,
          amount: amount.total,
          phone,
          email,
          isSuccess: true,
          OrderDetail: RES
        });
      }

      REZ = true;
      dispatch({ type: GET_POST_LOADING, payload: false });
    } else {
      // console.log(RES);
      dispatch({ type: GET_POST_LOADING, payload: false });
      navigation.replace('orderput', {
        isSuccess: false
      });
    }
  });
  return REZ;
};
const BusyFetchingData = Loading => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: Loading });
};
const BusyMakingList = Loading => async dispatch => {
  dispatch({ type: GET_LIST_LOADING, payload: Loading });
};
const Logout = navigation => dispatch => {
  navigation.navigate('DrawerFirstScreen');
  setTimeout(() => dispatch({ type: LOG_OUT, payload: null }), 1000);
};

const GetMegaMenuNew = FirstTime => async dispatch => {
  FirstTime && dispatch({ type: GET_POST_LOADING, payload: true });
  APIFuncs.GetMegaCategoryMenu().then(async RES => {
    const WholeMegaMenu = [
        {
          key: 'Freebies',
          _id: 'Freebies001',
          slug: 'freebies',
          popular: [],
          content: [],
          CoverImage: ''
        }
      ],
      DrawerContentData = [];
    const starterPromise = Promise.resolve(null);
    if (Array.isArray(RES)) {
      await RES.reduce(
        (p, itemX) =>
          p.then(async () => {
            const { name, _id, slug, img, active, app_image_absolute } = itemX;
            if (active === true) {
              let ITEM = {};
              ITEM.key = name;
              ITEM._id = _id;
              ITEM.slug = slug;
              ITEM.popular = [];
              ITEM.content = [];
              ITEM.CoverImage = app_image_absolute;
              prefetchImage(app_image_absolute);
              //console.log("IMAGESCover: ", app_image_absolute);
              DrawerContentData.push({
                Name: name,
                ID: _id,
                CoverImage: app_image_absolute,
                ToSearch: slug
              });
              await APIFuncs.getFeaturedLanding(slug).then(async RES1 => {
                ITEM.popular = RES1;
                if (Array.isArray(RES1) === true) {
                  RES1.map((ITEMA, INDEXA) => {
                    const { imgA } = ITEMA;
                    prefetchImage(imgA);
                    // console.log("Popular: ", imgA);
                  });
                }
                await APIFuncs.getCategoryWidget(slug).then(RES2 => {
                  ITEM.content = RES2;
                  WholeMegaMenu.push(ITEM);
                  if (Array.isArray(RES2) === true) {
                    RES2.map((ITEMZ, INDEXZ) => {
                      const { children } = ITEMZ;
                      if (children !== undefined) {
                        if (Array.isArray(children)) {
                          children.map((ITEMY, INDEXY) => {
                            const { hc_img_url } = ITEMY;
                            prefetchImage(hc_img_url);
                            //console.log("Content: ", hc_img_url);
                          });
                        }
                      }
                    });
                  }
                });
              });
            }
          }),
        starterPromise
      );

      dispatch({
        type: LOAD_MEGAMENU,
        payload: {
          WholeMegaMenu,
          DrawerContentData
        }
      });
      FirstTime && dispatch({ type: GET_POST_LOADING, payload: false });
    } else {
      console.log('HomeMenu with Error', RES);
      FirstTime && dispatch({ type: GET_POST_LOADING, payload: false });
    }
  });
};

// const resetPassword = Email => async dispatch => {
//   dispatch({ type: GET_POST_LOADING, payload: true });
//   APIFuncs.updatePassword(Email).then(RES => {
//     dispatch({ type: GET_POST_LOADING, payload: false });
//     alert(RES);
//   });
// };
//===================================================================
//---------------------------------------------------------------------
//forgotPasswordPhone
const forgotPasswordPhone = ({ phone }) => async dispatch => {
  let result = null;
  dispatch({ type: GET_POST_LOADING, payload: true });
  await APIFuncs.sendForResetorResent('forgotPasswordPhone', {
    phone
  }).then(RES => {
    dispatch({ type: GET_POST_LOADING, payload: false });
    result = RES;
  });
  return result;
};
//phoneResendResetCode
const resetPassword = ({ phone }) => async dispatch => {
  let result = null;
  dispatch({ type: GET_POST_LOADING, payload: true });
  await APIFuncs.sendForResetorResent('phoneResendResetCode', { phone }).then(
    RES => {
      dispatch({ type: GET_POST_LOADING, payload: false });
      result = RES;
    }
  );
  return result;
};
//resetPassword
const sendNewPassword = ({ phone, resetCode, password }) => async dispatch => {
  let result = null;
  dispatch({ type: GET_POST_LOADING, payload: true });
  await APIFuncs.sendForResetorResent('resetPassword', {
    phone,
    resetCode,
    password
  }).then(RES => {
    dispatch({ type: GET_POST_LOADING, payload: false });
    result = RES;
  });
  return result;
};
//verifySignUp
const otpVerifyPhone = ({ otp, phone }) => async dispatch => {
  let result = null;
  dispatch({ type: GET_POST_LOADING, payload: true });
  await APIFuncs.sendForResetorResent('verifySignUp', {
    otp,
    phone
  }).then(RES => {
    const { message, status } = RES;
    status === 'success' && dispatch({ type: VERIFY_PHONE, payload: true });
    dispatch({ type: GET_POST_LOADING, payload: false });
    result = RES;
  });
  return result;
};
//phoneResendSignUp
const resendSignUp = ({ phone }) => async dispatch => {
  let result = null;
  dispatch({ type: GET_POST_LOADING, payload: true });
  await APIFuncs.sendForResetorResent('phoneResendSignUp', {
    phone
  }).then(RES => {
    const { message, status } = RES;
    status === 'success' && dispatch({ type: VERIFY_PHONE, payload: true });
    dispatch({ type: GET_POST_LOADING, payload: false });
    result = RES;
  });
  return result;
};
//---------------------------------------------------------------------
//===================================================================
const Loader = LOAD => async dispatch => {
  dispatch({ type: GET_POST_LOADING, payload: LOAD });
};

const getPayMethods = () => async dispatch => {
  // let ResultTobesent = [];
  dispatch({ type: GET_POST_LOADING, payload: true });
  APIFuncs.GetMoneyMethods().then(RES => {
    // ResultTobesent = RES;
    dispatch({ type: GET_PAYMETHODS, payload: RES });
    dispatch({ type: GET_POST_LOADING, payload: false });
  });
  // return ResultTobesent;
};
const LOADExpoToken = tokenRegisteredInAll => async dispatch => {
  registerForPushNotificationsAsync().then(async ExpoToken => {
    const ExpoTokenTobeSent = ExpoTokenExtractor(ExpoToken);
    dispatch({
      type: LOAD_EXPO_TOKEN,
      payload: {
        ExpoToken: ExpoTokenTobeSent,
        ExpotokenReceived: true
      }
    });
    if (!tokenRegisteredInAll)
      await APIFuncs.SendExpoTokenToServer(ExpoTokenTobeSent).then(RES => {
        dispatch({
          type: EXPO_FIRST_TIME_TOKEN_SENT,
          payload: { tokenRegisteredInAll: RES }
        });
      });
  });
};

const SendTokenForUnAuthorizeUser = () => async dispatch => {
  registerForPushNotificationsAsync().then(ExpoToken => {
    APIFuncs.SendExpoTokenToServer(ExpoTokenExtractor(ExpoToken)).then(RES => {
      dispatch({
        type: EXPO_FIRST_TIME_TOKEN_SENT,
        payload: { tokenRegisteredInAll: RES }
      });
    });
  });
};
const ActionType = {
  AUTH_LOADING,
  GET_POST_LOADING,
  LOADING,
  LOADING_COMPLETE,
  ADDorREMOVEfromWISHLIST,
  ADDorREMOVEfromCart,
  ADD_USER_With_SIGNUP,
  ADD_USER_With_SIGNIN,
  GET_WISHLIST,
  GET_ADDRESS,
  PUT_ADDRESS,
  POST_ADDRESS,
  POST_ORDER,
  EMPTY_CART,
  GET_ORDER,
  PUSH_ORDERS,
  GET_CART,
  LOG_OUT,
  GET_LIST_LOADING,
  LOAD_MEGAMENU,
  GET_PAYMETHODS,
  LOAD_EXPO_TOKEN,
  EXPO_FIRST_TIME_TOKEN_SENT,
  EXPO_AUTHENTICATE_TIME_TOKEN_SENT,
  CHOOSE_ADDRESS,
  GET_PROMOTION,
  VERIFY_PHONE
};
export {
  ActionType,
  sendOrderWithTheme,
  ADDorREMOVECART,
  getMyWishlist,
  signUpUser,
  signInUser,
  signInUserWithPhone,
  FaceBookLogger,
  AddorRemoveWishList,
  addAddress,
  sendOrder,
  firstAddAddressThenPostOrder,
  getMyCart,
  getMyOrders,
  Logout,
  BusyFetchingData,
  BusyMakingList,
  GetMegaMenuNew,
  Loader,
  signUpUserWithPhone,
  getPayMethods,
  LOADExpoToken,
  SendTokenForUnAuthorizeUser,
  SelectAddressIndex,
  //--------------------------
  resetPassword,
  sendNewPassword,
  otpVerifyPhone,
  resendSignUp,
  forgotPasswordPhone
};
