"use strict";
import React from "react";
import { View, Text, StatusBar, TouchableOpacity } from "react-native";
import Style from "./styles";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
import PropTypes from "prop-types";
import { AntDesign as ICON } from "@expo/vector-icons";
import { connect } from "react-redux";
import {
  ADDorREMOVECART,
  sendOrder,
  firstAddAddressThenPostOrder
} from "../../redux/action";

class paymentMethodScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    sendOrder: PropTypes.func,
    ADDorREMOVECART: PropTypes.func
  };
  constructor(props) {
    super(props);
    const { lastName, firstName, email } = props.user;
    this.state = {
      name: firstName + " " + lastName,
      email,
      primaryPhone: "",
      secondaryPhone: "",
      pincode: "",
      address1: "",
      address2: "",
      city: "",
      state: ""
    };
  }
  LABEL = props => {
    const { lable } = props;
    return (
      <View
        style={{
          width: "100%",
          height: heightPercentageToDP(5),
          justifyContent: "center",
          paddingHorizontal: "2%"
        }}
      >
        <Text
          style={{
            fontSize: widthPercentageToDP(4),
            fontWeight: "bold"
          }}
        >
          {lable}
        </Text>
      </View>
    );
  };
  ContentBtn = props => {
    const { lable, image, moreinfo, func, navigation } = props;
    return (
      <TouchableOpacity
        onPress={() => func(navigation)}
        style={{
          width: "100%",
          height: heightPercentageToDP(10),
          // alignItems: "flex-start",
          justifyContent: "center",
          paddingHorizontal: "2%",
          backgroundColor: "#FFF"
        }}
      >
        <Text
          style={{
            fontSize: widthPercentageToDP(4),
            fontWeight: "bold"
          }}
        >
          {lable}
        </Text>
        {moreinfo !== "" ? (
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              fontWeight: "bold"
            }}
          >
            {moreinfo}
          </Text>
        ) : null}
      </TouchableOpacity>
    );
  };
  render() {
    const { LABEL, ContentBtn, props } = this;
    const { navigation } = props;
    const { params } = navigation.state;
    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />
        <View style={Style.statusBarBackground} />
        <View style={Style.otherThenStatusBarView}>
          <View style={Style.TopTitleBar}>
            <TouchableOpacity style={Style.leftTopBarIcon}>
              <ICON
                onPress={() => navigation.goBack()}
                name="left"
                color={"#6a6a6a"}
                size={widthPercentageToDP(5)}
              />
            </TouchableOpacity>
            <Text style={Style.TopTitleBarText}>{"Payment Method"}</Text>
          </View>
          <View
            style={{
              flex: 1
            }}
          >
            <LABEL lable={"OTHER PAYMENT METHODS"} />
            <ContentBtn
              lable={"Cash On Delivery"}
              moreinfo={""}
              func={params.proceedForOrder}
              navigation={navigation}
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  { ADDorREMOVECART, sendOrder, firstAddAddressThenPostOrder }
)(paymentMethodScreen);
