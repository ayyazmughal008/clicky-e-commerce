import { StyleSheet, StatusBar } from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
export default StyleSheet.create({
  cont: {
    flex: 1,
    backgroundColor:  "#FFF",
    alignItems: "center"
  },
  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#FFF",
    width: "100%"
  },
  otherThenStatusBarView: {
    flex: 1,
    backgroundColor:  "#E2E2E2",
    width: "100%"
  },
  TopTitleBar: {
    paddingHorizontal: "2%",
    backgroundColor:'#FFF',
    width: widthPercentageToDP(100),
    height: "8%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: "#E2E2E2"
  },
  ScrollView: {
    height: "84%",
    width: "100%"
  },

  ScreenBottomViewLeft: {
    height: "98%",
    width: "50%",
    alignItems: "center",
    justifyContent: "center"
  },
  ScreenBottomViewRight: {
    height: "98%",
    width: "48%",
    alignItems: "flex-end"
  },
  leftTopBarIcon: {
    left: "2%",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    width: "12.5%",
    height: "100%"
  },
  TopTitleBarText: {
    fontSize: widthPercentageToDP(5.5),
    color: "#000",
    fontWeight: "bold"
  },
  PreviousPrice: {
    textDecorationLine: "line-through",
    fontSize: widthPercentageToDP(3),
    color: "#999999"
  },
  Prices: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "40%",
    height: "100%",
    alignItems: "center"
  },
  CurrentPrice: {
    color: "#F15441",
    fontSize: widthPercentageToDP(3.5),
    fontWeight: "bold"
  },
  ItemBottomView: {
    flexDirection: "row",
    width: "100%",
    height: "30%"
  },
  ItemTopView: { width: "100%", height: "50%" },
  ItemMidView: { width: "100%", height: "20%" },
  Quantity: {
    paddingRight: "3%",
    justifyContent: "center",
    width: "60%",
    height: "100%",
    alignItems: "flex-end"
  },
  ScreenBottomViewRightButton: {
    backgroundColor: "#f15441",
    borderRadius: widthPercentageToDP(2),
    alignItems: "center",
    justifyContent: "center",
    width: "70%",
    height: "100%"
  },
  ScreenBottomViewRightButtonText: {
    color: "#FFFFFF",
    fontSize: widthPercentageToDP(3.5)
  },
  ScreenBottomViewLeftViewDown: {
    width: "100%",
    height: "43%"
  },
  ScreenBottomViewLeftViewUP: {
    flexDirection: "row",
    width: "100%",
    height: "55%",
    alignItems: "center"
  },
  ScreenBottomViewLeftViewUPPrice: {
    fontSize: widthPercentageToDP(3.5),
    color: "#F15444"
  },
  ScreenBottomViewLeftViewUPTitle: {
    fontSize: widthPercentageToDP(3.5),
    color: "#888888",
    fontWeight: "bold"
  },
  contentContainer: {
    flexGrow: 1,
    width: widthPercentageToDP(100),
    paddingHorizontal: "2%",
    backgroundColor: "#F5f5f5"
  },
  textInputStyle: {
    height: heightPercentageToDP(6),
    width: "100%",
    borderColor: "#EAEAEA",
    borderBottomWidth: widthPercentageToDP(0.3)
  },
  textInputView: {
    flexGrow: 1,
    backgroundColor: "#ffffff",
    marginBottom: heightPercentageToDP(2.5)
  },
  miniStatementView: {
    marginTop: heightPercentageToDP(2.5),
    width: "100%",
    height: heightPercentageToDP(20),
    backgroundColor: "#ffffff",
    paddingHorizontal: "2%"
  },
  miniStatementViewTop: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: "30%",
    alignItems: "center"
  },

  miniStatementViewBottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: "40%",
    alignItems: "center"
  },
  miniStatementViewTopleft: {
    fontSize: widthPercentageToDP(4),
    color: "#AAAAAA"
  },
  miniStatementViewTopRight: {
    fontSize: widthPercentageToDP(4),
    color: "#646464"
  },
  miniStatementViewBottomleft: {
    fontSize: widthPercentageToDP(6),
    color: "#646464"
  },

  miniStatementViewBottomRight: {
    fontSize: widthPercentageToDP(6),
    color: "#F15441"
  },
  screenBottomButton: {
    height: "8%",
    width: "100%",
    backgroundColor: "#F15441",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  screenBottomButtonText: {
    fontSize: widthPercentageToDP(4),
    color: "#fff"
  }
});
