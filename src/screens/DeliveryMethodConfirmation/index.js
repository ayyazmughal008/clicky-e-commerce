import React from "react";
import { View } from "react-native";
import CODMethod from "./CODMethod";
import HBLMethod from "./HBLMethod";
import EasyPaisa from "./EasyPaisa";
export default class DeliveryMethScreen extends React.Component {
  render() {
    const { navigation } = this.props;
    const { Method, MethodFunc, TotalPrice } = navigation.state.params;
    return (
      <View style={{ flex: 1 }}>
        {Method === "COD" ? (
          <CODMethod
            MethodFunc={MethodFunc}
            navigation={navigation}
            TotalPrice={TotalPrice}
          />
        ) : (
          <EasyPaisa
            MethodFunc={MethodFunc}
            navigation={navigation}
            TotalPrice={TotalPrice}
          />
        )}
      </View>
    );
  }
}
