import React from "react";
import { View, Text, Image } from "react-native";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import { ClickyButton } from "../../component/Buttons";
import { APIFuncs } from "../../modules/APIs";
import ParentHeaders from "../../component/Header";
export default class CODMethod extends React.Component {
  state = { ShippingCharges: 0 };
  componentWillMount() {
    APIFuncs.GetSettings().then(Res => {
      console.log("Setting", Res);
      this.setState({ ShippingCharges: Res[0].shipping.charge });
    });
  }
  render() {
    const { navigation, MethodFunc, TotalPrice } = this.props;
    const { ShippingCharges } = this.state;
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Cash on Delivery"
        />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            width: widthPercentageToDP(90),
            height: heightPercentageToDP(10),
            borderWidth: widthPercentageToDP(0.3),
            borderColor: "#cccccc",
            shadowColor: "#cccccc",
            shadowOpacity: widthPercentageToDP(0.3)
          }}
        >
          <Image
            resizeMode="contain"
            source={require("../../../assets/images/cash.png")}
            style={{
              height: widthPercentageToDP(10),
              width: widthPercentageToDP(10)
            }}
          />
          <Text
            style={{
              color: "#000",
              fontSize: widthPercentageToDP(4),
              fontWeight: "300",
              marginLeft: widthPercentageToDP(3),
              width: widthPercentageToDP(73)
            }}
            numberOfLines={2}
          >
            You can pay in cash to our courier when you receive the goods at
            your doorstep.
          </Text>
        </View>

        <View
          style={{
            flex: 1,
            width: "100%",
            position: "absolute",
            left: 0,
            right: 0,
            bottom: 0
          }}
        >
          <View
            style={{
              marginTop: 5,
              marginBottom: 5,
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              alignItems: "center",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"Total Amount"}
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"PKR " + TotalPrice}
            </Text>
          </View>
          <View
            style={{
              marginTop: 5,
              marginBottom: 5,
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              alignItems: "center",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"Price Discount"}
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"PKR 0"}
            </Text>
          </View>
          <View
            style={{
              marginTop: 5,
              marginBottom: 5,
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              alignItems: "center",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"Shipping Charges"}
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {ShippingCharges !== 0 ? "PKR " + ShippingCharges : "FREE"}
            </Text>
          </View>
          <View
            style={{
              marginTop: 5,
              marginBottom: 5,
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              alignItems: "center",
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"To be paid"}
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "400",
                color: "#000",
                textAlign: "left"
              }}
            >
              {"PKR " + (TotalPrice + ShippingCharges)}
            </Text>
          </View>
          <View
            style={{
              alignItems: "center"
            }}
          >
            <ClickyButton
              onPress={() => MethodFunc(navigation, "COD")}
              text={"SUBMIT ORDER"}
              style={{
                marginBottom: 5
              }}
            />
            {/* <Button
              title="SUBMIT ORDER"
              titleStyle={{
                color: "#fff",
                textAlign: "center",
                fontSize: 20
              }}
              onPress={() => MethodFunc(navigation, "COD")}
              buttonStyle={{
                width: widthPercentageToDP(95),
                height: heightPercentageToDP(8),
                backgroundColor: "#000"
                //borderRadius: 40
              }}
            /> */}
          </View>
        </View>
      </View>
    );
  }
}
