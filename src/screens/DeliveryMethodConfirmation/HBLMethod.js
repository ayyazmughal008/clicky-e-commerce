import React from "react";
import { View, Text } from "react-native";
import ParentHeaders from "../../component/Header";

export default class HBLMethod extends React.Component {
  render() {
    const { navigation, MethodFunc, TotalPrice } = this.props;
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="HBL Money Transfer"
        />
      </View>
    );
  }
}
