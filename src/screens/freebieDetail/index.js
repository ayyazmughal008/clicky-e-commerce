'use strict';
import React from 'react';
import {
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  Text,
  StyleSheet,
  ScrollView,
  Modal,
  Alert
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import Sections from '../../component/SectionOpener';
import { APIFuncs } from '../../modules/APIs';
import { Ionicons } from '@expo/vector-icons';
import ParentHeaders from '../../component/Header';
const PlaceHolder = require('../../../assets/images/placeholder.png');
import { BusyFetchingData, BusyMakingList } from '../../redux/action';
import AlertBox from '../../component/AlertModal';
import LoginScreen from '../LoginHome';
import { connect } from 'react-redux';
class FreebieDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageReady: false,
      ModalOn: false,
      AlertModalOn: false,
      alertContent: {
        header: '',
        message: '',
        type: 'info'
      },
      itemData: this.props.navigation.state.params.itemData
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    const { USER, navigation } = nextProps;
    const { updateFunc, itemData, itemIndex } = navigation.state.params;
    const { _id, has_applied, applicants } = itemData;
    const { isLoggedIn, token, userId } = USER;
    if (isLoggedIn && this.state.ModalOn) {
      this.setState(
        {
          ModalOn: false
        },
        async () => {
          const currentUserArray = applicants.filter(
            item => item.user === userId
          );
          if (currentUserArray === 0) {
            await APIFuncs.applyFreebiesFunc(_id, token).then(FreeBieResult => {
              const { id } = FreeBieResult;
              if (id !== undefined) {
                this.setState(
                  {
                    alertContent: {
                      header: 'Success',
                      message: 'You have successfully applied for this product',
                      type: 'success'
                    }
                  },
                  () => {
                    this.toggleModal(true);
                  }
                );
                updateFunc(itemIndex, userId);
                this.updateDetailUponAppliedHere(userId);
              }
            });
          } else {
            this.setState(
              {
                alertContent: {
                  header: 'Info',
                  message: 'You have already applied for this product',
                  type: 'error'
                }
              },
              () => {
                this.toggleModal(true);
              }
            );
          }
        }
      );
    }
  }
  updateDetailUponAppliedHere = _id => {
    const { itemData } = this.state;
    itemData.has_applied = true;
    itemData.applicants.push({ user: _id });
    itemData.totalApplicants = itemData.totalApplicants + 1;
    this.setState({ itemData });
  };
  proceedForApply = async () => {
    const { USER, navigation } = this.props;
    const { updateFunc, itemData, itemIndex } = navigation.state.params;
    const { _id, has_applied } = itemData;
    const { isLoggedIn, token, userId } = USER;
    if (isLoggedIn && !has_applied) {
      await APIFuncs.applyFreebiesFunc(_id, token).then(FreeBieResult => {
        const { id } = FreeBieResult;
        if (id !== undefined) {
          //Alert.alert('Successfully Applied');
          // Alert.alert(
          //   'Success',
          //   'You Successfully Applied',
          //   [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
          //   { cancelable: false }
          // );
          this.setState(
            {
              alertContent: {
                header: 'Success',
                message: 'You Successfully Applied',
                type: 'success'
              }
            },
            () => {
              this.toggleModal(true);
            }
          );
          updateFunc(itemIndex, userId);
          this.updateDetailUponAppliedHere(userId);
        }
        //console.log('KK', token);
      });
    } else if (has_applied) {
      // Alert.alert(
      //   'Alert!',
      //   'You already applied',
      //   [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      //   { cancelable: false }
      // );
      this.setState(
        {
          alertContent: {
            header: 'Alert!',
            message: 'You already applied',
            type: 'error'
          }
        },
        () => {
          this.toggleModal(true);
        }
      );
    } else
      this.setState({
        ModalOn: true
      });
  };

  CloseModal = () => {
    this.setState({
      ModalOn: false
    });
  };
  toggleModal = AlertModalOn => {
    this.setState({ AlertModalOn });
  };
  render() {
    const {
      imageReady,
      ModalOn,
      itemData,
      AlertModalOn,
      alertContent
    } = this.state;
    const { navigation } = this.props;
    const { endsIn: timeLeft } = navigation.state.params;
    const { totalApplicants, totalAvailable, has_applied } = itemData;
    const { mrp } = itemData.productData[0].variants[0];
    const { imgUrls, name, brandName, description } = itemData.productData[0];
    const { length } = imgUrls;
    return (
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(100)
        }}
      >
        <AlertBox
          alertContent={alertContent}
          toggaleFunc={this.toggleModal}
          onSwitch={AlertModalOn}
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalOn}
          onRequestClose={this.CloseModal}
        >
          <LoginScreen navigation={navigation} modalFunc={this.CloseModal} />
        </Modal>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <Image
          onLoad={() => {
            this.setState({ imageReady: true });
          }}
          source={{ uri: imgUrls[0] }}
          style={{ width: 1, height: 1, position: 'absolute' }}
          resizeMode="contain"
        />
        <View
          style={{
            width: widthPercentageToDP(100)
          }}
        >
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={() => navigation.goBack()}
            LeftIcon="arrow-back"
            title="FREEBIES"
          />
        </View>
        <ScrollView
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(82)
          }}
          contentContainerStyle={{
            width: widthPercentageToDP(100),
            backgroundColor: '#FFFFFF',
            alignItems: 'center'
          }}
          showsVerticalScrollIndicator={false}
        >
          <Image
            style={{
              marginVertical: heightPercentageToDP(2),
              width: widthPercentageToDP(85),
              height: heightPercentageToDP(25)
            }}
            resizeMode="contain"
            source={imageReady ? { uri: imgUrls[0] } : PlaceHolder}
          />
          <View
            style={{
              width: widthPercentageToDP(90),
              height: heightPercentageToDP(8)
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(3),
                color: 'black',
                textAlign: 'center',
                fontWeight: '400',
                flex: 1,
                overflow: 'hidden'
              }}
            >
              {name}
            </Text>
          </View>
          <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(8),
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderTopWidth: StyleSheet.hairlineWidth,
              flexDirection: 'row',
              paddingHorizontal: widthPercentageToDP(5),
              justifyContent: 'space-between'
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '33%',
                height: '100%'
              }}
            >
              <Text
                style={{
                  color: 'grey',
                  fontSize: widthPercentageToDP(4)
                  //fontFamily: 'Metropolis_Regular'
                }}
              >
                Original price
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: widthPercentageToDP(4)
                  //fontFamily: 'Metropolis_Regular'
                }}
              >
                {'PKR ' + mrp.toFixed(0)}
              </Text>
            </View>
            <View
              style={{
                width: '33%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                borderLeftWidth: StyleSheet.hairlineWidth,
                borderRightWidth: StyleSheet.hairlineWidth
              }}
            >
              <Text
                style={{
                  color: 'grey',
                  fontSize: widthPercentageToDP(4)
                  //fontFamily: 'Metropolis_Regular'
                }}
              >
                Available
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: widthPercentageToDP(4)
                  // fontFamily: 'Metropolis_Regular'
                }}
              >
                {'' + totalAvailable}
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '33%',
                height: '100%'
              }}
            >
              <Text
                style={{
                  color: 'grey',
                  fontSize: widthPercentageToDP(4)
                  //  fontFamily: 'Metropolis_Regular'
                }}
              >
                Applicants
              </Text>
              <Text
                style={{
                  color: '#65BA69',
                  fontSize: widthPercentageToDP(4)
                  //fontFamily: 'Metropolis_Regular'
                }}
              >
                {'' + totalApplicants}
              </Text>
            </View>
          </View>
          <View
            style={{
              width: widthPercentageToDP(90),
              height: heightPercentageToDP(6),
              alignItems: 'center',
              flexDirection: 'row',
              marginBottom: heightPercentageToDP(1)
              //justifyContent: 'center'
            }}
          >
            <Ionicons
              size={widthPercentageToDP(6)}
              name={'ios-alarm'}
              color={'#F4511E'}
            />
            <Text
              style={{
                marginLeft: widthPercentageToDP(1.5),
                fontSize: widthPercentageToDP(4)
                //fontFamily: 'Metropolis_Regular'
              }}
            >
              Ends in :
            </Text>
            <Text
              style={{
                marginLeft: widthPercentageToDP(1.5),
                fontSize: widthPercentageToDP(4),
                //fontFamily: 'Metropolis_Regular',
                color: '#F4511E'
              }}
            >
              {timeLeft}
            </Text>
          </View>
          <Sections
            ref={SecRef => (this.Section1 = SecRef)}
            title={'Product Information'}
            style={{
              width: widthPercentageToDP(95)
            }}
          >
            <View
              style={{
                padding: 10,
                marginTop: heightPercentageToDP(1)
              }}
            >
              {brandName !== undefined && <Text>{'Brand: ' + brandName}</Text>}
              {description !== undefined && (
                <Text>{'Discription: ' + description}</Text>
              )}
            </View>
          </Sections>
          {/* <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(20),
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderTopWidth: StyleSheet.hairlineWidth
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(6),
                flexDirection: 'row'
              }}
            ></View>
          </View> */}
        </ScrollView>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(8),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <TouchableOpacity
            disabled={timeLeft === 'Expire'}
            onPress={() => {
              this.proceedForApply();
            }}
            style={{
              width: widthPercentageToDP(50),
              height: heightPercentageToDP(6),
              backgroundColor:
                has_applied || timeLeft === 'Expire' ? '#F44336' : '#65BA69',
              borderRadius: widthPercentageToDP(1.5),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                //fontFamily: 'Metropolis_Regular',
                color: '#fff'
              }}
            >
              {has_applied
                ? 'Applied'
                : timeLeft === 'Expire'
                ? 'Expired'
                : 'Apply for Free'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { USER, APPSTATE } = state;
  return {
    AppState: APPSTATE,
    USER
  };
};
export default connect(
  mapStateToProps,
  { BusyFetchingData, BusyMakingList }
)(FreebieDetail);
