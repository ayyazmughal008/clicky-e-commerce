'use strict';
import React from 'react';
import {
  View,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Modal,
  TextInput
} from 'react-native';
import Lottie from 'lottie-react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { APIFuncs } from '../../modules/APIs';
import { ClickyButton } from '../../component/Buttons';
import PullandBear from '../../component/CategoryCard/PullandBear';
import ParentHeaders from '../../component/Header';
import Rheostat, { RheostatThemeProvider } from 'react-native-rheostat';
import { MaterialIcons as ICON } from '@expo/vector-icons';
import FilterSections from '../../component/filterModal/FilterSections';
import {
  sorts,
  stringMaker,
  firstLetterCapital,
  InfoLogger,
  WarningLogger
} from '../../modules/utilts';
import { BusyFetchingData, BusyMakingList } from '../../redux/action';
import { connect } from 'react-redux';
import _ from 'lodash';
const EmptyList = require('../../../assets/animation/emptyList.json');
const initialFilter = {
  Color: {
    content: [],
    features: false
  },
  Gender: {
    content: [],
    features: false
  },
  Sort: {
    content: null,
    features: false,
    name: 'No order'
  },
  brands: {
    content: [],
    features: false
  },
  price: {
    content: [0, 42350],
    features: false
  },
  sizes: {
    content: [],
    features: false
  }
};
class CategoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      Loading: true,
      pageSize: 0,
      count: 0,
      totalPages: 0,
      page: 1,
      SearchByURL: false,
      URL: this.params.URL ? this.params.URL : '',
      URLFilterString: this.params.FilterString ? this.params.FilterString : '',
      SelectedSubCategorySlug: this.params.slug ? this.params.slug : '',
      SearchString: this.params.searchString ? this.params.searchString : '',
      ThisSelectedSubCategoryContent: [],
      filterString: '',
      //AllFilters: [],
      //AppliedFilters: {},
      //===================
      APIOptionFilters: [],
      selectedFilters: null,
      currentIndex: -1,
      filterModalOn: false,
      //===================
      EnableModalToRender: false,
      ModalOn: false
    };
  }
  //setHeaderView = Switcher => this.setState({ HeaderEnabled: Switcher });
  //setHeaderView = SETTER => this.setState({ HeaderDis: SETTER });
  UNSAFE_componentWillMount() {
    const { BusyFetchingData } = this.props;
    const { searchString, slug, URL } = this.props.navigation.state.params;
    BusyFetchingData(true);

    let { page } = this.state;
    URL !== '' && URL !== undefined
      ? this.SearchByURLStringFirstTime()
      : searchString !== '' && searchString !== undefined
      ? this.setSearchStringAndApplied(searchString)
      : APIFuncs.GetSubCategoryContent(slug, page, '').then(RES => {
          let { count, pageSize, data } = RES;
          const totalPages = Math.ceil(count / pageSize);
          if (totalPages > 1) page = 2;
          this.setState({
            ThisSelectedSubCategoryContent: data,
            Loading: false,
            count,
            pageSize,
            totalPages,
            page
          });
        });

    APIFuncs.getFilters(
      searchString !== '' && searchString !== undefined ? searchString : null,
      slug
    ).then(filters => {
      const APIOptionFilters = [];
      const selectedFilters = {};
      APIOptionFilters.push({
        key: 'Sort',
        content: sorts,
        features: false
      });
      selectedFilters['Sort'] = {
        content: null,
        features: false,
        name: 'No order'
      };

      for (let key in filters) {
        if (filters.hasOwnProperty(key)) {
          if (key !== 'features') {
            APIOptionFilters.push({
              key,
              content: filters[key],
              features: false
            });
            selectedFilters[key] = {
              content: key !== 'price' ? [] : filters[key],
              features: false
            };
            if (key === 'price') initialFilter.price.content = filters[key];
          } else {
            filters[key].map(ITEM => {
              const { name, options } = ITEM;

              if (name) {
                APIOptionFilters.push({
                  key: name,
                  content: options,
                  features: true
                });

                selectedFilters[name] = {
                  content: [],
                  features: false
                };
              }
            });
          }
        }
      }
      this.setState({
        APIOptionFilters,
        selectedFilters,
        //InitialFilter: _.clone(selectedFilters),
        EnableView: true
      });
      BusyFetchingData(false);
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { FetchingAndGettingLoading, ListLoader } = this.props.AppState;
    if (!FetchingAndGettingLoading && !ListLoader) {
      const { length } = this.state.ThisSelectedSubCategoryContent;
      if (length === 0) this.animation.play();
    }
  }
  SearchByURLStringFirstTime = () => {
    let { URLFilterString, SelectedSubCategorySlug, page } = this.state;

    const { BusyMakingList } = this.props;
    BusyMakingList(true);
    this.setState({
      Loading: true
    });
    APIFuncs.GetSubCategoryContent(
      SelectedSubCategorySlug,
      page,
      URLFilterString
    ).then(RES => {
      let { count, pageSize, data } = RES;
      const totalPages = Math.ceil(count / pageSize);
      if (totalPages > 1) page = 2;
      data.length
        ? this.setState({
            ThisSelectedSubCategoryContent: data,
            Loading: false,
            SearchByURL: true,
            count,
            pageSize,
            totalPages,
            page
          })
        : this.setState({
            Loading: false,
            ThisSelectedSubCategoryContent: [],
            SearchByURL: true
          });
      BusyMakingList(false);
    });
  };

  SearchByURLString = () => {
    let {
      URLFilterString,
      SelectedSubCategorySlug,
      page,
      totalPages,
      ThisSelectedSubCategoryContent
    } = this.state;
    if (totalPages >= page) {
      const { BusyMakingList } = this.props;
      BusyMakingList(true);
      this.setState({
        Loading: true
      });
      APIFuncs.GetSubCategoryContent(
        SelectedSubCategorySlug,
        page,
        URLFilterString
      ).then(RES => {
        RES.data.length !== 0 &&
          this.setState({
            ThisSelectedSubCategoryContent: ThisSelectedSubCategoryContent.concat(
              RES.data
            ),
            Loading: false,
            page: page + 1,
            SearchByURL: true,
            Total: RES.count
          });
        BusyMakingList(false);
      });
    }
  };

  setAppliedFilter = AppliedFilters => {
    this.setState({
      //AppliedFilters,
      filterModalOn: false,
      currentIndex: -1
    });
    this.JustAppliedAndSearchwithFilter(AppliedFilters);
  };
  //===================================[SEARCH]=========================
  setSearchStringAndApplied = SearchString => {
    if (SearchString !== '') {
      const { BusyMakingList } = this.props;
      let { page } = this.state;
      BusyMakingList(true);
      this.setState({
        SearchString
      });

      APIFuncs.GetContentForSearch(SearchString, 1, '').then(RES => {
        const { count, pageSize, data } = RES;
        const totalPages = Math.ceil(count / pageSize);
        totalPages > 1 ? (page = 2) : (page = 1);
        this.setState({
          ThisSelectedSubCategoryContent: data,
          Loading: false,
          count,
          pageSize,
          totalPages,
          page
        });

        BusyMakingList(false);
      });
    } else {
      BusyMakingList(false);
    }
  };

  setSearchStringAndAppliedWithFilter = (SearchString, Filter) => {
    if (SearchString !== '') {
      const { BusyMakingList } = this.props;
      let { page } = this.state;
      BusyMakingList(true);
      this.setState({
        SearchString
      });

      APIFuncs.GetContentForSearch(SearchString, 1, Filter).then(RES => {
        let { count, pageSize, data } = RES;
        const totalPages = Math.ceil(count / pageSize);
        totalPages > 1 ? (page = 2) : (page = 1);
        this.setState({
          ThisSelectedSubCategoryContent: data,
          Loading: false,
          count,
          pageSize,
          totalPages,
          page
        });
        InfoLogger('SearchResult', RES);
        BusyMakingList(false);
      });
    }
  };

  clearAndSearchByDefault = () => {
    const { SearchString } = this.state;
    if (SearchString !== '') {
      this.setState({
        SearchString: ''
      });
      this.search1stTimeCategory('');
    }
  };

  //======================================================================
  _keyExtractor = (item, index) => 'MyKey' + index;

  closeFilterModal = () => {
    this.setState({
      filterModalOn: false
    });
  };
  openFilterModal = () => {
    this.setState({
      filterModalOn: true
    });
  };
  CloseSearchModal = () => {
    this.setState({
      ModalOn: false
    });
  };
  OpenSearchModal = () => {
    this.setState({
      ModalOn: true
    });
  };
  renderFooter = () =>
    this.state.Loading ? (
      <View style={{ paddingVertical: 20 }}>
        <ActivityIndicator animating size="large" />
      </View>
    ) : (
      <View style={{ width: 0, height: 0 }} />
    );

  search1stTimeCategory = SLUG => {
    const { BusyMakingList } = this.props;
    BusyMakingList(true);
    this.setState({
      Loading: true,
      ThisSelectedSubCategoryContent: []
    });
    let { AppliedFilters, SelectedSubCategorySlug, page } = this.state;
    const FilterString = stringMaker(AppliedFilters);
    APIFuncs.GetSubCategoryContent(
      SelectedSubCategorySlug,
      1,
      FilterString
    ).then(RES => {
      let { count, pageSize, data } = RES;
      const totalPages = Math.ceil(count / pageSize);
      totalPages > 1 ? (page = 2) : (page = 1);
      data.length
        ? this.setState({
            ThisSelectedSubCategoryContent: data,
            Loading: false,
            count,
            pageSize,
            totalPages,
            page
          })
        : this.setState({
            Loading: false,
            ThisSelectedSubCategoryContent: [],
            count,
            pageSize,
            totalPages,
            page
          });
      // this.refs.ContentFL.scrollToOffset({ x: 0, y: 0, animated: false });
      BusyMakingList(false);
    });
  };

  JustAppliedAndSearchwithFilter = FILTER => {
    let { SearchString, SelectedSubCategorySlug, page } = this.state;
    const { BusyMakingList } = this.props;
    BusyMakingList(true);
    this.setState({
      Loading: true
    });
    const FilterString = stringMaker(FILTER);
    // alert(FilterString);
    SearchString !== ''
      ? this.setSearchStringAndAppliedWithFilter(SearchString, FilterString)
      : APIFuncs.GetSubCategoryContent(
          SelectedSubCategorySlug,
          1,
          FilterString
        ).then(RES => {
          let { count, pageSize, data } = RES;
          const totalPages = Math.ceil(count / pageSize);
          totalPages > 1 ? (page = 2) : (page = 1);
          data.length
            ? this.setState({
                ThisSelectedSubCategoryContent: RES.data,
                Loading: false,
                count,
                pageSize,
                totalPages,
                page
              })
            : this.setState({
                Loading: false,
                ThisSelectedSubCategoryContent: [],
                count,
                pageSize,
                totalPages,
                page
              });

          // this.refs.ContentFL.scrollToOffset({ x: 0, y: 0, animated: false });
          BusyMakingList(false);
        });
    // SearchString &&
    //   APIFuncs.getFilters(SearchString, SelectedSubCategorySlug).then(
    //     filters => {
    //       const APIOptionFilters = [];
    //       const selectedFilters = {};
    //       APIOptionFilters.push({
    //         key: "Sort",
    //         content: sorts,
    //         features: false
    //       });
    //       selectedFilters["Sort"] = {
    //         content: null,
    //         features: false,
    //         name: "No order"
    //       };

    //       for (let key in filters) {
    //         if (filters.hasOwnProperty(key)) {
    //           if (key !== "features") {
    //             APIOptionFilters.push({
    //               key,
    //               content: filters[key],
    //               features: false
    //             });
    //             selectedFilters[key] = {
    //               content: key !== "price" ? [] : filters[key],
    //               features: false
    //             };
    //             if (key === "price") initialFilter.price.content = filters[key];
    //           } else {
    //             filters[key].map(ITEM => {
    //               const { name, options } = ITEM;

    //               if (name) {
    //                 APIOptionFilters.push({
    //                   key: name,
    //                   content: options,
    //                   features: true
    //                 });

    //                 selectedFilters[name] = {
    //                   content: [],
    //                   features: false
    //                 };
    //               }
    //             });
    //           }
    //         }
    //       }
    //       this.setState({
    //         currentIndex: -1,
    //         filterModalOn: false,
    //         APIOptionFilters,
    //         selectedFilters,
    //         //InitialFilter: _.clone(selectedFilters),
    //         EnableView: true
    //       });
    //    BusyFetchingData(false);
    //}
    // );
  };

  handleLoadMore = () => {
    let {
      ThisSelectedSubCategoryContent,
      totalPages,
      AppliedFilters,
      SelectedSubCategorySlug,
      SearchString,
      URLFilterString,
      SearchByURL,
      page
    } = this.state;
    if (totalPages > page) {
      const { BusyMakingList } = this.props;
      this.setState({
        Loading: false
      });
      BusyMakingList(true);
      const FilterString = stringMaker(AppliedFilters);
      SearchByURL
        ? this.SearchByURLString()
        : SearchString === ''
        ? APIFuncs.GetSubCategoryContent(
            SelectedSubCategorySlug,
            page,
            FilterString
          ).then(RES => {
            const { data } = RES;
            page = page + 1;
            if (data.length) {
              const NewDATA = ThisSelectedSubCategoryContent.concat(data);
              this.setState({
                ThisSelectedSubCategoryContent: NewDATA,
                Loading: false,
                page
              });
            } else {
              this.setState({
                Loading: false,
                page
              });
            }
            BusyMakingList(false);
          })
        : APIFuncs.GetContentForSearch(SearchString, page, '').then(RES => {
            const { data } = RES;
            page = page + 1;
            //console.warn("PAGE: ", page + " " + totalPages);
            if (data.length) {
              const NewDATA = ThisSelectedSubCategoryContent.concat(data);
              this.setState({
                ThisSelectedSubCategoryContent: NewDATA,
                Loading: false,
                page
              });
            } else {
              this.setState({
                Loading: false,
                page
              });
            }
            BusyMakingList(false);
          });
    }
  };

  clearAllFilters2 = () => {
    APIFuncs.getFilters(this.state.SelectedSubCategorySlug).then(filters => {
      const APIOptionFilters = [];
      const selectedFilters = {};
      APIOptionFilters.push({
        key: 'Sort',
        content: sorts,
        features: false
      });
      selectedFilters['Sort'] = {
        content: null,
        features: false,
        name: 'No order'
      };

      for (let key in filters) {
        if (filters.hasOwnProperty(key)) {
          if (key !== 'features') {
            APIOptionFilters.push({
              key,
              content: filters[key],
              features: false
            });
            selectedFilters[key] = {
              content: key !== 'price' ? [] : filters[key],
              features: false
            };
          } else {
            filters[key].map(ITEM => {
              const { name, options } = ITEM;

              if (name) {
                APIOptionFilters.push({
                  key: name,
                  content: options,
                  features: true
                });

                selectedFilters[name] = {
                  content: [],
                  features: false
                };
              }
            });
          }
        }
      }
      this.setState({
        APIOptionFilters,
        selectedFilters,
        //InitialFilter: _.clone(selectedFilters),
        EnableView: true
      });
    });
  };
  clearAllFilter = () => {
    this.clearAllFilters2();
    //const { InitialFilter } = this.state;
    //this.setState({ selectedFilters: APIFuncs.getFilters(this.state.SelectedSubCategorySlug) });
    //APIFuncs.getFilters(this.state.SelectedSubCategorySlug)
    //console.log("Pressed", "ClearMAinPageFilter");
  };

  CloseButMaintainAppliedFilter = ModalHandler => {
    const { AppliedFilters } = this.state;
    ModalHandler.setState({
      thisappliedFilters: AppliedFilters,
      currentIndex: -1
    });
    this.setState({
      filterModalOn: false
    });
    InfoLogger('Its Here', AppliedFilters);
  };
  //=============================================================================
  PriceView = ({ value, content, onRheostatValUpdated }) => (
    <RheostatThemeProvider theme={{ themeColor: '#000000', grey: '#fafafa' }}>
      <Rheostat
        values={value}
        min={content[0]}
        max={content[1]}
        onValuesUpdated={onRheostatValUpdated}
      />
    </RheostatThemeProvider>
  );

  SetIndex = currentIndex =>
    this.setState({
      currentIndex
    });

  // triggerApply = () => {
  //   const deep = _.cloneDeep(this.state.selectedFilters);
  //   this.props.triggerApply(deep);
  //   this.props.closeFunc();
  // };

  ModalClearAllFilter = () => {
    const deep = _.cloneDeep(initialFilter);
    //console.log("DEleteBtn Pressed", deep);
    this.setState({ selectedFilters: deep, currentIndex: -1 });
  };
  CloseModal = () => {
    this.props.closeFunc();
  };

  putFilter = (key, word, nameForSort) => {
    const { selectedFilters } = this.state;
    if (key === 'Sort') {
      selectedFilters['Sort'].content = word;
      selectedFilters['Sort'].name = nameForSort;
    } else {
      const FindAt = selectedFilters[key].content.indexOf(word);
      if (FindAt === -1) {
        selectedFilters[key].content.push(word);
      } else {
        selectedFilters[key].content.splice(FindAt, 1);
      }
    }
    this.setState({
      selectedFilters
    });
  };

  onRheostatValUpdated = ({ values }) => {
    const { selectedFilters } = this.state;
    selectedFilters['price'].content = values;
    this.setState({
      selectedFilters
    });
  };
  //==================================================================
  render() {
    const { navigation } = this.props;
    const {
      TITLE,
      ThisSelectedSubCategoryContent,
      SelectedSubCategorySlug,
      Loading,
      Skip,
      AllFilters,
      AppliedFilters,
      filterModalOn,
      EnableModalToRender,
      selectedFilters,
      currentIndex,
      InitialFilter,
      APIOptionFilters,
      SearchString,
      ModalOn,
      HeaderEnabled,
      HeaderDis
    } = this.state;

    const {
      PriceView,
      onRheostatValUpdated,
      putFilter,
      triggerApply,
      CloseModal,
      CloseSearchModal,
      OpenSearchModal
    } = this;

    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalOn}
          onRequestClose={CloseSearchModal}
        >
          <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(100)
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(7),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                borderBottomWidth: widthPercentageToDP(0.2)
              }}
            >
              <TouchableOpacity
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'absolute',
                  left: '2%'
                }}
                onPress={CloseSearchModal}
              >
                <ICON size={widthPercentageToDP(4)} name="close" />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: widthPercentageToDP(6)
                }}
              >
                {'Search'}
              </Text>
            </View>
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(7),
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: '1%',
                flexDirection: 'row',
                borderBottomWidth: widthPercentageToDP(0.2)
              }}
            >
              <View
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <ICON size={widthPercentageToDP(6)} name="search" />
              </View>
              <View
                style={{
                  width: widthPercentageToDP(75),
                  height: heightPercentageToDP(6),
                  fontSize: widthPercentageToDP(5),
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderLeftWidth: widthPercentageToDP(0.2),
                  borderRightWidth: widthPercentageToDP(0.2),
                  borderColor: '#E1E1E1'
                }}
              >
                <TextInput
                  onChangeText={SearchString => this.setState({ SearchString })}
                  value={this.state.SearchString}
                  style={{
                    width: widthPercentageToDP(72),
                    height: heightPercentageToDP(6),
                    fontSize: widthPercentageToDP(5)
                  }}
                  placeholder={'Search Product'}
                  onSubmitEditing={() => {
                    if (SearchString) {
                      const { BusyFetchingData } = this.props;
                      BusyFetchingData(true);
                      SearchString;
                      this.setSearchStringAndApplied(SearchString);

                      APIFuncs.getFilters(SearchString, '').then(filters => {
                        console.log('Filters', filters);
                        const APIOptionFilters = [];
                        const selectedFilters = {};
                        APIOptionFilters.push({
                          key: 'Sort',
                          content: sorts,
                          features: false
                        });
                        selectedFilters['Sort'] = {
                          content: null,
                          features: false,
                          name: 'No order'
                        };

                        for (let key in filters) {
                          if (filters.hasOwnProperty(key)) {
                            if (key !== 'features') {
                              APIOptionFilters.push({
                                key,
                                content: filters[key],
                                features: false
                              });
                              selectedFilters[key] = {
                                content: key !== 'price' ? [] : filters[key],
                                features: false
                              };
                              if (key === 'price')
                                initialFilter.price.content = filters[key];
                            } else {
                              filters[key].map(ITEM => {
                                const { name, options } = ITEM;

                                if (name) {
                                  APIOptionFilters.push({
                                    key: name,
                                    content: options,
                                    features: true
                                  });

                                  selectedFilters[name] = {
                                    content: [],
                                    features: false
                                  };
                                }
                              });
                            }
                          }
                        }
                        this.setState({
                          APIOptionFilters,
                          selectedFilters,
                          //InitialFilter: _.clone(selectedFilters),
                          EnableView: true
                        });
                        BusyFetchingData(false);
                      });
                      CloseSearchModal();
                    }
                  }}
                  returnKeyType="search"
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  if (SearchString) {
                    const { BusyFetchingData } = this.props;
                    BusyFetchingData(true);
                    SearchString;
                    this.setSearchStringAndApplied(SearchString);

                    APIFuncs.getFilters(SearchString, '').then(filters => {
                      console.log('Filters', filters);
                      const APIOptionFilters = [];
                      const selectedFilters = {};
                      APIOptionFilters.push({
                        key: 'Sort',
                        content: sorts,
                        features: false
                      });
                      selectedFilters['Sort'] = {
                        content: null,
                        features: false,
                        name: 'No order'
                      };

                      for (let key in filters) {
                        if (filters.hasOwnProperty(key)) {
                          if (key !== 'features') {
                            APIOptionFilters.push({
                              key,
                              content: filters[key],
                              features: false
                            });
                            selectedFilters[key] = {
                              content: key !== 'price' ? [] : filters[key],
                              features: false
                            };
                            if (key === 'price')
                              initialFilter.price.content = filters[key];
                          } else {
                            filters[key].map(ITEM => {
                              const { name, options } = ITEM;

                              if (name) {
                                APIOptionFilters.push({
                                  key: name,
                                  content: options,
                                  features: true
                                });

                                selectedFilters[name] = {
                                  content: [],
                                  features: false
                                };
                              }
                            });
                          }
                        }
                      }
                      this.setState({
                        APIOptionFilters,
                        selectedFilters,
                        //InitialFilter: _.clone(selectedFilters),
                        EnableView: true
                      });
                      BusyFetchingData(false);
                    });
                    CloseSearchModal();
                  }
                }}
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <ICON size={widthPercentageToDP(6)} name="chevron-right" />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={false}
          visible={filterModalOn}
          onRequestClose={this.closeFilterModal}
        >
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={this.closeFilterModal}
            LeftIcon="close"
            title="FILTER"
            Right1ClickHandler={this.ModalClearAllFilter}
            Right1con="delete"
          />

          <View style={styles.container}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
              {selectedFilters !== null &&
                APIOptionFilters.map((item, index) => {
                  const { content, key, features } = item;
                  return (
                    <FilterSections
                      key={'Filter' + index}
                      currentIndex={currentIndex}
                      SetIndex={this.SetIndex}
                      title={key === 'Sort' ? selectedFilters[key].name : key}
                      sectionkey={key}
                      number={index}
                      expanded={currentIndex === index}
                    >
                      {!features && key !== 'price' ? (
                        key === 'Sort' ? (
                          <View style={styles.filterContent}>
                            <FlatList
                              keyExtractor={this._keyExtractor}
                              showsVerticalScrollIndicator={false}
                              data={content}
                              extraData={this.state}
                              renderItem={({ item }) => {
                                const { name, val } = item;

                                const Selected =
                                  selectedFilters[key].content === val;
                                return (
                                  <TouchableOpacity
                                    onPress={() => {
                                      putFilter(key, val, name);
                                    }}
                                  >
                                    <Text
                                      style={{
                                        padding: widthPercentageToDP(2),
                                        fontSize: widthPercentageToDP(4),
                                        fontWeight: Selected ? 'bold' : 'normal'
                                      }}
                                    >
                                      {firstLetterCapital(name)}
                                    </Text>
                                  </TouchableOpacity>
                                );
                              }}
                            />
                          </View>
                        ) : (
                          <View style={styles.filterContent}>
                            <FlatList
                              keyExtractor={this._keyExtractor}
                              showsVerticalScrollIndicator={false}
                              data={content}
                              extraData={this.state}
                              numColumns={2}
                              renderItem={({ item }) => {
                                const { name } = item;
                                const Selected =
                                  selectedFilters[key].content.indexOf(name) !==
                                  -1;
                                if (name)
                                  return (
                                    <TouchableOpacity
                                      style={{
                                        width: '50%',
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                      }}
                                      onPress={() => {
                                        putFilter(key, name, name);
                                      }}
                                    >
                                      {Selected && (
                                        <ICON
                                          name="brightness-1"
                                          style={{ marginVertical: 5 }}
                                          size={widthPercentageToDP(3)}
                                          color="#333"
                                        />
                                      )}
                                      <Text
                                        style={{
                                          padding: widthPercentageToDP(2),
                                          fontSize: widthPercentageToDP(4),
                                          fontWeight: Selected
                                            ? 'bold'
                                            : 'normal'
                                        }}
                                      >
                                        {firstLetterCapital(name)}
                                      </Text>
                                    </TouchableOpacity>
                                  );
                              }}
                            />
                          </View>
                        )
                      ) : features ? (
                        <View style={styles.filterContent}>
                          <FlatList
                            keyExtractor={this._keyExtractor}
                            showsVerticalScrollIndicator={false}
                            numColumns={2}
                            data={content}
                            extraData={this.state}
                            renderItem={({ item }) => {
                              const { name } = item;
                              const Selected =
                                selectedFilters[key].content.indexOf(name) !==
                                -1;
                              if (name)
                                return (
                                  <TouchableOpacity
                                    style={{
                                      width: '50%',
                                      flexDirection: 'row',
                                      alignItems: 'center'
                                    }}
                                    onPress={() => {
                                      putFilter(key, name, name);
                                    }}
                                  >
                                    {Selected && (
                                      <ICON
                                        name="brightness-1"
                                        style={{ marginVertical: 5 }}
                                        size={widthPercentageToDP(3)}
                                        color="#333"
                                      />
                                    )}
                                    <Text
                                      style={{
                                        padding: widthPercentageToDP(2),
                                        fontSize: widthPercentageToDP(4),
                                        fontWeight: Selected ? 'bold' : 'normal'
                                      }}
                                    >
                                      {firstLetterCapital(name)}
                                    </Text>
                                  </TouchableOpacity>
                                );
                            }}
                          />
                        </View>
                      ) : (
                        key === 'price' && (
                          <View style={styles.headerText}>
                            <PriceView
                              value={selectedFilters[key].content}
                              content={content}
                              onRheostatValUpdated={onRheostatValUpdated}
                            />
                            <View
                              style={{
                                flexDirection: 'row',
                                width: '100%',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                paddingHorizontal: '5%',
                                paddingTop: '5%'
                              }}
                            >
                              <Text>
                                {'Min:'} {selectedFilters[key].content[0]}
                              </Text>
                              <Text>
                                {'Max:'} {selectedFilters[key].content[1]}
                              </Text>
                            </View>
                          </View>
                        )
                      )}
                    </FilterSections>
                  );
                })}
            </ScrollView>
            <View
              style={{
                alignItems: 'center',
                marginVertical: heightPercentageToDP(1.5)
              }}
            >
              <ClickyButton
                onPress={() =>
                  this.setAppliedFilter(this.state.selectedFilters)
                }
                text={'APPLY'}
              />
              {/* <TouchableOpacity
                onPress={() =>
                  this.setAppliedFilter(this.state.selectedFilters)
                }
                style={{
                  height: heightPercentageToDP(8),
                  width: widthPercentageToDP(90),
                  borderRadius: widthPercentageToDP(1),
                  elevation: 3,
                  backgroundColor: "#000",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 18,
                    fontWeight: "300",
                    color: "#fff"
                  }}
                >
                  {"APPLY"}
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </Modal>
        <View
          style={{
            // top: heightPercentageToDP(-HeaderDis),
            width: widthPercentageToDP(100)
          }}
        >
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={() => navigation.goBack()}
            LeftIcon="arrow-back"
            title="CATEGORY"
            Right1ClickHandler={this.openFilterModal}
            Right1con={'tune'}
          />
        </View>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(90)
            // top: heightPercentageToDP(0)
          }}
        >
          {ThisSelectedSubCategoryContent.length ? (
            <PullandBear
              style={{
                width: widthPercentageToDP(100)
              }}
              //headerAnim={this.setHeaderView}
              //ModalOpener={this.setSearchStringAndApplied}
              ModalOpener={OpenSearchModal}
              SearchString={this.state.SearchString}
              data={ThisSelectedSubCategoryContent}
              navigation={navigation}
              onEndReached={() => {
                this.setState({ Loading: true });
                this.handleLoadMore();
              }}
              refreshing={true}
              extraData={this.state}
              clearfun={this.clearAndSearchByDefault}
              //onRefresh={() =>
              //this.search1stTimeCategory(SelectedSubCategorySlug)
              //}
            />
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              {!this.props.AppState.FetchingAndGettingLoading &&
                !this.props.AppState.ListLoader && (
                  <View
                    style={{
                      width: widthPercentageToDP(100),
                      height: heightPercentageToDP(80),
                      backgroundColor: '#fff',
                      alignItems: 'center'
                    }}
                  >
                    <Lottie
                      ref={animation => {
                        this.animation = animation;
                      }}
                      autoPlay={true}
                      loop={false}
                      style={{
                        width: widthPercentageToDP(100),
                        height: heightPercentageToDP(60)
                      }}
                      source={EmptyList}
                    />
                    {/* <TouchableOpacity
                      onPress={() => {
                        navigation.goBack();
                      }}
                      style={{
                        height: heightPercentageToDP(8),
                        width: widthPercentageToDP(90),
                        borderRadius: widthPercentageToDP(1),
                        elevation: 3,
                        backgroundColor: "#000",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          fontSize: 18,
                          fontWeight: "300",
                          color: "#fff"
                        }}
                      >
                        {"Go Back To Home"}
                      </Text>
                    </TouchableOpacity> */}
                    <ClickyButton
                      onPress={() => {
                        navigation.goBack();
                      }}
                      text={'Go Back To Home'}
                    />
                  </View>
                )}
            </View>
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
    //paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20
  },
  header: {
    backgroundColor: '#FFF',
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerText: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    padding: 10
  },
  content: {
    padding: 20,
    backgroundColor: '#fff'
  },
  filterContent: {
    flex: 1,
    padding: 10,
    backgroundColor: '#f1f1f1'
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)'
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)'
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10
  },
  activeSelector: {
    fontWeight: 'bold'
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center'
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8
  },
  touchableStyle: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#000'
  }
});

export const PopularSearchMethod = props => {
  return (
    <TouchableOpacity

    //onPress={() => props.clickHandler(props.nav, props.param)}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text style={{ color: 'black', fontSize: 18, fontWeight: '300' }}>
            {props.item.text}
          </Text>
        </View>
        {/* <View style = {{backgroundColor:'black', height:2,flex:1, flexDirection: 'row'}}>
          </View> */}
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    AppState: state.APPSTATE
  };
};
export default connect(
  mapStateToProps,
  { BusyFetchingData, BusyMakingList }
)(CategoryPage);
