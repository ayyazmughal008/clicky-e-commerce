'use strict';
import React from 'react';
import { View, Text, StatusBar, Modal, Alert } from 'react-native';
import styles from './styles1';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import { TextField } from 'react-native-material-textfield';
import { phoneNumberValidator } from '../../modules/utilts';
import { ClickyButton } from '../../component/Buttons';
import ParentHeaders from '../../component/Header';
import AlertModal from '../../component/AlertModal';
export default class Recovery extends React.Component {
  state = {
    Etext: '',
    codeText: '',
    newPassword: '',
    confirmPassword: '',
    Loading: false,
    forgetPassword: false,
    RenderNextView: false,
    AlertModalOn: false,
    alertContent: {
      header: '',
      message: '',
      type: 'info'
    }
  };

  ValidateBeforeSend = () => {
    const { Etext } = this.state;
    const { valid, internationalFormatcell: phone } = phoneNumberValidator(
      Etext
    );
    if (valid) {
      this.props.resetPassword({ phone });
      this.setState({ RenderNextView: true });
    } else {
      Alert.alert('Error!', 'Please provide valid phone number');
    }
  };
  ValidateBeforeResentPassword = () => {
    const { newPassword: password, codeText: resetCode, Etext } = this.state;
    const { validatePassword, validateCode } = this;
    const { valid, internationalFormatcell: phone } = phoneNumberValidator(
      Etext
    );
    if (valid && validatePassword() && validateCode()) {
      this.props
        .sendNewPassword({ password, resetCode, phone })
        .then(({ status, message }) => {
          if (status === 'Success' && message === 'Password has been changed') {
            this.setState(
              {
                RenderNextView: false,
                alertContent: {
                  header: 'Success!',
                  message: 'Password reset',
                  type: 'success'
                }
              },
              () => {
                this.toggleAlertModal(true);
              }
            );
          }
        });
    }
  };
  validatePassword = () => {
    const { confirmPassword, newPassword } = this.state;
    if (newPassword.length > 7 && confirmPassword === newPassword) {
      console.log('Password:', 'True');
      return true;
    }
    return false;
  };
  validateCode = () => {
    const { codeText } = this.state;
    if (codeText !== '') {
      console.log('code:', 'True');
      return true;
    }
    return false;
  };
  renderPreviousView = () => {
    this.setState({ RenderNextView: false });
  };
  toggleAlertModal = AlertModalOn => {
    this.setState({ AlertModalOn });
  };
  render() {
    const { ModalOn, navigation, CloseModal } = this.props;
    const {
      Etext,
      codeText,
      newPassword,
      confirmPassword,
      RenderNextView,
      AlertModalOn,
      alertContent
    } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ModalOn}
        onRequestClose={CloseModal}
      >
        <AlertModal
          alertContent={alertContent}
          toggaleFunc={this.toggleAlertModal}
          onSwitch={AlertModalOn}
        />
        <View
          style={{
            backgroundColor: '#FFF',
            alignItems: 'center',
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(100)
          }}
        >
          <StatusBar
            animated={true}
            backgroundColor={'#FFF'}
            barStyle={'light-content'}
          />
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={
              RenderNextView ? this.renderPreviousView : CloseModal
            }
            LeftIcon="arrow-back"
            title="Recover Password"
          />
          <View style={styles.otherThenStatusBarView}>
            {RenderNextView ? (
              <View style={styles.FormView}>
                <Text style={styles.FormHeader1}>
                  {'Received password reset verification code?'}
                </Text>
                <Text style={styles.FormHeader2}>
                  {
                    'Enter your new password with the received verification code'
                  }
                </Text>
                <View style={{ height: '40%', width: '100%', padding: '5%' }}>
                  <TextField
                    value={newPassword}
                    labelPadding={1}
                    labelHeight={heightPercentageToDP(1)}
                    autoCorrect={false}
                    secureTextEntry={true}
                    label="PASSWORD"
                    maxLength={40}
                    tintColor={'#111'}
                    onChangeText={newPassword =>
                      this.setState({
                        newPassword
                      })
                    }
                  />
                  <TextField
                    value={confirmPassword}
                    labelPadding={1}
                    labelHeight={heightPercentageToDP(1)}
                    autoCorrect={false}
                    secureTextEntry={true}
                    label="CONFIRM PASSWORD"
                    maxLength={40}
                    tintColor={'#111'}
                    onChangeText={confirmPassword =>
                      this.setState({
                        confirmPassword
                      })
                    }
                  />
                  <TextField
                    value={codeText}
                    labelPadding={1}
                    keyboardType="number-pad"
                    labelHeight={heightPercentageToDP(1)}
                    autoCorrect={false}
                    label="VERIFICATION CODE"
                    maxLength={8}
                    tintColor={'#111'}
                    onChangeText={codeText =>
                      this.setState({
                        codeText: codeText.replace(/\s/g, '')
                      })
                    }
                  />
                  <ClickyButton
                    style={{ marginTop: '5%' }}
                    onPress={this.ValidateBeforeResentPassword}
                    text={'SUBMIT'}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.FormView}>
                <Text style={styles.FormHeader1}>{'Forgot Password?'}</Text>
                <Text style={styles.FormHeader2}>
                  {
                    "It's okay,it can happen to anyone. Enter your phone number and we'll send your reset code."
                  }
                </Text>
                <View style={{ height: '40%', width: '100%', padding: '5%' }}>
                  <TextField
                    onSubmitEditing={this.ValidateBeforeSend}
                    value={Etext}
                    labelPadding={1}
                    keyboardType="phone-pad"
                    labelHeight={heightPercentageToDP(1)}
                    autoCorrect={false}
                    prefix={'+92'}
                    label="PHONE"
                    maxLength={11}
                    tintColor={'#111'}
                    onChangeText={Etext =>
                      this.setState({
                        Etext: Etext
                      })
                    }
                  />
                  <ClickyButton
                    style={{ marginTop: '5%' }}
                    onPress={this.ValidateBeforeSend}
                    text={'SEND'}
                  />
                </View>
              </View>
            )}
          </View>
        </View>
      </Modal>
    );
  }
}
