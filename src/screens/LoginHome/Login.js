'use strict';
import React from 'react';
import { View, Text, ScrollView, Alert, TouchableOpacity } from 'react-native';
import Style from './styles';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import { phoneNumberValidator, EmailValidator } from '../../modules/utilts';
import { ClickyButton } from '../../component/Buttons';
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import {
  signInUser,
  resetPassword,
  signInUserWithPhone,
  sendNewPassword
} from '../../redux/action';
import RecoveryModal from './RecoveryModal';
class LOGIN extends React.Component {
  state = {
    LoginEmail: '',
    Phone_Email: '',
    LoginPassword: '',
    loading: false,
    emailError: null,
    passwordError: null,
    isFormValid: false,
    RecoveryModalOn: false
  };

  onLogin = () => {
    const { Phone_Email, LoginPassword: password } = this.state;
    const { ExpoSate, signInUserWithPhone, signInUser } = this.props;
    const { ExpoToken: expoPushToken } = ExpoSate;
    const { valid, internationalFormatcell: phone } = phoneNumberValidator(
      Phone_Email
    );
    this.setState({ loading: true });
    if (valid && password) {
      const signInData = {
        phone,
        password,
        expoPushToken
      };
      signInUserWithPhone(signInData).then(() => {
        this.setState({ loading: false });
      });
    } else if (EmailValidator(Phone_Email) && password) {
      const signInData = {
        email: Phone_Email,
        password,
        expoPushToken
      };
      signInUser(signInData);
    } else {
      Alert.alert('Username or password is not correct');
      this.setState({ isFormValid: false, loading: false });
    }
  };

  render() {
    const { resetPassword, sendNewPassword } = this.props;
    const { Phone_Email, LoginPassword, RecoveryModalOn, loading } = this.state;
    return (
      <ScrollView>
        <RecoveryModal
          ModalOn={RecoveryModalOn}
          resetPassword={resetPassword}
          sendNewPassword={sendNewPassword}
          CloseModal={() => {
            this.setState({
              RecoveryModalOn: false
            });
          }}
        />
        <View style={Style.Forms}>
          <TextField
            onSubmitEditing={() => {
              this.TextInput2.focus();
            }}
            value={Phone_Email}
            //keyboardType="email-address"
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            autoCapitalize="none"
            returnKeyType={'next'}
            label="PHONE/EMAIL"
            maxLength={40}
            tintColor={'#111'}
            onChangeText={Phone_Email =>
              this.setState({
                Phone_Email: Phone_Email.toLowerCase()
              })
            }
          />

          <TextField
            onSubmitEditing={this.onLogin}
            ref={input => {
              this.TextInput2 = input;
            }}
            value={LoginPassword}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PASSWORD"
            maxLength={40}
            tintColor={'#111'}
            onChangeText={LoginPassword =>
              this.setState({
                LoginPassword
              })
            }
            secureTextEntry={true}
          />

          <View
            style={{
              height: '5%',
              width: '100%',
              alignItems: 'center',
              flexDirection: 'row-reverse',
              marginBottom: '5%'
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  RecoveryModalOn: true
                })
              }
            >
              <Text style={{ fontSize: widthPercentageToDP(3.5) }}>
                {'Forgot your Password?'}
              </Text>
            </TouchableOpacity>
          </View>

          <ClickyButton
            onPress={this.onLogin}
            text={'LOGIN'}
            loading={loading}
          />
        </View>
      </ScrollView>
    );
  }
}
const mapStateToProps = state => ({
  ExpoSate: state.ExpoTokenState
});

export default connect(
  mapStateToProps,
  { signInUser, resetPassword, signInUserWithPhone, sendNewPassword }
)(LOGIN);
