'use strict';
import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import Style from './styles';
import PropsType from 'prop-types';
import { widthPercentageToDP } from '../../modules/MakeMeResponsive';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { connect } from 'react-redux';
import SignUpSection from './Signup';
import LogInSection from './Login';
import { FaceBookLogger } from '../../redux/action';
const LOGO = require('../../../assets/images/Login_logo.png');
import { Icon } from 'react-native-elements';
const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width
};
const _renderTabBar = props => (
  <TabBar
    {...props}
    style={{ backgroundColor: '#FFF' }}
    indicatorStyle={{
      backgroundColor: '#111',
      width: widthPercentageToDP(15),
      justifyContent: 'space-around',
      marginHorizontal: widthPercentageToDP(17.5)
    }}
    labelStyle={{
      fontSize: widthPercentageToDP(4),
      fontWeight: 'bold',
      color: '#111'
    }}
  />
);
class loginHome extends React.Component {
  static propTypes = {
    FaceBookLogger: PropsType.func,
    ExpoSate: PropsType.object
  };
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'LOG IN' },
      { key: 'second', title: 'SIGN UP' }
    ]
  };

  render() {
    const { FaceBookLogger, ExpoSate, modalFunc, navigation } = this.props;
    const { navigate } = navigation;
    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />

        <View style={Style.statusBarBackground} />
        <View style={{ height: '5%' }} />
        <View style={Style.otherThenStatusBarView}>
          <TouchableOpacity
            style={{ position: 'absolute', top: '0%', right: '5%', zIndex: 3 }}
            onPress={() => {
              modalFunc !== undefined ? modalFunc() : navigation.goBack();
            }}
          >
            <Icon name="close" />
          </TouchableOpacity>
          <View style={Style.LOGOView}>
            <Image source={LOGO} style={Style.LOGO} resizeMode={'contain'} />
          </View>
          <View style={Style.OtherTHenLOGOView}>
            <KeyboardAvoidingView
              enabled={false}
              style={{ height: '80%' }}
              behavior={'padding'}
              //keyboardVerticalOffset={10}
            >
              <TabView
                swipeEnabled={false}
                navigationState={this.state}
                renderScene={SceneMap({
                  first: () => <LogInSection navigate={navigate} />,
                  second: () => <SignUpSection navigate={navigate} />
                })}
                renderTabBar={_renderTabBar}
                onIndexChange={index => this.setState({ index })}
                initialLayout={initialLayout}
              />
            </KeyboardAvoidingView>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginVertical: 2
              }}
            >
              <View
                style={{
                  backgroundColor: '#D3D3D3',
                  height: 2,
                  width: 30,
                  alignSelf: 'center'
                }}
              />
              <Text
                style={{
                  alignSelf: 'center',
                  paddingHorizontal: 5,
                  fontSize: 14,
                  color: '#D3D3D3'
                }}
              >
                Or Join with
              </Text>
              <View
                style={{
                  backgroundColor: '#D3D3D3',
                  height: 2,
                  width: 30,
                  alignSelf: 'center'
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: widthPercentageToDP(2),
                marginHorizontal: widthPercentageToDP(15),
                width: widthPercentageToDP(70),
                // height: heightPercentageToDP(8),
                justifyContent: 'space-around',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity
                onPress={() => FaceBookLogger(ExpoSate.ExpoToken)}
              >
                <Image
                  source={require('../../../assets/images/FB.png')}
                  style={{
                    width: widthPercentageToDP(12),
                    height: widthPercentageToDP(12),
                    borderRadius: widthPercentageToDP(6)
                  }}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
              {/* <TouchableOpacity
                onPress={() => {
                  alert('InProgress');
                }}
              >
                <Image
                  source={require('../../../assets/images/google.png')}
                  style={{
                    width: widthPercentageToDP(12),
                    height: widthPercentageToDP(12),
                    borderRadius: widthPercentageToDP(6)
                  }}
                  resizeMode={'contain'}
                />
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  ExpoSate: state.ExpoTokenState
});

export default connect(
  mapStateToProps,
  { FaceBookLogger }
)(loginHome);
