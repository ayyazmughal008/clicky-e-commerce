import { StyleSheet, StatusBar } from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
export default StyleSheet.create({
  LOGO: {
    height: '50%',
    width: '60%'
  },
  LOGOView: {
    height: '15%',
    width: widthPercentageToDP(100),
    backgroundColor: '#FFF',
    alignItems: 'center',
    marginTop: 10
  },
  cont: {
    width: widthPercentageToDP(100),
    height: heightPercentageToDP(100),
    backgroundColor: '#FFF',
    alignItems: 'center'
  },
  Forms: {
    flex: 1,
    paddingVertical: '6%',
    paddingHorizontal: '5%',
    backgroundColor: '#FFF'
  },
  RegisterHeaderText: {
    fontSize: widthPercentageToDP(4),
    fontWeight: 'bold',
    marginBottom: '5%',
    marginTop: '10%'
  },
  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: '#FFF',
    width: '100%'
  },
  otherThenStatusBarView: { flex: 1, backgroundColor: '#FFF', width: '100%' },
  OtherTHenLOGOView: {
    zIndex:5,
    height: '85%',
    width: widthPercentageToDP(100),
    backgroundColor: '#FFF'
  }
});
