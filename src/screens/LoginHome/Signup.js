'use strict';
import React from 'react';
import { View, Text, ScrollView, Alert } from 'react-native';
import Style from './styles';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import {
  phoneNumberValidator,
  InfoLogger,
  NameValidator
} from '../../modules/utilts';
import { connect } from 'react-redux';
import {
  signUpUserWithPhone,
  signInUserWithPhone,
  otpVerifyPhone,
  resendSignUp
} from '../../redux/action';
import { ClickyButton, RadioButton } from '../../component/Buttons';
import { TextField } from 'react-native-material-textfield';
import { APIFuncs } from '../../modules/APIs';
import VerifyPhoneModal from '../../component/verifyPhoneModal';
import RadioArea from '../../component/RadioButton';
import AlertModal from '../../component/AlertModal';
const {
  sendSmsVerification,
  getSmsAuth,
  otpVerification,
  postSignUpWithPhone
} = APIFuncs;
class SIGNUP extends React.Component {
  state = {
    //========================
    phoneValide: true,
    nameValide: true,
    passwordValide: true,
    phone: '',
    password: '',
    confirmPassword: '',
    name: '',
    dob: '',
    gender: 'male',
    internationalFormatcell: '',
    //------------------------
    phoneError: null,
    nameError: null,
    passwordError: null,
    isFormValid: false,
    //========================
    ModalOn: false,
    text: '',
    AlertModalOn: false,
    alertContent: {
      header: '',
      message: '',
      type: 'info'
    },
    //--
    loading: false
  };
  toggleAlertModal = AlertModalOn => {
    this.setState({ AlertModalOn });
  };
  onPressSignup = async () => {
    const { name, gender, dob, password, internationalFormatcell } = this.state;
    const { ExpoSate, signUpUserWithPhone } = this.props;
    const { ExpoToken } = ExpoSate;
    let firstName = '',
      lastName = '';
    const FirstOccurance = name.indexOf(' ');
    if (FirstOccurance !== 0) {
      firstName = name.substr(0, FirstOccurance);
      lastName = name.substr(FirstOccurance + 1);
    }

    const signupData = {
      strategy: 'phone',
      email: internationalFormatcell + '@gmail.com',
      password,
      firstName,
      lastName,
      dob: dob === '' ? '00-00-0000' : dob,
      gender,
      role: 'user',
      expoPushToken: ExpoToken,
      phone: internationalFormatcell
    };

    if (
      this.validatePassword() &&
      this.validateName() &&
      this.validatePhone()
    ) {
      console.log('Sign Up Data', signupData);
      this.setState({ isFormValid: true, loading: true });
      await postSignUpWithPhone(signupData).then(Result => {
        console.log('ResultReceived', Result);
        const { name, message, errors } = Result;
        if (errors === undefined) {
          this.setState({ signupStarted: true, ModalOn: true });
        } else {
          if (name === 'Conflict') {
            this.setState(
              {
                alertContent: {
                  header: 'Error!',
                  message: 'User with same number already exist',
                  type: 'error'
                }
              },
              () => {
                this.toggleAlertModal(true);
              }
            );
          }
        }
        //  const { error, message } = Result;
        //  if (error === true) {
        //  if (message === 'Conflict') {
        // this.setState(
        //   {
        //     alertContent: {
        //       header: 'Error!',
        //       message: 'User with same number already exist',
        //       type: 'error'
        //     }
        //   },
        //   () => {
        //     this.toggleAlertModal(true);
        //   }
        // );
        //      }
        //    } else {
        // this.setState({ signupStarted: true, ModalOn: true });
        //    }
        this.setState({ loading: false });
      });
    } else {
      this.setState({ isFormValid: false });
      Alert.alert('Error!', 'Data is invalid');
      InfoLogger('Sign Up Data', signupData);
    }
  };

  signIn = async () => {
    const { signInUserWithPhone, ExpoSate } = this.props;
    const { ExpoToken } = ExpoSate;
    const { password, internationalFormatcell } = this.state;
    this.setState({ loading: true });
    await signInUserWithPhone({
      strategy: 'phone',
      phone: internationalFormatcell,
      password,
      expoPushToken: ExpoToken
    }).then(() => {
      this.setState({ loading: false });
    });
  };

  validatePassword = () => {
    const { confirmPassword, password } = this.state;
    if (confirmPassword !== password || !password || !confirmPassword) {
      this.setState({
        passwordError: 'Passwords do not match',
        passwordValide: false
      });
      return false;
    } else if (password === confirmPassword) {
      this.setState({ passwordError: null, passwordValide: true });
      return true;
    }
  };

  validateName = () => {
    const { name } = this.state;
    const IsValid = NameValidator(name);
    if (IsValid) {
      this.setState({ nameError: null, nameValide: true });
      return true;
    } else {
      this.setState({ nameError: 'Name is not Valid', nameValide: false });
    }
    return false;
  };

  validatePhone = () => {
    const { phone, phoneError, internationalFormatcell } = this.state;
    if (phone !== '' && internationalFormatcell !== '' && phoneError === null) {
      this.setState({ phoneValide: true });
      return true;
    } else {
      this.setState({ phoneValide: false });
    }
    return false;
  };

  toggleSingupStarted = signupStarted => {
    this.setState({ signupStarted });
  };

  render() {
    const {
      password,
      confirmPassword,
      internationalFormatcell,
      name,
      phone,
      gender,
      dob,
      ModalOn,
      AlertModalOn,
      alertContent,
      nameError,
      phoneError,
      passwordError,
      phoneValide,
      nameValide,
      passwordValide,
      loading
    } = this.state;
    const { otpVerifyPhone, resendSignUp } = this.props;
    return (
      <ScrollView>
        <AlertModal
          alertContent={alertContent}
          toggaleFunc={this.toggleAlertModal}
          onSwitch={AlertModalOn}
        />
        {ModalOn && (
          <VerifyPhoneModal
            whilecheckout={false}
            phone={internationalFormatcell}
            skipper={this.signIn}
            otpVerification={otpVerifyPhone}
            resendSignUp={() => {
              resendSignUp({ phone: internationalFormatcell });
            }}
            ModalOn={ModalOn}
            CloseModal={() => {
              this.setState({
                ModalOn: false
              });
            }}
          />
        )}
        <View style={Style.Forms}>
          <TextField
            onSubmitEditing={() => {
              this.secondTextInput.focus();
            }}
            prefix={'+92'}
            value={phone}
            keyboardType="number-pad"
            labelPadding={1}
            returnKeyType={'next'}
            autoCapitalize="none"
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PHONE"
            maxLength={11}
            tintColor={'#111'}
            onChangeText={phone => {
              const { valid, internationalFormatcell } = phoneNumberValidator(
                phone
              );
              this.setState({
                phone,
                internationalFormatcell: valid ? internationalFormatcell : '',
                phoneError: valid ? null : 'Phone Number is invalid'
              });
            }}
          />
          {phoneError !== null && (
            <Text style={{ color: '#F33951' }}>{phoneError}</Text>
          )}
          <TextField
            onSubmitEditing={() => {
              this.TextInput3.focus();
            }}
            ref={input => {
              this.secondTextInput = input;
            }}
            value={password}
            returnKeyType={'next'}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PASSWORD"
            maxLength={40}
            tintColor={'#111'}
            onChangeText={password =>
              this.setState({
                password
              })
            }
            secureTextEntry={true}
          />
          <TextField
            onSubmitEditing={() => {
              this.TextInput4.focus();
            }}
            ref={input => {
              this.TextInput3 = input;
            }}
            returnKeyType={'next'}
            value={confirmPassword}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="CONFIRM PASSWORD"
            maxLength={40}
            tintColor={'#111'}
            onChangeText={confirmPassword =>
              this.setState({
                confirmPassword
              })
            }
            secureTextEntry={true}
          />
          {passwordError !== null && (
            <Text style={{ color: '#F33951' }}>{passwordError}</Text>
          )}
          <TextField
            ref={input => {
              this.TextInput4 = input;
            }}
            value={name}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCapitalize="words"
            label="NAME"
            maxLength={40}
            tintColor={'#111'}
            onChangeText={name =>
              this.setState({
                name
              })
            }
          />
          {nameError !== null && (
            <Text style={{ color: '#F33951' }}>{nameError}</Text>
          )}
          <View
            style={{
              height: heightPercentageToDP(5),
              width: widthPercentageToDP(90),
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: heightPercentageToDP(1)
            }}
          >
            <Text style={{ fontSize: widthPercentageToDP(4), color: 'grey' }}>
              {'GENDER '}
            </Text>
            <RadioArea
              options={[
                { value: 'male', text: 'Male' },
                { value: 'female', text: 'Female' }
              ]}
              onChange={gender => {
                this.setState({ gender });
              }}
              selected={gender}
            />
          </View>
          <ClickyButton
            onPress={this.onPressSignup}
            text={'SIGNUP'}
            //  loading={this.props.AuthLoading}
            loading={loading}
          />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  // AuthLoading: state.APPSTATE.AuthLoading,
  ExpoSate: state.ExpoTokenState
});
export default connect(
  mapStateToProps,
  { signUpUserWithPhone, signInUserWithPhone, otpVerifyPhone, resendSignUp }
)(SIGNUP);
