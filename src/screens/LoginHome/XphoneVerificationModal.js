'use strict';
import React from 'react';
import { View, Text, StatusBar, Modal, TouchableOpacity } from 'react-native';
import styles from './styles1';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import { TextField } from 'react-native-material-textfield';
import { ClickyButton } from '../../component/Buttons';
import ParentHeaders from '../../component/Header';
import AlertModal from '../../component/AlertModal';

import { WarningLogger, InfoLogger } from '../../modules/utilts';
const makeMeWait = 30;
export default class Recovery extends React.Component {
  //phone={internationalFormatcell}
  //skipper={this.signIn}
  //otpVerification={otpVerification}
  //ModalOn={ModalOn}
  //CloseModal={() => {
  //   this.setState({
  //    ModalOn: false
  //  });
  //}}
  state = {
    Etext: '',
    resent_Enter: false,
    Loading: false,
    forgetPassword: false,
    waiter: makeMeWait,
    AlertModalOn: false,
    alertContent: {
      header: '',
      message: '',
      type: 'info'
    }
  };
  Ticker = null;

  componentDidMount() {
    this.makeClock();
  }

  componentWillUnmount() {
    this.destroyClock();
  }

  resend = () => {
    this.setState({ resent_Enter: false });
    this.makeClock();
    this.props.resendSignUp();
  };
  verify = async () => {
    //otp, phone
    const { otpVerification, phone, skipper } = this.props;
    const { Etext: otp } = this.state;
    if (otp !== '')
      await otpVerification({ phone, otp }).then(Result => {
        InfoLogger('OtpReturn', Result);
        if (Result !== null) {
          const { status, message } = Result;
          if (status === 'success') {
            skipper();
            this.closeModal();
          } else {
            this.setState(
              {
                alertContent: {
                  header: 'Error!',
                  message,
                  type: 'error'
                }
              },
              () => {
                this.toggleAlertModal(true);
              }
            );
          }
        } else {
          this.setState(
            {
              alertContent: {
                header: 'Alert!',
                message: 'Wrong verification code',
                type: 'error'
              }
            },
            () => {
              this.toggleAlertModal(true);
            }
          );
        }
      });
  };
  makeClock = () => {
    this.Ticker = setInterval(() => {
      const { waiter } = this.state;
      if (waiter > 0) {
        this.setState({
          waiter: waiter - 1
        });
        console.log('Hi', waiter);
      } else {
        this.destroyClock();
        this.setState({
          resent_Enter: true,
          waiter: makeMeWait
        });
      }
    }, 1000);
  };
  destroyClock = () => {
    if (this.Ticker) {
      clearInterval(this.Ticker);
      this.Ticker = null;
    }
  };
  closeModal = () => {
    this.destroyClock();
    this.props.CloseModal();
  };
  toggleAlertModal = AlertModalOn => {
    this.setState({ AlertModalOn });
  };
  render() {
    const { ModalOn } = this.props;
    const {
      Etext,
      resent_Enter,
      waiter,
      AlertModalOn,
      alertContent
    } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ModalOn}
        onRequestClose={this.closeModal}
      >
        <AlertModal
          alertContent={alertContent}
          toggaleFunc={this.toggleAlertModal}
          onSwitch={AlertModalOn}
        />
        <View
          style={{
            backgroundColor: '#FFF',
            alignItems: 'center',
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(100)
          }}
        >
          <StatusBar
            animated={true}
            backgroundColor={'#FFF'}
            barStyle={'light-content'}
          />
          <ParentHeaders
            backgroundColor="#014b68"
            //leftClickHandler={this.closeModal}
            //LeftIcon="arrow-back"
            title="Phone Verification"
          />
          <View style={styles.otherThenStatusBarView}>
            <View style={styles.FormView}>
              {resent_Enter ? (
                <View style={styles.EmptyView} />
              ) : (
                <View style={styles.TimerView}>
                  <Text>{waiter.toString()}</Text>
                </View>
              )}

              <Text style={styles.FormHeader1}>
                {'Got your verification Code?'}
              </Text>
              <Text style={styles.FormHeader2}>
                {'Please enter the coding sent to your phone'}
              </Text>
              <View style={{ height: '40%', width: '100%', padding: '5%' }}>
                <TextField
                  value={Etext}
                  labelPadding={1}
                  keyboardType="number-pad"
                  labelHeight={heightPercentageToDP(1)}
                  autoCorrect={false}
                  label="Verification Code"
                  maxLength={8}
                  tintColor={'#111'}
                  onChangeText={Etext =>
                    this.setState({
                      Etext
                    })
                  }
                />
                <ClickyButton
                  style={{ marginTop: '5%' }}
                  //onPress={resent_Enter ? this.resend : this.verify}
                  onPress={this.verify}
                  //text={resent_Enter ? 'Resend' : 'Verify'}
                  text={'Verify'}
                />
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.skipper();
              this.closeModal();
            }}
            style={{
              width: widthPercentageToDP(25),
              height: heightPercentageToDP(5),
              borderRadius: widthPercentageToDP(1),
              bottom: heightPercentageToDP(3),
              right: widthPercentageToDP(3),
              backgroundColor: '#000',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              zIndex: 3
            }}
          >
            <Text style={{ color: '#fff', fontSize: widthPercentageToDP(4) }}>
              SKIP
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
