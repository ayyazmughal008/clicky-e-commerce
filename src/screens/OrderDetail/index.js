import React from 'react';
import { View, ScrollView, FlatList, StatusBar, Text } from 'react-native';
import { connect } from 'react-redux';
import { APIFuncs } from '../../modules/APIs';
import call from 'react-native-phone-call';
import PropTypes from 'prop-types';
import { ExtractDateAndTime } from '../../modules/utilts';
import ParentHeaders from '../../component/Header';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import OrderDetailDesign from '../../component/OrderDetailDesign';
import Data from './data';
class OrderDetail extends React.Component {
  static propTypes = {
    user: PropTypes.object
  };
  state = { orderDetail: null, renderAble: false };
  componentWillMount() {
    const orderID = this.props.navigation.getParam('orderID', '123123');
    const { token } = this.props.user;
    APIFuncs.getMyOrderDetail(orderID, token).then(orderDetail => {
      this.setState({ orderDetail });
      if (orderDetail !== null)
        setTimeout(() => {
          this.setState({ renderAble: true });
        }, 500);
      console.log('OrderDetail:', orderDetail);
      console.log('OrderID:', orderID);
    });
  }

  _keyExtractor = (item, index) => 'MyKey' + index;

  callUs = () => {
    const args = {
      number: '03401111340',
      prompt: false
    };
    call(args).catch(console.error);
  };

  render() {
    const { navigation, user } = this.props;
    const { orderDetail, renderAble } = this.state;
    console.log('OrderDetail', orderDetail);
    const { lastName, firstName } = user;
    let StartAt = null,
      UpdateAt = null,
      PaymentMode = '',
      PaymentStatus = '',
      OrderNumber = '',
      orderStatus = '';
    orderDetail !== null &&
      ((StartAt = ExtractDateAndTime(orderDetail.createdAt)),
      (UpdateAt = ExtractDateAndTime(orderDetail.updatedAt)),
      (OrderNumber = orderDetail.orderNo),
      (PaymentMode = orderDetail.payment.method),
      (PaymentStatus = orderDetail.payment.state),
      (orderStatus = orderDetail.status));
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f5f5' }}>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title={'Order ' + OrderNumber}
        />
        {renderAble && (
          <ScrollView
          //contentContainerStyle={{ flexGrow: 1 }}
          >
            <View
              style={{
                padding: widthPercentageToDP(2.5),
                marginTop: heightPercentageToDP(3.5)
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between'
                }}
              >
                <Text
                  style={{
                    fontSize: widthPercentageToDP(3.5),
                    fontWeight: '400',
                    color: '#000'
                  }}
                >
                  Order Placed on {StartAt.dDate}
                </Text>
                <Text
                  style={{
                    fontSize: widthPercentageToDP(3.5),
                    fontWeight: '500',
                    color: '#000',
                    marginRight: widthPercentageToDP(22.5)
                  }}
                >
                  TOTAL
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between',
                  marginTop: heightPercentageToDP(0.5)
                }}
              >
                <Text
                  style={{
                    fontSize: widthPercentageToDP(3.5),
                    fontWeight: '400',
                    color: '#000'
                  }}
                >
                  {'               '}
                </Text>
                <Text
                  style={{
                    fontSize: widthPercentageToDP(3.5),
                    fontWeight: '300',
                    color: '#000'
                  }}
                >
                  PKR {orderDetail.amount.total} for {orderDetail.amount.qty}
                  {' items'}
                </Text>
              </View>
            </View>
            <FlatList
              contentContainerStyle={{
                justifyContent: 'center',
                alignItems: 'center'
              }}
              showsVerticalScrollIndicator={false}
              keyExtractor={this._keyExtractor}
              data={orderDetail.items}
              renderItem={({ item }) => (
                <OrderDetailDesign
                  size={item.size}
                  status={orderStatus}
                  title={item.name}
                  qty={item.qty}
                  startDate={StartAt.dDate}
                  udate={UpdateAt.dDate}
                  image={item.img.medium}
                  discountPrice={item.price}
                  orginalPrice={item.mrp}
                  bgStatusColor={orderStatus === 'Canceled' ? 'red' : 'green'}
                  statusIcon={orderStatus === 'Canceled' ? 'close' : 'check'}
                />
              )}
            />
            <View
              style={{
                padding: widthPercentageToDP(2.5),
                marginTop: heightPercentageToDP(2)
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '500',
                  color: '#3a3a3a'
                }}
              >
                SHIPPING DETAILS
              </Text>
            </View>
            <View
              style={{
                padding: widthPercentageToDP(2.5),
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  //padding: widthPercentageToDP(2.5),
                  backgroundColor: '#fff',
                  height: heightPercentageToDP(27),
                  width: widthPercentageToDP(90)
                }}
              >
                <View
                  style={{
                    padding: widthPercentageToDP(4),
                    width: widthPercentageToDP(50)
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(5),
                      fontWeight: '300',
                      color: '#000',
                      marginTop: heightPercentageToDP(0.3),
                      marginBottom: heightPercentageToDP(0.3)
                    }}
                    numberOfLines={2}
                  >
                    {firstName + ' ' + lastName}
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(5),
                      fontWeight: '300',
                      color: '#000',
                      marginTop: heightPercentageToDP(0.3),
                      marginBottom: heightPercentageToDP(0.3)
                    }}
                    numberOfLines={1}
                  >
                    {orderDetail.address.phone}
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(5),
                      fontWeight: '300',
                      color: '#000',
                      marginTop: heightPercentageToDP(0.3),
                      marginBottom: heightPercentageToDP(0.3)
                    }}
                    numberOfLines={5}
                  >
                    {orderDetail.address.address.split('|')}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                padding: widthPercentageToDP(2.5),
                marginTop: heightPercentageToDP(2)
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '500',
                  color: '#3a3a3a'
                }}
              >
                PAYMENT SUMMARY
              </Text>
            </View>

            <View
              style={{
                padding: widthPercentageToDP(2.5),
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  //padding: widthPercentageToDP(2.5),
                  backgroundColor: '#fff',
                  height: heightPercentageToDP(65),
                  width: widthPercentageToDP(90)
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(8)
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    Total item price
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    PKR {orderDetail.amount.subtotal}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7),
                    backgroundColor: '#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    Discount
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    PKR {orderDetail.amount.discount}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7)
                    //backgroundColor:'#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    Promo code
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    PKR 0
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7),
                    backgroundColor: '#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    Shipping
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '300',
                      color: '#666666'
                    }}
                  >
                    PKR {orderDetail.shipping.charge}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7)
                    //backgroundColor:'#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    Total
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    PKR {orderDetail.amount.total}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7),
                    backgroundColor: '#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#74ad5b'
                    }}
                  >
                    Total refund
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#74ad5b'
                    }}
                  >
                    PKR 0
                  </Text>
                </View>

                <View
                  style={{
                    padding: widthPercentageToDP(2.5)
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(5),
                      fontWeight: '500',
                      color: '#3a3a3a'
                    }}
                  >
                    PAYMENT MODE
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7)
                    //backgroundColor:'#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    Payment Type
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    {PaymentMode}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: widthPercentageToDP(3),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%',
                    height: heightPercentageToDP(7)
                    //backgroundColor:'#fbfbfb'
                  }}
                >
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    Payment status
                  </Text>
                  <Text
                    style={{
                      fontSize: widthPercentageToDP(3.6),
                      fontWeight: '500',
                      color: '#666666'
                    }}
                  >
                    {PaymentStatus}
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.USER
});

export default connect(
  mapStateToProps,
  {}
)(OrderDetail);
