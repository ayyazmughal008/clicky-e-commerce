export default [
  {
    image: "https://rebuild-catalog.rushordertees.com/modelImages/g270_00_fr.jpg",
    status: "12 Jun",
    title: 'Robo print Sleveless Vest - Navy by Do Re Me',
    size: '23',
    qty: '1',
    price: '300'
  },
  {
    image: "https://image.jimcdn.com/app/cms/image/transf/dimension=1820x1280:format=jpg/path/sf50acdf4cb6f2e3f/image/i76a7cb4083b56e84/version/1528496342/image.jpg",
    status: "22 Feb",
    title: 'Gonchi Perfume Brand',
    size: 'Free',
    qty: '1',
    price: '2000'
  },
  {
    image: "https://images.windsorstore.com/images/products/1_508219_ZM_CLEAR.JPG",
    status: "12 Jun",
    title: 'In The Clear PVC Bra',
    size: '32',
    qty: '1',
    price: '1500'
  },

];
