import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StatusBar,
  ScrollView,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import Gift from '../../../assets/animation/gift.json';
import Sorry from '../../../assets/animation/lf20_cLJq7R.json';
import { ClickyButton } from '../../component/Buttons';
import SuccessOrdersList from '../../component/SuccessOrdersList';
import VerifyPhoneModal from '../../component/verifyPhoneModal';
import Lottie from 'lottie-react-native';
import { resendSignUp, otpVerifyPhone } from '../../redux/action';
class PaymentSuccess extends Component {
  state = { ModalOn: true };
  skipForNow = () => {
    this.setState({ ModalOn: false });
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this.animation.play();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.navigate('HomeScreen');
  };
  _keyExtractor = (item, index) => 'MyKey' + index;

  render() {
    const {
      navigation,
      resendSignUp,
      otpVerifyPhone,
      phone,
      verified
    } = this.props;
    const { ModalOn } = this.state;
    const orderNumber = navigation.getParam('orderNumber', '-1');
    const isSuccess = navigation.getParam('isSuccess', false);
    const message = navigation.getParam('message', false);
    const OrderDetail = navigation.getParam('OrderDetail', null);
    const card = navigation.getParam('card', false);
    let amount = null,
      ShippingCharges = 0;
    if (OrderDetail !== null) {
      amount = OrderDetail.amount;
      ShippingCharges = OrderDetail.shipping.charge;
    }
    console.log('Order ID received in success page', orderNumber);

    return (
      <View style={{ flex: 1 }}>
        {verified !== true && card !== true && phone !== undefined && (
          <VerifyPhoneModal
            whilecheckout={true}
            phone={phone}
            skipper={this.skipForNow}
            CloseModal={this.skipForNow}
            otpVerification={otpVerifyPhone}
            resendSignUp={() => {
              resendSignUp({ phone });
            }}
            ModalOn={ModalOn}
          />
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
          style={{
            marginBottom: heightPercentageToDP(8)
          }}
        >
          <View style={{ height: StatusBar.height }} />
          <View
            style={{
              justifyContent: 'flex-start',
              padding: widthPercentageToDP(4.3),
              marginTop: heightPercentageToDP(5)
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(7),
                fontWeight: '500',
                color: '#000'
              }}
            >
              {isSuccess ? 'All done' : 'Sorry'}
            </Text>
          </View>
          <View
            style={{
              backgroundColor: '#fff',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Lottie
              ref={animation => {
                this.animation = animation;
              }}
              style={{
                width: widthPercentageToDP(90),
                height: widthPercentageToDP(50),
                backgroundColor: '#eee'
              }}
              source={isSuccess ? Gift : Sorry}
            />
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
              padding: widthPercentageToDP(4.3),
              marginTop: heightPercentageToDP(2)
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(7),
                fontWeight: '300',
                color: '#696969'
              }}
            >
              {'Order ' + orderNumber}
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(5),
                fontWeight: '300',
                color: '#cccccc',
                marginTop: heightPercentageToDP(0.5)
              }}
            >
              {isSuccess ? 'Your order has been confirmed' : message}
            </Text>
          </View>
          <FlatList
            contentContainerStyle={{ flexGrow: 1 }}
            showsVerticalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            data={OrderDetail.items}
            renderItem={({ item }) => (
              <SuccessOrdersList
                status={item.status}
                image={item.img.medium}
                title={item.name}
                size={item.size}
                qty={item.qty}
                price={item.price}
              />
            )}
          />
          <View
            style={{
              justifyContent: 'flex-start',
              padding: widthPercentageToDP(4.3),
              marginTop: heightPercentageToDP(2)
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(7),
                fontWeight: '300',
                color: '#696969'
              }}
            >
              Price Summary
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(4.5),
                fontWeight: '300',
                color: '#cccccc',
                marginTop: heightPercentageToDP(0.5)
              }}
            >
              include GST and all government taxes.
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
              padding: widthPercentageToDP(4.3)
              //marginTop: heightPercentageToDP(2)
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '300',
                  color: '#808080'
                }}
              >
                Total item price
              </Text>
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '300',
                  color: '#808080'
                }}
              >
                {'PKR ' + amount.subtotal}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '300',
                  color: '#808080'
                }}
              >
                Shipping fee
              </Text>
              <Text
                style={{
                  fontSize: widthPercentageToDP(5),
                  fontWeight: '300',
                  color: '#808080'
                }}
              >
                {ShippingCharges !== 0 ? 'PKR ' + ShippingCharges : 'FREE'}
              </Text>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            position: 'absolute',
            bottom: 2,
            left: 0,
            right: 0,
            alignItems: 'center'
          }}
        >
          {/* <Button
            title={"CONTINUE SHOPPING"}
            titleStyle={{
              color: "#fff",
              textAlign: "center",
              fontSize: widthPercentageToDP(5)
            }}
            onPress={() => navigation.navigate("HomeScreen")}
            buttonStyle={{
              width: widthPercentageToDP(95),
              height: heightPercentageToDP(7),
              backgroundColor: "#000",
              marginBottom: 5
            }}
          /> */}
          <ClickyButton
            style={{ marginBottom: 5 }}
            onPress={() => {
              this.onBackPress;
            }}
            text={'CONTINUE SHOPPING'}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { USER } = state;
  const { phone, verified } = USER;
  return {
    phone,
    verified
  };
};

export default connect(
  mapStateToProps,
  { resendSignUp, otpVerifyPhone }
)(PaymentSuccess);
