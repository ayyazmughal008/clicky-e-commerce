import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import ParentHeaders from '../../component/Header';
import Stepper from '../../component/Stepper';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  sendOrderWithTheme,
  firstAddAddressThenPostOrder,
  getPayMethods
} from '../../redux/action';
import { widthPercentageToDP } from '../../modules/MakeMeResponsive';
class NewDesignPaymentMethods extends Component {
  static propTypes = {
    user: PropTypes.object,
    sendOrderWithTheme: PropTypes.func,
    ADDorREMOVECART: PropTypes.func,
    getPayMethods: PropTypes.func
  };
  state = {
    PayMethods: []
  };

  componentDidMount() {
    this.props.getPayMethods();
  }
  checkAddressAvailability_SendOrder = (navigation, TYPE) => {
    const { user, sendOrderWithTheme } = this.props;
    const {
      userDetailAvailable,
      address,
      token,
      currentAddressIndex,
      firstName,
      lastName
    } = user;
    userDetailAvailable &&
      sendOrderWithTheme(
        {
          address: {
            ...address[currentAddressIndex],
            firstName,
            lastName
          },
          paymentMethod: TYPE,
          platform: 'app'
        },
        token,
        navigation,
        TYPE
      );
  };

  render() {
    const { user, navigation, PayMethods } = this.props;
    const { cart } = user;
    let length = 0,
      Quantity = 0,
      TotalPrice = 0;
    if (cart) {
      console.log('CART', cart);
      // length = cart.items.length;
      // Quantity = cart.qty;
      // TotalPrice = cart.subtotal;
    }
    return (
      <View style={{ flex: 1 }}>
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Payment method"
        />
        <View style={{ height: '5%' }} />
        <Stepper
          stepOneBgColor={'#000'}
          stepOnetextColor={'#fff'}
          stepTwoBgColor={'#000'}
          stepTwotextColor={'#fff'}
          stepThreeBgColor={'#000'}
          stepThreetextColor={'#fff'}
          stepFourBgColor={'#fff'}
          stepFourtextColor={'#000'}
        />
        <View style={{ height: '5%' }} />
        <View style={{ height: '5%', marginLeft: 10, alignItems: 'center' }}>
          <Text style={{ fontSize: 20, fontWeight: '300', padding: 5 }}>
            Choose payment method
          </Text>
        </View>
        <View style={{ height: '5%' }} />
        {PayMethods.map((items, index) => {
          const { name, _id } = items;
          return (
            <TouchableOpacity
              key={_id}
              onPress={() => {
                navigation.navigate('DeliveryMethodConfirmationScreen', {
                  Method: name,
                  MethodFunc:
                    name === 'COD' || name === 'DEBIT/CREDIT CARD'
                      ? this.checkAddressAvailability_SendOrder
                      : () => {},
                  TotalPrice
                });
              }}
              style={{
                marginHorizontal: 10,
                borderBottomWidth: 1,
                borderBottomColor: '#cccccc',
                height: '7%',
                justifyContent: 'center'
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  fontWeight: 'bold'
                }}
              >
                {'Pay with ' + name}
              </Text>
            </TouchableOpacity>
          );
        })}

        <View style={{ height: '2%' }} />
        <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginLeft: 10,
              marginRight: 10,
              marginTop: 5,
              marginBottom: 10,
              justifyContent: 'space-between',
              alignItems: 'flex-start'
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                fontWeight: 'bold'
              }}
            >
              Subtotal
            </Text>
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                fontWeight: 'bold'
              }}
            >
              {'PKR ' + TotalPrice}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.USER,
    PayMethods: state.MEGAMENU.PayMethods
  };
};
export default connect(
  mapStateToProps,
  { sendOrderWithTheme, firstAddAddressThenPostOrder, getPayMethods }
)(NewDesignPaymentMethods);
