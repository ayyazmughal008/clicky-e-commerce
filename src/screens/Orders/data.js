export default [
  {
    status: "Arrives",
    date: "June 7",
    qty: "3",
    products: [
      {
        imgs:
          "http://cdn.shopify.com/s/files/1/0123/5065/2473/products/buck_mason-bruiser_bomber_jacket-thumb_1024x1024.jpg?v=1541786057"
      },
      {
        imgs:
          "https://www.colehaan.com/dw/image/v2/AALO_PRD/on/demandware.static/-/Sites-itemmaster_colehaan/default/dw41427f5e/images/large/T10157_A.jpg?sw=2000"
      },
      {
        imgs:
          "https://kathmandu.imgix.net/catalog/product/1/4/14560_yatradownjacket_a_913.jpg"
      }
    ]
  },
  {
    status: "Refunded",
    date: "May 1",
    qty: "1",
    products: [
      {
        imgs:
          "https://kathmandu.imgix.net/catalog/product/1/4/14560_yatradownjacket_a_913.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Jan 15",
    qty: "2",
    products: [
      {
        imgs:
          "https://img2.exportersindia.com/product_images/bc-full/dir_87/2603996/penty-bra-set-1967787.jpeg"
      },
      {
        imgs:
          "http://gangavathiexports.com/media/catalog/product/cache/1/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/s/e/xsexy-bra-panty-lav-color-a.jpg.pagespeed.ic.GUKBYoDncN.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Mar 23",
    qty: "4",
    products: [
      {
        imgs:
          "https://images-na.ssl-images-amazon.com/images/I/81w%2BaMozKVL._UL1500_.jpg"
      },
      {
        imgs:
          "https://image.dhgate.com/0x0/f2/albu/g7/M00/10/1F/rBVaSVtRwoaAKe9mAAVXibDewPA744.jpg"
      },
      {
        imgs:
          "https://ae01.alicdn.com/kf/HTB1HplAOFXXXXXCapXXq6xXFXXXj/Newborn-Bebe-Jeans-Baby-Pants-Boys-Trousers-Girls-Leggings-Winter-Warm-Infant-Toddler-Pants-Kids-Denim.jpg"
      },
      {
        imgs:
          "https://www.bumpalumpa.com/wp-content/uploads/2017/05/Levis-baby-boys-Jeans-.jpg"
      }
    ]
  },
  {
    status: "Arrives",
    date: "June 7",
    qty: "5",
    products: [
      {
        imgs:
          "http://assets.myntassets.com/assets/images/9132085/2019/4/15/35488c00-f2a8-4b64-81f0-4b123737d6381555320992384-Gini-and-Jony-Girls-Jeans-6511555320991800-1.jpg"
      },
      {
        imgs:
          "https://www.dhresource.com/0x0s/f2-albu-g5-M01-B8-38-rBVaJFjAwTWAa9QdAAKBLOIpzAM891.jpg/ripped-jeans-for-women-girls-skinny-jeans.jpg"
      },
      {
        imgs: "https://www.forever21.com/images/2_side_750/00333363-04.jpg"
      },
      {
        imgs:
          "https://www.easygiftproducts.co.uk/22259/lee-cooper-jeans-girls-cuffed-light-blue-ac2926.jpg"
      },
      {
        imgs:
          "https://images.boardriders.com/global/roxy-products/all/default/hi-res/ergdp03043_americanride,f_slew_frt1.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Aug 14",
    qty: "3",
    products: [
      {
        imgs:
          "http://assets.myntassets.com/assets/images/1382567/2016/6/15/11465994028754-Marie-Claire-Women-Shirts-6161465994028342-1.jpg"
      },
      {
        imgs:
          "http://feeds2.yourstorewizards.com/2084/images/full/rock-roll-cowgirl-girls-shirts-long-sleeve-g4t8450.jpg"
      },
      {
        imgs:
          "https://images-na.ssl-images-amazon.com/images/I/61llms00d2L._UL1024_.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Feb 23",
    qty: "1",
    products: [
      {
        imgs: "https://ebhfashion.com/wp-content/uploads/0250-3-808.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Nov 12",
    qty: "2",
    products: [
      {
        imgs:
          "http://www.unze.com.pk/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/l/2/l29188_1.jpg"
      },
      {
        imgs:
          "https://charlesclinkard_co_uk.secure-cdn.visualsoft.co.uk/images/products/1513260701-26589600.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Jul 13",
    qty: "1",
    products: [
      {
        imgs:
          "http://assets.myntassets.com/assets/images/2382216/2018/2/5/11517826731735-Tommy-Hilfiger-Women-Perfume-and-Body-Mist-3041517826731761-1.jpg"
      }
    ]
  },
  {
    status: "Delivered",
    date: "Jul 20",
    qty: "3",
    products: [
      {
        imgs:
          "https://www.brandability.co.za/wp-content/uploads/2017/06/6-Panel-Corner-Insert-Cap-Navy.jpg"
      },
      {
        imgs:
          "https://surlybikes.com/uploads/parts/surly-wool-cycling-cap-black-CL0075-1000x1000.jpg"
      },
      {
        imgs:
          "https://images-na.ssl-images-amazon.com/images/I/61AyliAJ1qL._UX679_.jpg"
      }
    ]
  }
];
