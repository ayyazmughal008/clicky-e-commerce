import React, { Component } from "react";
import {
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity
} from "react-native";
import Lottie from "lottie-react-native";
const EmptyList = require("../../../assets/animation/emptyList.json");
import { connect } from "react-redux";
import { ExtractDateAndTime } from "../../modules/utilts";
import { getMyOrders } from "../../redux/action";
import Style from "./styles";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
import ParentHeaders from "../../component/Header";
import OrderListDesign from "../../component/OrderListDesign";

class OrdersListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userOrders: null,
      pageNo: 1,
      isFetching: false,
      newOrders: [],
      totalPages: 0
    };
  }
  componentWillMount() {
    this.props.getMyOrders(this.props.user.token);
  }
  componentDidMount() {
    const { userOrders } = this.props.user;
    const { FetchingAndGettingLoading } = this.props.AppState;
    if (userOrders.length === 0 && !FetchingAndGettingLoading) {
      this.animation.play();
    }
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { userOrders } = this.props.user;
    const { FetchingAndGettingLoading } = this.props.AppState;
    if (userOrders.length === 0 && !FetchingAndGettingLoading) {
      this.animation.play();
    }
  }
  _onPress = ID =>
    this.props.navigation.navigate("OrderDetail", {
      orderID: ID
    });
  _keyExtractor = (item, index) => "OrderList" + index;

  render() {
    const { navigation } = this.props;
    const { userOrders } = this.props.user;
    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Orders"
        />
        {userOrders.length !== 0 ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            data={userOrders}
            renderItem={({ item }) => (
              <OrderListDesign
                status={item.status}
                qty={item.amount.qty}
                date={ExtractDateAndTime(item.updatedAt)}
                products={item.items}
                onPress={() => this._onPress(item._id)}
              />
            )}
          />
        ) : (
          <View style={{}}>
            <Lottie
              ref={animation => {
                this.animation = animation;
              }}
              autoPlay={true}
              loop={true}
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(60)
              }}
              source={EmptyList}
            />
            <TouchableOpacity
              style={{
                width: widthPercentageToDP(80),
                justifyContent: "center",
                alignItems: "center",
                height: heightPercentageToDP(6),
                elevation:3
              }}
              onPress={() => {
                navigation.goBack();
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  color: "#FFF"
                }}
              >
                {"Go Back"}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.USER,
  AppState: state.APPSTATE
});

export default connect(
  mapStateToProps,
  { getMyOrders }
)(OrdersListScreen);
