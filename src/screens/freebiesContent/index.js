'use strict';
import React from 'react';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import Lottie from 'lottie-react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { APIFuncs } from '../../modules/APIs';
import { ClickyButton } from '../../component/Buttons';
import FreeBeisContent from './freebiesComponents';
import ParentHeaders from '../../component/Header';
import { BusyFetchingData, BusyMakingList } from '../../redux/action';
import { connect } from 'react-redux';
import _ from 'lodash';
const EmptyList = require('../../../assets/animation/emptyList.json');
class FeebiesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Loading: true,
      freebiesData: []
    };
  }
  UNSAFE_componentWillMount() {
    const { BusyFetchingData, USER } = this.props;
    BusyFetchingData(true);
    APIFuncs.getFreebiesFunc(USER.token).then(REZ => {
      const { data } = REZ;
      const freebiesData = data.filter(item => item.productData.length > 0);
      this.setState({
        freebiesData,
        Loading: false
      });
      BusyFetchingData(false);
    });
  }
  updateList = (INDEX, _id) => {
    const { freebiesData } = this.state;
    freebiesData[INDEX].has_applied = true;
    freebiesData[INDEX].applicants.push({ user: _id });
    freebiesData[INDEX].totalApplicants =
      freebiesData[INDEX].totalApplicants + 1;
    this.setState({ freebiesData });
  };
  render() {
    const { navigation } = this.props;
    const { freebiesData, Loading } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <View
          style={{
            width: widthPercentageToDP(100)
          }}
        >
          <ParentHeaders
            backgroundColor="#014b68"
            leftClickHandler={() => navigation.goBack()}
            LeftIcon="arrow-back"
            title="FREEBIES"
          />
        </View>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(90)
          }}
        >
          {freebiesData.length ? (
            <FreeBeisContent
              style={{
                width: widthPercentageToDP(100)
              }}
              data={freebiesData}
              navigation={navigation}
              extraData={this.state}
              updateFunc={this.updateList}
            />
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              {!this.props.AppState.FetchingAndGettingLoading &&
                !this.props.AppState.ListLoader && (
                  <View
                    style={{
                      width: widthPercentageToDP(100),
                      height: heightPercentageToDP(80),
                      backgroundColor: '#fff',
                      alignItems: 'center'
                    }}
                  >
                    <Lottie
                      ref={animation => {
                        this.animation = animation;
                      }}
                      autoPlay={true}
                      loop={false}
                      style={{
                        width: widthPercentageToDP(100),
                        height: heightPercentageToDP(60)
                      }}
                      source={EmptyList}
                    />
                    <ClickyButton
                      onPress={() => {
                        navigation.goBack();
                      }}
                      text={'Go Back To Home'}
                    />
                  </View>
                )}
            </View>
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
    //paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20
  },
  header: {
    backgroundColor: '#FFF',
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  headerText: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    padding: 10
  },
  content: {
    padding: 20,
    backgroundColor: '#fff'
  },
  filterContent: {
    flex: 1,
    padding: 10,
    backgroundColor: '#f1f1f1'
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)'
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)'
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10
  },
  activeSelector: {
    fontWeight: 'bold'
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center'
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8
  },
  touchableStyle: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#000'
  }
});

const mapStateToProps = state => {
  const { USER, APPSTATE } = state;
  return {
    AppState: APPSTATE,
    USER
  };
};
export default connect(
  mapStateToProps,
  { BusyFetchingData, BusyMakingList }
)(FeebiesPage);
