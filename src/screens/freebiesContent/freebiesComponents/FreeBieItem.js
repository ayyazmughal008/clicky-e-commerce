import React, { useState } from 'react';
import { View, Image, TouchableOpacity, Text, StyleSheet } from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../../modules/MakeMeResponsive';
const Placeholder = require('../../../../assets/images/placeholder.png');
import { Ionicons } from '@expo/vector-icons';

export default ({
  sourceImage,
  title,
  realAmount,
  totalApplicants,
  totalAvailable,
  timeLeft,
  has_applied,
  ProductID,
  clickHandler
}) => {
  const [ImageReady, setImageReady] = useState(false);
  return (
    <View
      style={{
        width: widthPercentageToDP(100),
        height: heightPercentageToDP(58),
        backgroundColor: '#FFFFFF',
        alignItems: 'center'
      }}
    >
      <Image
        onLoad={() => {
          setImageReady(true);
        }}
        source={sourceImage}
        style={{ width: 1, height: 1, position: 'absolute' }}
        resizeMode="contain"
      />
      <TouchableOpacity
        style={{
          marginVertical: heightPercentageToDP(2),
          width: widthPercentageToDP(85),
          height: heightPercentageToDP(25)
        }}
        onPress={clickHandler}
      >
        <Image
          style={{
            width: widthPercentageToDP(85),
            height: heightPercentageToDP(25)
          }}
          resizeMode="contain"
          source={ImageReady ? sourceImage : Placeholder}
        />
      </TouchableOpacity>

      <View
        style={{
          width: widthPercentageToDP(90),
          height: heightPercentageToDP(8)
        }}
      >
        <Text
          style={{
            fontSize: widthPercentageToDP(3),
            color: 'black',
            textAlign: 'center',
            fontWeight: '400',
            flex: 1,
            overflow: 'hidden'
          }}
        >
          {title}
        </Text>
      </View>
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(8),
          borderBottomWidth: StyleSheet.hairlineWidth,
          borderTopWidth: StyleSheet.hairlineWidth,
          flexDirection: 'row',
          paddingHorizontal: widthPercentageToDP(5),
          justifyContent: 'space-between'
        }}
      >
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '33%',
            height: '100%'
          }}
        >
          <Text
            style={{
              color: 'grey',
              fontSize: widthPercentageToDP(4)
              //fontFamily: 'Metropolis_Regular'
            }}
          >
            Original price
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: widthPercentageToDP(4)
              //fontFamily: 'Metropolis_Regular'
            }}
          >
            {'PKR ' + realAmount}
          </Text>
        </View>
        <View
          style={{
            width: '33%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderLeftWidth: StyleSheet.hairlineWidth,
            borderRightWidth: StyleSheet.hairlineWidth
          }}
        >
          <Text
            style={{
              color: 'grey',
              fontSize: widthPercentageToDP(4)
              //fontFamily: 'Metropolis_Regular'
            }}
          >
            Available
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: widthPercentageToDP(4)
              // fontFamily: 'Metropolis_Regular'
            }}
          >
            {'' + totalAvailable}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '33%',
            height: '100%'
          }}
        >
          <Text
            style={{
              color: 'grey',
              fontSize: widthPercentageToDP(4)
              //  fontFamily: 'Metropolis_Regular'
            }}
          >
            Applicants
          </Text>
          <Text
            style={{
              color: '#65BA69',
              fontSize: widthPercentageToDP(4)
              //fontFamily: 'Metropolis_Regular'
            }}
          >
            {'' + totalApplicants}
          </Text>
        </View>
      </View>
      <View
        style={{
          width: widthPercentageToDP(90),
          height: heightPercentageToDP(6),
          alignItems: 'center',
          flexDirection: 'row'
          //justifyContent: 'center'
        }}
      >
        <Ionicons
          size={widthPercentageToDP(6)}
          name={'ios-alarm'}
          color={'#F4511E'}
        />
        <Text
          style={{
            marginLeft: widthPercentageToDP(1.5),
            fontSize: widthPercentageToDP(4)
            //fontFamily: 'Metropolis_Regular'
          }}
        >
          Ends in :
        </Text>
        <Text
          style={{
            marginLeft: widthPercentageToDP(1.5),
            fontSize: widthPercentageToDP(4),
            //fontFamily: 'Metropolis_Regular',
            color: '#F4511E'
          }}
        >
          {timeLeft}
        </Text>
      </View>
      <View
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(7),
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <TouchableOpacity
          //disabled={has_applied}
          onPress={clickHandler}
          style={{
            width: widthPercentageToDP(50),
            height: heightPercentageToDP(6),
            backgroundColor:
              has_applied || timeLeft === 'Expire' ? '#F44336' : '#65BA69',
            borderRadius: widthPercentageToDP(1.5),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              //fontFamily: 'Metropolis_Regular',
              color: '#fff'
            }}
          >
            {has_applied
              ? 'Already Applied'
              : timeLeft === 'Expire'
              ? 'Expired'
              : 'Apply for Free'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
