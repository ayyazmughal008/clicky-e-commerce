import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import ListThumb from './FreeBieItem';
import { timeLefter } from '../../../modules/utilts';
const PlaceHolder = require('../../../../assets/images/placeholder.png');
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../../modules/MakeMeResponsive';
import { connect } from 'react-redux';
class WholeListSCreen extends Component {
  _keyExtractor = (i, index) => 'unique' + index;
  render() {
    const { navigation, data, updateFunc } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          ItemSeparatorComponent={() => (
            <View
              style={{
                height: heightPercentageToDP(3),
                backgroundColor: '#EEE',
                width: widthPercentageToDP(100)
              }}
            />
          )}
          keyExtractor={this._keyExtractor}
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={({ item,index }) => {
            const {
              totalApplicants,
              totalAvailable,
              ValidTo,
              has_applied
            } = item;
            const endsIn = timeLefter(ValidTo);
            const productDataAtZero = item.productData[0];
            const { imgUrls, name, brandName } = productDataAtZero;
            const { mrp, price } = productDataAtZero.variants[0];
            const { length } = imgUrls;
            return (
              <ListThumb
                has_applied={has_applied}
                timeLeft={endsIn}
                totalApplicants={totalApplicants}
                totalAvailable={totalAvailable}
                navigation={navigation}
                sourceImage={length !== 0 ? { uri: imgUrls[0] } : PlaceHolder}
                title={name}
                heading={brandName}
                realAmount={mrp.toFixed(0)}
                discountAmount={price.toFixed(0)}
                clickHandler={() => {
                  navigation.navigate('FreebiesDetail', {
                    itemData: item,
                    endsIn,
                    updateFunc,
                    itemIndex:index
                  });
                }}
              />
            );
          }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  LOADER: state.APPSTATE.ListLoader
});
export default connect(
  mapStateToProps,
  {}
)(WholeListSCreen);
