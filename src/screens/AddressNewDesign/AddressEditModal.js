import React, { Component } from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addAddress, SelectAddressIndex } from '../../redux/action';
import {
  widthPercentageToDP,
  getStatusBarHeight,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
class AddressEditModal extends Component {
  static propTypes = {
    user: PropTypes.object,
    navigation: PropTypes.object
  };
  constructor(props) {
    const {
      userDetailAvailable,
      address,
      currentAddressIndex,
      firstName,
      lastName
    } = props.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];

    super(props);
    this.state = {
      name: firstName + ' ' + lastName,
      primaryPhone: userDetailAvailable ? CurrentInfo.phone + '' : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',

      city: userDetailAvailable ? CurrentInfo.city + '' : '',
      ChooseModalOn: false
    };
  }
  componentWillReceiveProps(nextProps) {
    const { userDetailAvailable, currentAddressIndex, phone } = nextProps.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];
    this.setState({
      primaryPhone:
        phone !== undefined
          ? '+' + phone
          : userDetailAvailable
          ? CurrentInfo.phone + ''
          : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',

      city: userDetailAvailable ? CurrentInfo.city + '' : ''
    });
  }
  editAddress = () => {
    //_id, primaryPhone, address, city, firstName, lastName
    const { user, addAddress, CloseModal } = this.props;
    const { token, phone } = user;
    const { primaryPhone, address, city } = this.state;
    addAddress(
      {
        city,
        state,
        country: 'Pakistan',
        phone: primaryPhone,
        address
        //zipcode
      },
      token
    ).then(Res => {
      console.log('AddressResult', Res);
      if (Res) CloseModal();
    });
  };
  render() {
    const { ModalOn, CloseModal, user, SelectAddressIndex } = this.props;
    const { phone } = user;
    const { name, primaryPhone, address, city } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ModalOn}
        onRequestClose={CloseModal}
      >
        <View
          style={{
            backgroundColor: '#FFF',
            alignItems: 'center',
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(100),
            paddingTop: getStatusBarHeight(true)
          }}
        >
          <ChooseModal
            SelectAddressIndex={SelectAddressIndex}
            user={user}
            ModalOn={ChooseModalOn}
            CloseModal={() => this.toggleThisModal(false)}
          />
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={'padding'}>
            <ScrollView
              style={{
                flex: 1,
                width: '100%'
              }}
              contentContainerStyle={{
                flexGrow: 1,
                width: widthPercentageToDP(100),
                paddingHorizontal: '2%'
              }}
              showsVerticalScrollIndicator={false}
            >
              <View style={{ height: '5%' }} />
              <View
                style={{
                  marginHorizontal: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: '#cccccc',
                  height: '7%'
                }}
              >
                <Text>{name}</Text>
              </View>
              <View
                style={{
                  marginHorizontal: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: '#cccccc',
                  marginTop: 5,
                  height: '7%'
                }}
              >
                <TextInput
                  placeholderTextColor="#cccccc"
                  value={primaryPhone + ''}
                  textContentType="telephoneNumber"
                  placeholder="Phone"
                  editable={phone !== undefined ? false : true}
                  onChangeText={primaryPhone => this.setState({ primaryPhone })}
                />
              </View>
              <View
                style={{
                  marginHorizontal: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: '#cccccc',
                  marginTop: 5,
                  height: '7%'
                }}
              >
                <TextInput
                  placeholderTextColor="#cccccc"
                  value={address + ''}
                  textContentType="streetAddressLine1"
                  placeholder="Address"
                  onChangeText={address => this.setState({ address })}
                />
              </View>

              <View
                style={{
                  marginHorizontal: 10,
                  borderBottomWidth: StyleSheet.hairlineWidth,
                  borderBottomColor: '#cccccc',
                  marginTop: 5,
                  height: '7%'
                }}
              >
                <TextInput
                  placeholderTextColor="#cccccc"
                  value={city}
                  textContentType="addressCity"
                  placeholder="City"
                  onChangeText={city => this.setState({ city })}
                />
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
          <View style={{ height: '2%' }} />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              right: 0,
              bottom: 0,
              height: heightPercentageToDP(20),
              width: widthPercentageToDP(100)
            }}
          >
            <TouchableOpacity
              onPress={() => this.toggleThisModal(true)}
              style={{
                height: heightPercentageToDP(8),
                width: widthPercentageToDP(80),
                borderRadius: widthPercentageToDP(10),
                backgroundColor: '#e3e627',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000'
                }}
              >
                Choose
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.addNewAddress}
              style={{
                height: heightPercentageToDP(8),
                width: widthPercentageToDP(80),
                borderRadius: widthPercentageToDP(10),
                backgroundColor: '#e3e627',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000'
                }}
              >
                UPDATE
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  { addAddress, SelectAddressIndex }
)(AddressEditModal);
