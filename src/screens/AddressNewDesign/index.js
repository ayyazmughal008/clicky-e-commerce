import React from 'react';
import {
  View,
  StatusBar,
  Text,
  Alert,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { Header } from 'react-native-elements';
import { AntDesign as Icon } from '@expo/vector-icons';
import Stepper from '../../component/Stepper';
import StaticField, { AddressArea } from './staticFields';
import { connect } from 'react-redux';
import { InfoLogger, numberFormator } from '../../modules/utilts';
import ChangeModal from './ChangeAddressModal';
import PropTypes from 'prop-types';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
class AddressScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object
  };

  constructor(props) {
    super(props);
    const {
      userDetailAvailable,
      address,
      currentAddressIndex,
      firstName,
      lastName,
      phone
      //email
    } = props.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];
    InfoLogger('Address', address);
    this.state = {
      // email,
      name: firstName + ' ' + lastName,
      primaryPhone:
        phone !== undefined
          ? phone
          : userDetailAvailable
          ? numberFormator(CurrentInfo.phone)
          : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',

      city: userDetailAvailable ? CurrentInfo.city + '' : '',
      ModalOn: false
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      userDetailAvailable,
      address,
      currentAddressIndex
    } = nextProps.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];
    this.setState({
      primaryPhone: userDetailAvailable
        ? numberFormator(CurrentInfo.phone)
        : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',

      city: userDetailAvailable ? CurrentInfo.city + '' : ''
    });
  }

  ToggleModal = TOGGLE =>
    this.setState({
      ModalOn: TOGGLE
    });

  goForNextStep = () => {
    const { user, navigation } = this.props;
    const { userDetailAvailable } = user;
    const { primaryPhone, address, city } = this.state;
    if (primaryPhone && address && city && userDetailAvailable) {
      navigation.navigate('NewDesignPaymentMethods');
    } else {
      if (!userDetailAvailable) {
        this.ToggleModal(true);
      } else Alert.alert('Address not Complete');
    }
  };

  render() {
    const { navigation, user, whileCheckout } = this.props;
    const {
      //email,address2,address1,state,
      name,
      primaryPhone,
      address,
      city,
      ModalOn
    } = this.state;
    const { userDetailAvailable } = user;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <Header
          leftComponent={{
            icon: 'arrow-back',
            color: '#000',
            onPress: () => navigation.goBack()
          }}
          centerComponent={{
            text: 'Address Details',
            style: {
              color: '#000',
              fontWeight: '500',
              fontSize: widthPercentageToDP(4.5)
            }
          }}
          containerStyle={{
            backgroundColor: '#fff',
            justifyContent: 'space-around',
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderColor: '#aaa'
          }}
        />
        <ChangeModal
          That={this}
          ModalOn={ModalOn}
          navigation={navigation}
          CloseModal={() => {
            this.ToggleModal(false);
          }}
        />
        <View style={{ height: '5%' }} />
        {whileCheckout !== undefined && (
          <Stepper
            stepOneBgColor={'#000'}
            stepOnetextColor={'#fff'}
            stepTwoBgColor={'#000'}
            stepTwotextColor={'#fff'}
            stepThreeBgColor={'#fff'}
            stepThreetextColor={'#000'}
            stepFourBgColor={'#fff'}
            stepFourtextColor={'#000'}
          />
        )}
        <View style={{ height: '5%' }} />
        <View
          style={{
            height: '5%',
            marginHorizontal: widthPercentageToDP(5),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <Text
            style={{
              fontSize: widthPercentageToDP(6),
              fontWeight: '500'
            }}
          >
            Personal details
          </Text>
          {whileCheckout !== undefined ? (
            userDetailAvailable === true && (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: '100%'
                }}
                onPress={() => this.ToggleModal(true)}
              >
                <Icon
                  name="edit"
                  type="antdesign"
                  color="#000000"
                  size={widthPercentageToDP(6)}
                />
                <Text
                  style={{
                    fontSize: widthPercentageToDP(3),
                    fontWeight: '300',
                    color: '#000000',
                    marginLeft: widthPercentageToDP(1)
                  }}
                >
                  Change
                </Text>
              </TouchableOpacity>
            )
          ) : (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: '100%'
              }}
              onPress={() => this.ToggleModal(true)}
            >
              <Icon
                name="edit"
                type="antdesign"
                color="#000000"
                size={widthPercentageToDP(6)}
              />
              <Text
                style={{
                  fontSize: widthPercentageToDP(3),
                  fontWeight: '300',
                  color: '#000000',
                  marginLeft: widthPercentageToDP(1)
                }}
              >
                {userDetailAvailable === true ? 'Change' : 'Add'}
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            flex: 1,
            marginTop: heightPercentageToDP(5),
            marginBottom: heightPercentageToDP(2),
            width: widthPercentageToDP(90),
            marginHorizontal: widthPercentageToDP(5),
            paddingTop: heightPercentageToDP(2)
          }}
        >
          <StaticField feildName={'Name'} value={name} />
          <StaticField feildName={'Phone'} value={primaryPhone} />
          <AddressArea feildName={'Address'} value={address} />
          <StaticField feildName={'City'} value={city} />
        </View>

        <View
          style={{
            position: 'absolute',
            width: widthPercentageToDP(100),
            bottom: heightPercentageToDP(2),
            height: heightPercentageToDP(11),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <TouchableOpacity
            onPress={() => {
              if (whileCheckout) this.goForNextStep();
              else {
                navigation.goBack();
              }
            }}
            style={{
              height: heightPercentageToDP(8),
              width: widthPercentageToDP(80),
              borderRadius: widthPercentageToDP(20),
              backgroundColor: '#e3e627',
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 3
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: widthPercentageToDP(3.5),
                fontWeight: '500',
                color: '#000000'
              }}
            >
              {whileCheckout === undefined
                ? 'Back to profile'
                : 'Proceed to payment'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  {}
)(AddressScreen);
