import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { FontAwesome as ICON } from '@expo/vector-icons';

export default class MultiAddress extends React.Component {
  render() {
    const {
      selector,
      userPhoneNo,
      userAddress,
      userCity,
      selected,
      firstName,
      lastName,
      _id,
      editFunc,
      deleteFunc
    } = this.props;
    const username =
      (firstName !== undefined ? firstName : '') +
      ' ' +
      (lastName !== undefined ? lastName : '');
    return (
      <View
        style={{
          width: widthPercentageToDP(90),
          backgroundColor: selected === true ? '#000000' : '#ffffff',
          borderBottomWidth: StyleSheet.hairlineWidth,
          borderBottomColor: '#cccccc',
          borderRadius: widthPercentageToDP(2),
          padding: widthPercentageToDP(2),
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}
      >
        <TouchableOpacity
          onPress={selector}
          style={{
            width: widthPercentageToDP(90)
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <ICON
              name="map-marker"
              color={selected === true ? '#ffffff' : '#000000'}
              size={widthPercentageToDP(4)}
            />
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                marginLeft: widthPercentageToDP(5),
                fontWeight: '500',
                color: selected === true ? '#ffffff' : '#000000'
              }}
            >
              {username}
            </Text>
          </View>
          <Text
            style={{
              marginTop: heightPercentageToDP(1),
              fontSize: widthPercentageToDP(4),
              marginLeft: widthPercentageToDP(7),
              fontWeight: '300',
              color: selected === true ? '#ffffff' : '#000000'
            }}
          >
            {userPhoneNo !== undefined ? userPhoneNo : ''}
          </Text>
          <Text
            style={{
              marginTop: heightPercentageToDP(1),
              fontSize: widthPercentageToDP(3),
              marginLeft: widthPercentageToDP(7),
              fontWeight: '300',
              color: selected === true ? '#ffffff' : '#000000'
            }}
          >
            {userAddress !== undefined ? userAddress.replace('|', ' ') : ''}
          </Text>
          <Text
            style={{
              marginTop: heightPercentageToDP(1),
              fontSize: widthPercentageToDP(3),
              marginLeft: widthPercentageToDP(7),
              fontWeight: '300',
              color: selected === true ? '#ffffff' : '#000000'
            }}
          >
            {userCity !== undefined ? userCity : ''}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
