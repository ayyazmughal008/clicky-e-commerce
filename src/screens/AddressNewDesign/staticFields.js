import React from 'react';
import { View, Text } from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';

export default ({ feildName, value }) => {
  return (
    <View
      style={{
        paddingVertical: heightPercentageToDP(1.2),
        justifyContent: 'center'
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap'
        }}
      >
        <View style={{ flexWrap: 'wrap' }}>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              fontWeight: '500',
              color: '#000000'
            }}
          >
            {feildName + ':'}
          </Text>
        </View>
        <View style={{ flexWrap: 'wrap' }}>
          <Text
            numberOfLines={2}
            style={{
              textAlign: 'center',
              fontSize: widthPercentageToDP(4),
              fontWeight: '300',
              color: '#000000',
              marginLeft: widthPercentageToDP(1.5)
            }}
          >
            {value}
          </Text>
        </View>
      </View>
    </View>
  );
};

const AddressArea = ({ feildName, value }) => {
  return (
    <View
      style={{
        paddingVertical: heightPercentageToDP(1.2),
        justifyContent: 'center'
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap'
        }}
      >
        <View style={{ flexWrap: 'wrap' }}>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              fontWeight: '500',
              color: '#000000'
            }}
          >
            {feildName + ':'}
          </Text>
        </View>
        <View style={{ flexWrap: 'wrap' }}>
          <Text
            numberOfLines={3}
            style={{
              textAlign: 'center',
              fontSize: widthPercentageToDP(4),
              fontWeight: '300',
              color: '#000000',
              marginLeft: widthPercentageToDP(1.5)
            }}
          >
            {value}
          </Text>
        </View>
      </View>
    </View>
  );
};

export { AddressArea };
