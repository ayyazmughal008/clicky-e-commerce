import React, { Component } from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Header } from 'react-native-elements';
import {
  widthPercentageToDP,
  getStatusBarHeight,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import style from './style';
class AddressEditModal extends Component {
  static propTypes = {
    user: PropTypes.object,
    navigation: PropTypes.object
  };
  constructor(props) {
    const { firstName, lastName, phone } = props.user;
    super(props);
    this.state = {
      // email,
      name: firstName + ' ' + lastName,
      primaryPhone: phone !== undefined ? '+' + phone : '',
      zipcode: '',
      address: '',
      city: ''
    };
  }

  addNewAddress = () => {
    const { addFunc, user } = this.props;
    const { firstName, lastName } = user;
    const { primaryPhone, address, city } = this.state;
    addFunc(primaryPhone, address, city, firstName, lastName);
  };

  render() {
    const { ModalOn, CloseModal, user } = this.props;
    const { phone } = user;
    const { name, primaryPhone, address, city } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={ModalOn}
        onRequestClose={CloseModal}
      >
        <View
          style={{
            backgroundColor: '#FFF',
            alignItems: 'center',
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(100),
            paddingTop: getStatusBarHeight(true)
          }}
        >
          <Header
            leftComponent={{
              icon: 'arrow-back',
              color: '#000',
              onPress: () => CloseModal()
            }}
            centerComponent={{
              text: 'Add New Address',
              style: { color: '#000', fontWeight: '500', fontSize: 17 }
            }}
            containerStyle={{
              backgroundColor: '#fff',
              justifyContent: 'space-around',
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#aaa'
            }}
          />
          <KeyboardAvoidingView style={{ flex: 1 }} behavior={'padding'}>
            <ScrollView
              style={style.ScrollView}
              contentContainerStyle={style.contentContainer}
              showsVerticalScrollIndicator={false}
              nestedScrollEnabled={true}
            >
              <View style={style.textInputView}>
                <View style={style.textInputStyle}>
                  <Text>{name}</Text>
                </View>
                <TextInput
                  placeholderTextColor="#cccccc"
                  value={primaryPhone + ''}
                  keyboardType={'phone-pad'}
                  style={style.textInputStyle}
                  maxLength={12}
                  editable={phone !== undefined ? false : true}
                  textContentType="telephoneNumber"
                  placeholder="Phone"
                  onChangeText={primaryPhone => this.setState({ primaryPhone })}
                />
                <TextInput
                  placeholderTextColor="#cccccc"
                  style={style.textInputStyle}
                  multiline={true}
                  value={address + ''}
                  numberOfLines={3}
                  textContentType="streetAddressLine1"
                  placeholder="Address"
                  onChangeText={address => this.setState({ address })}
                />
                <TextInput
                  placeholderTextColor="#cccccc"
                  style={style.textInputStyle}
                  value={city}
                  textContentType="addressCity"
                  placeholder="City"
                  onChangeText={city => this.setState({ city })}
                />
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
          <View style={{ height: '2%' }} />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              right: 0,
              bottom: 0,
              height: heightPercentageToDP(20),
              width: widthPercentageToDP(100)
            }}
          >
            <TouchableOpacity
              onPress={this.addNewAddress}
              style={{
                height: heightPercentageToDP(8),
                width: widthPercentageToDP(80),
                borderRadius: widthPercentageToDP(10),
                backgroundColor: '#e3e627',
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 3
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000'
                }}
              >
                ADD NEW
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  {}
)(AddressEditModal);
