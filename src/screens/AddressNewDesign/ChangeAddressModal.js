import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Modal,
  Alert
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import PropTypes from 'prop-types';
import { Header } from 'react-native-elements';
import { connect } from 'react-redux';
import MultiAddress from './MultiAddress';
import { addAddress, SelectAddressIndex } from '../../redux/action';
import { phoneNumberValidator, numberFormator } from '../../modules/utilts';
import AddressAddModal from './AddressAddModal';
import AddressEditModal from './AddressEditModal';
class ChangeAddressModal extends Component {
  static propTypes = {
    user: PropTypes.object,
    addAddress: PropTypes.func,
    navigation: PropTypes.object
  };
  constructor(props) {
    super(props);
    const {
      userDetailAvailable,
      address,
      currentAddressIndex,
      firstName,
      lastName,
      phone,
      email
    } = props.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];

    this.state = {
      email,
      name: firstName + ' ' + lastName,
      primaryPhone:
        phone !== undefined
          ? phone
          : userDetailAvailable
          ? numberFormator(CurrentInfo.phone)
          : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',

      city: userDetailAvailable ? CurrentInfo.city + '' : '',
      state: userDetailAvailable ? CurrentInfo.state + '' : '',
      currentAddressIndex,
      ChooseModalOn: false,
      AddModalOn: false,
      EditModalOn: false,
      currentEdit: null
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      userDetailAvailable,
      address,
      currentAddressIndex,
      phone
    } = nextProps.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];

    this.setState({
      primaryPhone:
        phone !== undefined
          ? phone
          : userDetailAvailable
          ? numberFormator(CurrentInfo.phone)
          : '',
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + '' : '',
      address:
        CurrentInfo.length !== 0
          ? CurrentInfo.hasOwnProperty('address')
            ? CurrentInfo.address
            : ''
          : '',
      city: userDetailAvailable ? CurrentInfo.city + '' : '',
      state: userDetailAvailable ? CurrentInfo.state + '' : '',
      currentAddressIndex
    });
  }

  addNewAddress = (primaryPhone, address, city, firstName, lastName) => {
    const { user, addAddress, CloseModal } = this.props;
    const { token } = user;
    const PhoneValid = phoneNumberValidator(primaryPhone);
    if (PhoneValid.valid === true) {
      if (city && address && firstName && lastName) {
        addAddress(
          {
            firstName,
            lastName,
            city,
            country: 'Pakistan',
            phone: PhoneValid.internationalFormatcell,
            address
          },
          token
        ).then(Res => {
          console.log('AddressResult', Res);
          if (Res) {
            this.toggleThisModal(false, 'ADD'), CloseModal();
          }
        });
      } else {
        Alert.alert('Please fill all fields !');
      }
    } else {
      Alert.alert('Phone number is not valid !');
    }
  };

  editAddress = (_id, primaryPhone, address, city, firstName, lastName) => {
    const { user, addAddress, CloseModal } = this.props;
    const { token } = user;
    const PhoneValid = phoneNumberValidator(primaryPhone);
    if (PhoneValid.valid === true) {
      if (city && state && address1 && firstName && lastName) {
        addAddress(
          {
            firstName,
            lastName,
            city,
            country: 'Pakistan',
            phone: PhoneValid.internationalFormatcell,
            address
          },
          token
        ).then(Res => {
          console.log('AddressResult', Res);
          if (Res) {
            this.toggleThisModal(false, 'ADD'), CloseModal();
          }
        });
      } else {
        Alert.alert('Please fill all fields !');
      }
    } else {
      Alert.alert('Phone number is not valid !');
    }
  };

  toggleThisModal = (TOGGLE, TYPE) => {
    TYPE === 'ADD'
      ? this.setState({
          AddModalOn: TOGGLE
        })
      : this.setState({
          EditModalOn: TOGGLE
        });
  };

  _keyExtractor = (item, index) => 'MyKey' + index;

  render() {
    const { ModalOn, CloseModal, user, SelectAddressIndex } = this.props;
    const { address, phone, currentAddressIndex } = user;
    const { AddModalOn, EditModalOn } = this.state;
    return (
      <Modal
        animationType="fade"
        transparent={false}
        visible={ModalOn}
        onRequestClose={CloseModal}
      >
        {AddModalOn === true && (
          <AddressAddModal
            addFunc={this.addNewAddress}
            ModalOn={AddModalOn}
            CloseModal={() => {
              this.toggleThisModal(false, 'ADD');
            }}
          />
        )}
        {EditModalOn === true && (
          <EditModalOn
            editFunc={this.editAddress}
            ModalOn={EditModalOn}
            CloseModal={() => {
              this.toggleThisModal(false, 'EDIT');
            }}
          />
        )}

        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(100),
            backgroundColor: '#FFFFFF',
            alignItems: 'center'
          }}
        >
          <Header
            leftComponent={{
              icon: 'arrow-back',
              color: '#000',
              onPress: () => CloseModal()
            }}
            centerComponent={{
              text: 'Select Shipping Address',
              style: { color: '#000', fontWeight: '500', fontSize: 17 }
            }}
            containerStyle={{
              backgroundColor: '#fff',
              justifyContent: 'space-around',
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#aaa'
            }}
          />

          <TouchableOpacity
            onPress={() => this.toggleThisModal(true, 'ADD')}
            style={{
              height: heightPercentageToDP(12),
              width: widthPercentageToDP(90),
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: '#000',
              borderStyle: 'dashed',
              borderWidth: widthPercentageToDP(0.5),
              borderRadius: 1,
              position: 'relative'
            }}
          >
            <Text
              style={{
                fontSize: widthPercentageToDP(4),
                color: '#000',
                fontWeight: '300'
              }}
            >
              {' '}
              + Add Address
            </Text>
          </TouchableOpacity>

          <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(0.3),
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderBottomColor: '#cccccc',
              marginTop: widthPercentageToDP(5)
            }}
          />

          <FlatList
            key={'MyAddress'}
            style={{
              marginTop: heightPercentageToDP(1),
              width: widthPercentageToDP(100),
              marginBottom: heightPercentageToDP(1)
            }}
            contentContainerStyle={{
              width: widthPercentageToDP(100),
              alignItems: 'center'
            }}
            // getItemLayout={(data, index) => ({
            //   length: heightPercentageToDP(8),
            //   offset: heightPercentageToDP(8) * index,
            //   index
            // })}
            initialScrollIndex={this.state.currentAddressIndex}
            keyExtractor={this._keyExtractor}
            showsVerticalScrollIndicator={false}
            data={address}
            extraData={this.state}
            renderItem={({ item, index }) => (
              <MultiAddress
                selector={() => SelectAddressIndex(index)}
                _id={item._id}
                selected={this.state.currentAddressIndex === index}
                firstName={item.firstName}
                lastName={item.lastName}
                userPhoneNo={phone !== undefined ? phone : item.phone}
                userAddress={item.address}
                userCity={item.city}
                editFunc={() => {}}
                deleteFunc={() => {}}
              />
            )}
          />
        </View>
      </Modal>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  { addAddress, SelectAddressIndex }
)(ChangeAddressModal);
