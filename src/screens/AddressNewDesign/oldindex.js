import React from "react";
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  StyleSheet
} from "react-native";
import ParentHeaders from "../../component/Header";
import Stepper from "../../component/Stepper";
import { connect } from "react-redux";
//import { AntDesign as ICON } from "@expo/vector-icons";
import EditModal from "./ChangeAddressModal";
import { addAddress } from "../../redux/action"; //Remove
import PropTypes from "prop-types";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
class AddressScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    addAddress: PropTypes.func //Remove
  };
  // constructor(props) {
  //   super(props);
  //   const { userDetailAvailable, address } = this.props.user;
  //   const AddressArray = userDetailAvailable
  //     ? address[0].address.split("|")
  //     : [];
  //   console.log("Address", address);
  //   this.state = {
  //     email: this.props.user.email,
  //     name: this.props.user.firstName + " " + this.props.user.lastName,
  //     primaryPhone: userDetailAvailable ? address[0].phone + "" : "",
  //     secondaryPhone: "",
  //     pincode: "",
  //     address1: AddressArray.length !== 0 ? AddressArray[0] : "",
  //     address2:
  //       AddressArray.length !== 0
  //         ? AddressArray.length === 2
  //           ? AddressArray[1]
  //           : ""
  //         : "",
  //     city: userDetailAvailable ? address[0].city + "" : "",
  //     state: userDetailAvailable ? address[0].state + "" : ""
  //   };
  // }
  constructor(props) {
    super(props);
    const {
      userDetailAvailable,
      address,
      currentAddressIndex,
      firstName,
      lastName,
      email
    } = props.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];
    const AddressArray =
      CurrentInfo.length !== 0
        ? CurrentInfo.hasOwnProperty("address")
          ? CurrentInfo.address.split("|")
          : []
        : [];

    //console.log("Address", address);
    this.state = {
      email,
      name: firstName + " " + lastName,
      primaryPhone: userDetailAvailable ? CurrentInfo.phone + "" : "",
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + "" : "",
      address1: AddressArray.length !== 0 ? AddressArray[0] : "",
      address2:
        AddressArray.length !== 0
          ? AddressArray.length === 2
            ? AddressArray[1]
            : ""
          : "",
      city: userDetailAvailable ? CurrentInfo.city + "" : "",
      state: userDetailAvailable ? CurrentInfo.state + "" : "",
      ModalOn: false
    };
  }
  componentWillReceiveProps(nextProps) {
    const {
      userDetailAvailable,
      address,
      currentAddressIndex
    } = nextProps.user;
    const CurrentInfo = userDetailAvailable ? address[currentAddressIndex] : [];
    const AddressArray =
      CurrentInfo.length !== 0
        ? CurrentInfo.hasOwnProperty("address")
          ? CurrentInfo.address.split("|")
          : []
        : [];

    //console.log("Address", address);
    this.setState({
      primaryPhone: userDetailAvailable ? CurrentInfo.phone + "" : "",
      zipcode: userDetailAvailable ? CurrentInfo.zipcode + "" : "",
      address1: AddressArray.length !== 0 ? AddressArray[0] : "",
      address2:
        AddressArray.length !== 0
          ? AddressArray.length === 2
            ? AddressArray[1]
            : ""
          : "",
      city: userDetailAvailable ? CurrentInfo.city + "" : "",
      state: userDetailAvailable ? CurrentInfo.state + "" : ""
    });
  }
  ToggleModal = TOGGLE =>
    this.setState({
      ModalOn: TOGGLE
    });

  addNewAddress = () => {
    const { user, navigation, addAddress } = this.props;
    const { token } = user;
    const {
      primaryPhone,
      secondaryPhone,
      pincode,
      address1,
      address2,
      city,
      state
    } = this.state;

    addAddress(
      {
        city,
        state,
        country: "Pakistan",
        phone: primaryPhone,
        address: address1 + "|" + address2,
        zipcode: pincode
      },
      token
    ).then(Res => {
      const { userDetailAvailable, address } = this.props.user;
      const AddressArray = userDetailAvailable
        ? address[0].address.split("|")
        : [];
      Res &&
        this.setState({
          email: this.props.user.email,
          name: this.props.user.firstName + " " + this.props.user.lastName,
          primaryPhone: userDetailAvailable ? address[0].phone + "" : "",
          address1: AddressArray.length !== 0 ? AddressArray[0] : "",
          address2:
            AddressArray.length !== 0
              ? AddressArray.length === 2
                ? AddressArray[1]
                : ""
              : "",
          city: userDetailAvailable ? address[0].city + "" : "",
          state: userDetailAvailable ? address[0].state + "" : ""
        });
    });
  };
  goForNextStep = () => {
    const { user, navigation } = this.props;
    const { userDetailAvailable } = user;
    const { primaryPhone, address1, address2, city, state } = this.state;
    if (
      primaryPhone &&
      address1 &&
      address2 &&
      city &&
      state &&
      userDetailAvailable
    ) {
      navigation.navigate("NewDesignPaymentMethods");
    } else {
      Alert.alert("Address not Complete");
    }
  };
  render() {
    const { navigation, user } = this.props;
    const {
      email,
      name,
      primaryPhone,
      address2,
      address1,
      city,
      secondaryPhone,
      pincode,
      state,
      ModalOn
    } = this.state;
    const { token } = user;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />
        <EditModal
          That={this}
          ModalOn={ModalOn}
          navigation={navigation}
          CloseModal={() => {
            this.ToggleModal(false);
          }}
        />
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Add new address"
        />
        <View style={{ height: "5%" }} />
        <Stepper
          stepOneBgColor={"#000"}
          stepOnetextColor={"#fff"}
          stepTwoBgColor={"#000"}
          stepTwotextColor={"#fff"}
          stepThreeBgColor={"#fff"}
          stepThreetextColor={"#000"}
          stepFourBgColor={"#fff"}
          stepFourtextColor={"#000"}
        />
        <View style={{ height: "5%" }} />
        <View style={{ height: "5%", marginLeft: 10 }}>
          <Text style={{ fontSize: 20, fontWeight: "500", padding: 5 }}>
            Personal details
          </Text>
        </View>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={"padding"}>
          <ScrollView
            style={{
              flex: 1,
              width: "100%"
            }}
            contentContainerStyle={{
              flexGrow: 1,
              width: widthPercentageToDP(100),
              paddingHorizontal: "2%"
            }}
            showsVerticalScrollIndicator={false}
          >
            <View style={{ height: "5%" }} />
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                height: "12%"
              }}
            >
              <TextInput
                value={email + ""}
                textContentType="emailAddress"
                placeholder="Email"
                style={{ padding: 5 }}
                editable={false}
                selectTextOnFocus={false}
                onChangeText={email => this.setState({ email })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                style={{ padding: 5 }}
                placeholderTextColor="#cccccc"
                value={name + ""}
                textContentType="givenName"
                placeholder="Name"
                editable={false}
                selectTextOnFocus={false}
                onChangeText={name => this.setState({ name })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                placeholderTextColor="#cccccc"
                style={{ padding: 5 }}
                value={primaryPhone + ""}
                textContentType="telephoneNumber"
                placeholder="Phone"
                onChangeText={primaryPhone => this.setState({ primaryPhone })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                style={{ padding: 5 }}
                placeholderTextColor="#cccccc"
                value={address1 + ""}
                textContentType="streetAddressLine1"
                placeholder="Address Line 1"
                onChangeText={address1 => this.setState({ address1 })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                style={{ padding: 5 }}
                placeholderTextColor="#cccccc"
                value={address2 + ""}
                textContentType="streetAddressLine2"
                placeholder="Address Line 2"
                onChangeText={address2 => this.setState({ address2 })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                style={{ padding: 5 }}
                placeholderTextColor="#cccccc"
                value={state + ""}
                textContentType="addressState"
                placeholder="State"
                onChangeText={state => this.setState({ state })}
              />
            </View>
            <View
              style={{
                marginLeft: 10,
                marginRight: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#cccccc",
                marginTop: 5,
                height: "12%"
              }}
            >
              <TextInput
                style={{ padding: 5 }}
                placeholderTextColor="#cccccc"
                value={city + ""}
                textContentType="addressCity"
                placeholder="City"
                onChangeText={city => this.setState({ city })}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <View style={{ height: "2%" }} />
        <View
          style={{
            justifyContent: "space-around",
            alignItems: "center",
            // position: "absolute",
            // right: 0,
            // bottom: 0,
            height: heightPercentageToDP(20),
            width: widthPercentageToDP(100)
          }}
        >
          <TouchableOpacity
         //   onPress={this.addNewAddress}
         onPress={()=>{
           this.setState({
             ModalOn:true
           })
         }}
            style={{
              height: heightPercentageToDP(8),
              width: widthPercentageToDP(80),
              borderRadius: widthPercentageToDP(10),
              backgroundColor: "#e3e627",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 13,
                fontWeight: "500",
                color: "#000"
              }}
            >
              {"UPDATE ADDRESS"}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.goForNextStep()}
            style={{
              height: heightPercentageToDP(8),
              width: widthPercentageToDP(80),
              borderRadius: widthPercentageToDP(10),
              backgroundColor: "#e3e627",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 13,
                fontWeight: "500",
                color: "#000"
              }}
            >
              {"PROCEED TO PAYMENT"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  { addAddress }
)(AddressScreen);
