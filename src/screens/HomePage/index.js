'use strict';
import React from 'react';
import {
  View,
  StatusBar,
  Modal,
  TouchableOpacity,
  Text,
  TextInput,
  Platform,
  Alert
} from 'react-native';
import TABView from './TabViewWrapper';
import { MaterialIcons as ICON } from '@expo/vector-icons';
import { GetMegaMenuNew } from '../../redux/action';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Icon } from 'react-native-elements';
import Header from '../../component/Header/headerCart';
//import CustomHeader from "../../component/Header/CustomHeader";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
class HomeScreen extends React.Component {
  static propTypes = {
    GetMegaMenuNew: PropTypes.func,
    MegaMenu: PropTypes.array
  };
  state = {
    ModalOn: false,
    searchString: ''
  };
  _keyExtractor = (item, index) => 'MyKey' + index;
  componentWillMount() {
    const { GetMegaMenuNew, MegaMenu } = this.props;
    GetMegaMenuNew(MegaMenu.length !== 0 ? false : true);
  }
  CloseModal = () => {
    this.setState({
      ModalOn: false
    });
  };
  OpenModal = () => {
    this.setState({
      ModalOn: true
    });
  };
  SearchFunc = () => {
    const { searchString } = this.state;
    const { navigate } = this.props.navigation;
    if (searchString !== '') {
      navigate('CategoryPage', { searchString });
      this.CloseModal();
      setTimeout(
        () =>
          this.setState({
            searchString: ''
          }),
        700
      );
    } else {
      Alert.alert('Nothing To Search !');
    }
  };
  render() {
    const { props, state, CloseModal, OpenModal } = this;
    const { navigation, MegaMenu, Cart } = props;
    const { ModalOn, searchString } = state;
    // if (MegaMenu.length !== 0) {
    //   console.log("Whole MagaMenu: \n ", MegaMenu);
    // }
    return (
      <View style={{ flex: 1 }}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={ModalOn}
          onRequestClose={CloseModal}
        >
          <View
            style={{
              marginTop: Platform.OS === 'ios' ? heightPercentageToDP(2) : 0,
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(100)
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(7),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                borderBottomWidth: widthPercentageToDP(0.2)
              }}
            >
              <TouchableOpacity
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'absolute',
                  left: '2%'
                }}
                onPress={CloseModal}
              >
                <Icon name="close" />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize:
                    Platform.OS === 'ios'
                      ? widthPercentageToDP(4)
                      : widthPercentageToDP(6)
                }}
              >
                {'Search'}
              </Text>
            </View>
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(7),
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: '1%',
                flexDirection: 'row',
                borderBottomWidth: widthPercentageToDP(0.2)
              }}
            >
              <View
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <ICON size={widthPercentageToDP(6)} name="search" />
              </View>
              <View
                style={{
                  width: widthPercentageToDP(75),
                  height: heightPercentageToDP(6),
                  fontSize: widthPercentageToDP(5),
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderLeftWidth: widthPercentageToDP(0.2),
                  borderRightWidth: widthPercentageToDP(0.2),
                  borderColor: '#E1E1E1'
                }}
              >
                <TextInput
                  onChangeText={searchString => this.setState({ searchString })}
                  value={this.state.searchString}
                  style={{
                    width: widthPercentageToDP(72),
                    height: heightPercentageToDP(6),
                    fontSize:
                      Platform.OS === 'ios'
                        ? widthPercentageToDP(4)
                        : widthPercentageToDP(7)
                  }}
                  placeholder={'Search Product'}
                  onSubmitEditing={this.SearchFunc}
                  returnKeyType="search"
                />
              </View>
              <TouchableOpacity
                onPress={this.SearchFunc}
                style={{
                  width: heightPercentageToDP(6),
                  height: heightPercentageToDP(6),
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <ICON size={widthPercentageToDP(6)} name="chevron-right" />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        {/* <CustomHeader
          leftClickHandler={() => navigation.openDrawer()}
          searchClickHandler={OpenModal}
          CartClickHandler={() => navigation.navigate("Cart")}
          cart={Cart}
        /> */}
        <Header
          leftClickHandler={() => navigation.openDrawer()}
          leftIcon="menu"
          image={require('../../../assets/images/Login_logo.png')}
          centerText="CLICKY"
          right1ClickHandler={() => navigation.navigate('Cart')}
          right1Icon="shopping-cart"
          right2ClickHandler={() => OpenModal()}
          right2Icon="search"
          cart={Cart}
        />

        {MegaMenu.length !== 0 && (
          <TABView routes={MegaMenu} navigation={navigation} />
        )}
      </View>
    );
  }
}

export const PopularSearchMethod = props => {
  return (
    <TouchableOpacity
    //onPress={() => props.clickHandler(props.nav, props.param)}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          padding: 10,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text style={{ color: 'black', fontSize: 18, fontWeight: '300' }}>
            {props.item.text}
          </Text>
        </View>
        {/* <View style = {{backgroundColor:'black', height:2,flex:1, flexDirection: 'row'}}>
          </View> */}
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => ({
  MegaMenu: state.MEGAMENU.WholeMegaMenu,
  Cart: state.USER.cart
});
export default connect(
  mapStateToProps,
  {
    GetMegaMenuNew
  }
)(HomeScreen);
