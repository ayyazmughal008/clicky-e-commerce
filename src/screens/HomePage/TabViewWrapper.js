'use strict';
import React from 'react';
import { View, Text, Platform } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import SingleTabView from './TabViewChild';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
const initialLayout = {
  height: 0,
  width: widthPercentageToDP(100)
};

export default class HomeScreen extends React.PureComponent {
  state = {
    index: 1,
    routes: this.props.routes
  };

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled={true}
      style={{ backgroundColor: '#fff' }}
      tabStyle={{
        //paddingBottom: '8%',
        //paddingTop:0,
        width: widthPercentageToDP(20),
        height: heightPercentageToDP(7)
      }}
      onTabPress={({ route, preventDefault }) => {
        if (route.key === 'Freebies') {
          preventDefault();
          const { navigate } = this.props.navigation;
          navigate('Freebies', { Freebies: true });
        }
      }}
      indicatorStyle={{ backgroundColor: '#65BA69' }}
      activeBackgroundColor="#fff"
      inActiveBackgroundColor="#fff"
      inactiveColor="#cccccc"
      activeColor="#000"
      labelStyle={{ backgroundColor: '#fff' }}
      renderLabel={({ route, focused, color }) => {
        const { key } = route;
        return (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text
              ellipsizeMode={'tail'}
              numberOfLines={1}
              style={{
                textAlign: 'center',
                fontSize:
                  Platform.OS === 'ios'
                    ? widthPercentageToDP(3)
                    : widthPercentageToDP(4),
                fontFamily: 'Metropolis_Regular',
                fontWeight: '500',
                color: key === 'Freebies' ? '#65BA69' : color
              }}
            >
              {key}
            </Text>
          </View>
        );
      }}
    />
  );
  getSceneMap = () => {
    let sceneMap = {};
    const { routes } = this.state;
    const { navigation } = this.props;
    routes.map((item, index) => {
      const { content, CoverImage, key, _id, slug, popular } = item;
      const { navigate } = navigation;
      sceneMap[key] = () => {
        return (
          <SingleTabView
            key={_id}
            slug={slug}
            navigate={navigate}
            CoverImage={CoverImage}
            popular={popular}
            data={content}
          />
        );
      };
    });

    return sceneMap;
  };

  _renderScene = SceneMap(this.getSceneMap());

  render = () => {
    const { navigate } = this.props.navigation;
    return (
      <TabView
        swipeEnabled={true}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={index => {
          index !== 0 ? this.setState({ index }) : navigate('Freebies');
        }}
        initialLayout={initialLayout}
      />
    );
  };
}
