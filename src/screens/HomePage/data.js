export default [
    [
        {
            id: 1,
            text: 'Casual Shirts',
            image: 'https://www.dhresource.com/600x600/f2/albu/g10/M00/CC/A5/rBVaVlzVMNWAPj1eAAGQcUkWzEc564.jpg'
        },
        {
            id: 2,
            text: 'Blazer',
            image: 'http://cdn.shopify.com/s/files/1/1707/7541/products/6bd87c22-83cd-40f0-809f-d6635d42af2b_grande.jpg'
        },
        {
            id: 3,
            text: 'Casual Shoes',
            image: 'https://cdn.shopify.com/s/files/1/2343/4325/products/product-image-402753337.jpg'
        },
        {
            id: 4,
            text: 'Men Jeans',
            image: 'https://ak1.ostkcdn.com/images/products/is/images/direct/4a9a4698421feda6da27fa93262cdaaca848fa3a/Lee-Dungarees-Men%27s-Straight-Fit-Jean.jpg'
        },
    ],

    [
        {
            id: 5,
            text: 'Shoe',
            image: 'http://www.unze.com.pk/media/catalog/product/cache/1/image/600x600/9df78eab33525d08d6e5fb8d27136e95/g/s/gs6207_16.jpg'
        },
        {
            id: 6,
            text: 'Kurtis',
            image: 'https://www.leocarts.com/wp-content/uploads/2018/11/Mona-Leaves-Woolen-Kurtis-GK-030-600x600.jpg'
        },
        {
            id: 7,
            text: 'jeans',
            image: 'http://cdn.shopify.com/s/files/1/0423/8761/products/image_03c70496-9f36-4eb6-b649-d40938b185ba_grande.jpg'
        },
        {
            id: 8,
            text: 'InnerWear',
            image: 'https://www.innerwear.com.au/wp-content/uploads/2018/08/123959-fpb2yd-600x600.jpg'
        },
    ],

]