"use strict";
import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  FlatList,
  Platform,
  StyleSheet
} from "react-native";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import RoundImages from "../../component/RoundImages";
import ReactAngelDesign from "../../component/ReactAngelDesign";
import { ProgressiveImage } from "../../component/progressive";
const Placeholder = require("../../../assets/images/placeholderB.png");
export default class SingleTabView extends Component {
  clickHandler = slug =>
    this.props.navigate("CategoryPage", {
      slug
    });
  _keyExtractor1 = (item, index) => "MyKey1" + index;
  _keyExtractor2 = (item, index) => "MyKey2" + index;
  render() {
    const { CoverImage, slug, popular, data } = this.props;
    return (
      <ScrollView style={{ flex: 1 }}>
        <TouchableOpacity
          transparent
          onPress={() => this.clickHandler(slug)}
          style={styles.roundButtonsWrap}
        >
          {/* <Image
            style={styles.rectangle}
            resizeMode="stretch"
            source={
              CoverImage
                ? { uri: CoverImage }
                : require("../../../assets/images/main_banner.png")
            }
            PlaceholderContent={<ActivityIndicator />}
          /> */}
          <ProgressiveImage
            style={styles.rectangle}
            thumbnailSource={Placeholder}
            resizeMode="cover"
            source={CoverImage ? { uri: CoverImage } : Placeholder}
          />
        </TouchableOpacity>

        <View
          style={{
            alignItems: "center"
          }}
        >
          <Text
            style={{
              marginTop: heightPercentageToDP(3),
              color: "#313131",
              fontSize: widthPercentageToDP(5.5),
              fontWeight: "500"
            }}
          >
            Popular Category
          </Text>
          <FlatList
            style={{ marginTop: heightPercentageToDP(3), marginBottom: 5 }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            keyExtractor={this._keyExtractor2}
            data={popular}
            renderItem={({ item, index }) => (
              <RoundImages
                text={item.name}
                imageUri={{ uri: item.imgA }}
                clickHandler={() => this.clickHandler(item.slug)}
              />
            )}
          />
        </View>

        {data.map((itemA, indexA) => {
          const { name, children } = itemA;
          return (
            <View
              key={"Content" + indexA}
              style={{
                flex: 1,
                backgroundColor: "#f0f1f3",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  marginTop: heightPercentageToDP(3),
                  color: "#313131",
                  fontSize: widthPercentageToDP(5.5),
                  fontWeight: "500"
                }}
              >
                {name}
              </Text>
              <FlatList
                style={{ marginTop: heightPercentageToDP(3), marginBottom: 5 }}
                horizontal={false}
                numColumns={2}
                keyExtractor={this._keyExtractor1}
                data={children}
                renderItem={({ item, index }) => (
                  <ReactAngelDesign
                    text={item.name}
                    imageUri={{ uri: item.hc_img_url }}
                    clickHandler={() => this.clickHandler(item.slug)}
                  />
                )}
              />
            </View>
          );
        })}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  roundButtonsWrap: {
    height: heightPercentageToDP(25),
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 11,
    fontWeight: "500",
    marginTop: 5
  },
  round: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    width: 80,
    height: 80,
    backgroundColor: "#fff",
    borderRadius: 60
  },
  rectangle: {
    width: widthPercentageToDP(100),
    height: heightPercentageToDP(25),
    marginRight: 2
  }
});
