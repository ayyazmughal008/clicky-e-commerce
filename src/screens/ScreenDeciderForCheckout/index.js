"use strict";
import React from "react";
import ACCOUNT from "../Account";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AddressScreen from "../../screens/AddressNewDesign";
class ScreenDeciderForCheckout extends React.Component {
  static propTypes = {
    user: PropTypes.object
  };
  Params = this.props.navigation.state.params;
  render() {
    const { user, navigation } = this.props;
    const { isLoggedIn } = user;

    return isLoggedIn ? (
      <AddressScreen whileCheckout={true} navigation={navigation} />
    ) : (
      <ACCOUNT navigation={navigation} />
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(mapStateToProps)(ScreenDeciderForCheckout);
