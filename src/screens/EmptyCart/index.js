import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import EmptyCartGif from '../../../assets/animation/empty-cart.json';
import Lottie from 'lottie-react-native';
import { ClickyButton } from '../../component/Buttons';
export default class EmptyCart extends Component {
  componentDidMount() {
    this.animation.play();
  }

  render() {
    const { navigation } = this.props;
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            marginTop: heightPercentageToDP(15),
            alignItems: 'center'
          }}
        >
          <Lottie
            ref={animation => {
              this.animation = animation;
            }}
            loop={false}
            style={{
              width: widthPercentageToDP(90),
              height: widthPercentageToDP(60),
              backgroundColor: '#fff'
            }}
            source={EmptyCartGif}
          />
        </View>
        <View
          style={{
            alignItems: 'center',
            marginTop: heightPercentageToDP(7)
          }}
        >
          <Text
            style={{
              fontSize: widthPercentageToDP(7),
              color: '#000',
              fontWeight: '500',
              marginTop: 5
            }}
          >
            Whoops!
          </Text>
          <Text
            style={{
              fontSize: widthPercentageToDP(4),
              color: '#000',
              fontWeight: '300',
              marginTop: 5
            }}
          >
            Your Cart is Empty
          </Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            marginTop: heightPercentageToDP(3)
          }}
        >
          <ClickyButton
            style={{ marginBottom: 5 }}
            onPress={() => navigation.navigate('HomeScreen')}
            text={'Continue Shopping'}
          />
        </View>
      </View>
    );
  }
}
