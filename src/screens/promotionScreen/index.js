'use strict';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
import { MaterialIcons as Icon } from '@expo/vector-icons';
import { makeWorkingHtml } from '../../modules/utilts';
import WEBVIEW from './webview';
class promotion extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {}

  render() {
    const { navigate, promotion } = this.props;
    const { content, slug } = promotion;
    return (
      <View style={{ flex: 1 }}>
        <WEBVIEW html={makeWorkingHtml(content)} />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  promotion: state.MEGAMENU.promotionData
});

export default connect(
  mapStateToProps,
  {}
)(promotion);
