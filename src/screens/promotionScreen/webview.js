import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
import { widthPercentageToDP } from '../../modules/MakeMeResponsive';
export default ({ html }) => {
  return (
    <View style={{ width: widthPercentageToDP(100) }}>
      <WebView
        useWebKit={true}
        javaScriptEnabled={true}
        style={{ width: widthPercentageToDP(100) }}
        source={{ html }}
        onLoad={() =>
          this.setState({
            renderAnimation: false
          })
        }
        startInLoadingState={true}
        renderLoading={() => (
          <View
            style={{
              width: widthPerDP(100),
              height: heightPerDP(92),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <ActivityIndicator color="#000" size={'large'} />
          </View>
        )}
      />
    </View>
  );
};
