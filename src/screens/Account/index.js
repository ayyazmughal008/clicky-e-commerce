'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProfileScreen from '../Profile';
import LoginHomeScreen from '../LoginHome';
class Account extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    navigation: PropTypes.object
  };
  render() {
    const { navigation, user } = this.props;
    const { isLoggedIn } = user;
    return isLoggedIn ? (
      <ProfileScreen navigation={navigation} />
    ) : (
      <LoginHomeScreen navigation={navigation} />
    );
  }
}

const mapStateToProps = state => ({
  user: state.USER
});
export default connect(
  mapStateToProps,
  {}
)(Account);
