import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { WebView } from 'react-native-webview';
import ParentHeaders from '../../component/Header';
import moment from 'moment';
import { Buffer } from 'buffer';
import base64 from 'base-64';
global.Buffer = Buffer; // very important
const EasyPaisaHost = 'https://app.clicky.pk/';
const headers = new Headers();
headers.append(
  'Authorization',
  'Basic ' + base64.encode('app@clicky.pk:400N827JSiOP6M47fDz63G5pA2Y5InRH')
);
headers.append('method', 'GET');

export default class Easypay extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true, url: '', hash: '', isFetch: true };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const orderId = navigation.getParam('orderId', '1234');
    const amount = navigation.getParam('amount', '1234');
    const phone = navigation.getParam('phone', '1234');
    const email = navigation.getParam('email', 'abc@gmail.com');
    const formatAmount = amount + '.0';
    this.fetchEncodedValues(formatAmount, orderId, email, phone);
    // this.fetchEncodedValues(
    //   '8274.0',
    //   '1193110276180',
    //   '923205496470@gmail.com',
    //   '03205496470'
    // );
  }

  fetchEncodedValues = async (amount, orderId, email, phone) => {
    // const encoderUrl = `http://www.clicky.pk/api/EnDe?orderRefNum=${orderId}&amount=${amount}&emailAddr=${email}&mobileNum=${phone}`;
    //const encoderUrl = `https://app.clicky.pk/api/pay/easypaisa_hash?amount=1228.0&phone=3036156978&email=alihamzabukhari007@gmail.com&orderRefNum=1594690596104`;
    const URL = `${EasyPaisaHost}api/pay/easypaisa_hash?amount=${amount}&phone=${phone}&email=${email}&orderRefNum=${orderId}`;

    console.log('fetch encoded values', URL);
    await fetch(URL, { headers })
      .then(res => res.json())
      .then(({ hash }) => {
        console.log('My encoded value is:\n', hash);
        this.setState({
          hash,
          isFetch: false
        });
      });
  };

  hideSpinner = () => {
    this.setState({ visible: false });
  };

  _onNavigationStateChange = ({ url }) => {
    this.setState({ url }, () => {
      console.log(this.state.url);
    });
  };

  pageFinished = () => {
    const { navigation } = this.props;
    const randomId = navigation.getParam('id', -1);
    const { url } = this.state;
    // if (
    //   this.state.url !==
    //   "https://easypay.easypaisa.com.pk/easypay/faces/pg/site/AuthError.jsf"
    // ) {
    if (url === EasyPaisaHost + 'index.php/?success=true') {
      navigation.navigate('PaymentSuccess', {
        id: randomId,
        isSuccess: true
      });
    } else if (url === EasyPaisaHost + 'index.php/?success=false') {
      navigation.navigate('PaymentSuccess', {
        id: randomId,
        isSuccess: false
      });
    } else {
      navigation.navigate('PaymentSuccess', { id: randomId, isSuccess: false });
    }
    // }
    // else {
    //   navigation.navigate("PaymentSuccess", { id: randomId, isSuccess: false });
    // }
  };
  render() {
    const { navigation } = this.props;
    const OrderDetail = navigation.getParam('OrderDetail', null);
    const randomId = navigation.getParam('id', -1);
    const isSuccess = navigation.getParam('isSuccess', false);
    const orderId = navigation.getParam('orderId', '1234');
    const amount = navigation.getParam('amount', '1234');
    const phone = navigation.getParam('phone', '1234');
    const email = navigation.getParam('email', 'abc@gmail.com');
    // const orderId = '1594690596104';
    // const amount = '1228';
    // const phone = '3036156978';
    // const email = 'alihamzabukhari007@gmail.com';
    const { hash, isFetch } = this.state;
    console.log(
      'Mydata',
      orderId + ' ' + amount + ' ' + phone + ' ' + email + '\n' + hash
    );
    const end = moment()
      .add(1, 'days')
      .startOf('day');
    const datetime = end.format('YYYYMMDD') + ' ';
    console.log('MyDate', datetime);
    return (
      <View style={{ flex: 1 }}>
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() =>
            navigation.navigate('orderput', {
              OrderDetail,
              orderNumber: orderId,
              isSuccess,
              alreadyPay: true
            })
          }
          LeftIcon="arrow-back"
          title="EasyPay"
        />

        {isFetch ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              opacity: 0.7,
              backgroundColor: 'transparent'
            }}
          >
            <ActivityIndicator size="large" color="green" />
          </View>
        ) : (
          <WebView
            source={{
              uri: 'https://easypay.easypaisa.com.pk/easypay/Index.jsf',
              body: `amount=${amount}.0&autoRedirect=1&emailAddr=${encodeURIComponent(
                email
              )}&expiryDate=${datetime}235959&mobileNum=${phone}&orderRefNum=${orderId}&paymentMethod=CC_PAYMENT_METHOD&postBackURL=${EasyPaisaHost}pgw/easypaisa&storeId=5503&merchantHashedReq=${hash}`,
              method: 'POST'
            }}
            onNavigationStateChange={this._onNavigationStateChange}
          />
        )}
      </View>
    );
  }
}
