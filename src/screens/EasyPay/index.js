import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { WebView } from 'react-native-webview';
import { Linking } from 'expo';
import ParentHeaders from '../../component/Header';
import moment from 'moment';
import { Buffer } from 'buffer';
import { APIConst } from '../../modules/APIs';
import { validURL } from '../../modules/utilts';
const { HOST } = APIConst;
import base64 from 'base-64';
global.Buffer = Buffer; // very important
const headers = new Headers();
headers.append(
  'Authorization',
  'Basic ' + base64.encode('app@clicky.pk:400N827JSiOP6M47fDz63G5pA2Y5InRH')
);
headers.append('method', 'GET');

export default class Easypay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      url: '',
      hash: '',
      isFetch: true
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const orderId = navigation.getParam('orderId', '332211');
    const amount = navigation.getParam('amount', '10');
    const phone = navigation.getParam('phone', '3036156978');
    const email = navigation.getParam('email', 'abc@gmail.com');
    //const orderId = '1388161321961';
    //const phone = '3035191910';
    //const email = 'admin@clicky.pk';
    const rphone = phone.indexOf('92') === 0 ? phone.slice(-10) : phone;
    this.fetchEncodedValues(amount + '.0', orderId, email, rphone);
  }

  fetchEncodedValues = (amount, orderId, email, phone) => {
    //const encoderUrl = `http://www.clicky.pk/api/EnDe?orderRefNum=${orderId}&amount=${amount}&emailAddr=${email}&mobileNum=${phone}`;
    //const encoderUrl = `https://beta.clicky.pk/api/pay/easypaisa_hash?amount=${amount}&phone=${phone}&email=${email}&orderRefNum=${orderId}`;
    //console.log("fetch encoded values", encoderUrl);
    const URL = `${HOST}/api/pay/easypaisa_hash?amount=${amount}&phone=${phone}&email=${email}&orderRefNum=${orderId}`;
    fetch(URL, { headers })
      .then(res => res.json())
      .then(({ hash }) => {
        console.log('My encoded value is:\n', hash);
        this.setState({ hash, isFetch: false });
      });
  };

  hideSpinner = () => {
    this.setState({ visible: false });
  };

  // _onNavigationStateChange = ({ url }) => {
  //this.setState({ url }, () => {
  //   console.log('Navigation URL', this.state.url);
  // });
  // this.pageFinished(url);
  //};

  _onNavigationStateChange = ({ url }) => {
    console.log('Navigation URL', url);
    if (validURL(url)) {
      const { navigation } = this.props;
      const { queryParams } = Linking.parse(url);
      const { message, pgw, status } = queryParams;
      //const JavaScriptObjected = new URL(url);
      //const { searchParams } = JavaScriptObjected;
      console.log('URL Correct', 'True');
      const Exist =
        message !== undefined && pgw !== undefined && status !== undefined;
      if (Exist) {
        const OrderDetail = navigation.getParam('OrderDetail', null);
        const orderId = navigation.getParam('orderId', '1234');
        // const message = searchParams.get('message');
        // const status = searchParams.get('status');
        //const pgw = searchParams.get('pgw');
        if (status === 'true') {
          navigation.navigate('PaymentSuccess', {
            orderNumber: orderId,
            isSuccess: true,
            OrderDetail,
            message,
            card: true
          });
        } else if (status === 'false') {
          navigation.navigate('PaymentSuccess', {
            orderNumber: orderId,
            isSuccess: false,
            OrderDetail,
            message,
            card: true
          });
        }
      }
    }
  };

  render() {
    const { navigation } = this.props;
    //const OrderDetail = navigation.getParam('OrderDetail', null);
    //const randomId = navigation.getParam('id', -1);
    //const isSuccess = navigation.getParam('isSuccess', false);
    const orderId = navigation.getParam('orderId', '1234');
    const amount = navigation.getParam('amount', '1234') + '.0';
    //const amount = '1.0';
    const phone = navigation.getParam('phone', '1234');
    const email = navigation.getParam('email', 'abc@gmail.com');
    //const orderId = '1388161321961';
    // const phone = '3035191910';
    //const email = 'admin@clicky.pk';
    const rphone = phone.indexOf('92') === 0 ? phone.slice(-10) : phone;
    const { hash, isFetch } = this.state;
    //console.log(
    // 'Mydata',
    //orderId + ' ' + amount + ' ' + phone + ' ' + email + '\n' + hash;
    //);
    const end = moment()
      .add(1, 'days')
      .startOf('day');
    const datetime = end.format('YYYYMMDD') + ' 235959';
    const body = `amount=${amount}&autoRedirect=1&emailAddr=${encodeURIComponent(
      email
    )}&expiryDate=${datetime}&mobileNum=${rphone}&orderRefNum=${orderId}&paymentMethod=CC_PAYMENT_METHOD&postBackURL=https://www.clicky.pk/pgw/easypaisa&storeId=5503&merchantHashedReq=${hash}`;

    //console.log('MyDate', datetime);
    //console.log('final request body is :', body);
    return (
      <View style={{ flex: 1 }}>
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => {
            navigation.goBack();
            // navigation.navigate('orderput', {
            //   OrderDetail,
            //   orderNumber: orderId,
            //   isSuccess
            // });
          }}
          LeftIcon="arrow-back"
          title="EasyPay"
        />

        {isFetch ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              opacity: 0.7,
              backgroundColor: 'transparent'
            }}
          >
            <ActivityIndicator size="large" color="green" />
          </View>
        ) : (
          <WebView
            source={{
              uri: 'https://easypay.easypaisa.com.pk/easypay/Index.jsf',
              body,
              method: 'POST'
            }}
            onNavigationStateChange={this._onNavigationStateChange}
          />
        )}
      </View>
    );
  }
}
