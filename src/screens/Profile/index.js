'use strict';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Image,
  Text,
  TouchableHighlight,
  ImageBackground,
  TouchableOpacity,
  View,
  StyleSheet
} from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP
} from '../../modules/MakeMeResponsive';
const CoverImg = require('../../../assets/images/Mens_jacket.jpg');
const VerifiedImg = require('../../../assets/images/verified.png');
const UnverifiedImg = require('../../../assets/images/unverified.png');
const LogoutImg = require('../../../assets/images/logout.png');
const OrderImg = require('../../../assets/images/nav_orders.png');
const AddressImg = require('../../../assets/images/address.png');
const WishlistImg = require('../../../assets/images/wishlist.png');

import { Logout, resendSignUp, otpVerifyPhone } from '../../redux/action';
import VerifyPhoneModal from '../../component/verifyPhoneModal';
import { MaterialIcons as Icon } from '@expo/vector-icons';
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countCollections: 0,
      countOrders: 0,
      countFollowing: 0,
      countFollowers: 0,
      ModalOn: false
    };
  }
  toggle = ModalOn => {
    this.setState({ ModalOn });
  };

  render() {
    const {
      Logout,
      firstName,
      lastName,
      orders_qty,
      wishListLength,
      profileDP,
      address,
      userDetailAvailable,
      resendSignUp,
      otpVerifyPhone,
      phone,
      verified,
      navigation
    } = this.props;
    const { navigate, goBack } = navigation;
    const { ModalOn } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {verified !== true && phone !== undefined && (
          <VerifyPhoneModal
            whilecheckout={true}
            phone={phone}
            skipper={() => this.toggle(false)}
            CloseModal={() => this.toggle(false)}
            otpVerification={otpVerifyPhone}
            resendSignUp={() => {
              resendSignUp({ phone });
            }}
            ModalOn={ModalOn}
          />
        )}
        <ImageBackground
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(50)
          }}
          source={CoverImg}
          resizeMode="stretch"
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: heightPercentageToDP(7),
              height: heightPercentageToDP(8),
              width: widthPercentageToDP(100)
            }}
          >
            <Icon
              name="arrow-back"
              color="#fff"
              onPress={() => goBack()}
              size={widthPercentageToDP(6)}
              style={{ marginLeft: widthPercentageToDP(5) }}
            />
            <TouchableOpacity
              onPress={() => Logout(navigation)}
              style={{
                height: widthPercentageToDP(8),
                width: widthPercentageToDP(8),
                marginRight: widthPercentageToDP(5)
              }}
            >
              <Image
                style={{
                  height: widthPercentageToDP(8),
                  width: widthPercentageToDP(8),
                  tintColor: '#fff'
                }}
                resizeMode="contain"
                source={LogoutImg}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              position: 'absolute',
              bottom: '2%',
              left: '4%',
              right: '0%'
            }}
          >
            <Text
              style={{
                color: '#fff',
                fontSize: widthPercentageToDP(5),
                fontWeight: '300',
                marginLeft: widthPercentageToDP(5)
              }}
            >
              {firstName + ' ' + lastName}
            </Text>
          </View>
        </ImageBackground>
        <View style={{ flexDirection: 'row' }}>
          <TouchableHighlight onPress={() => navigate('wishList')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: widthPercentageToDP(14)
              }}
            >
              <Image
                style={{
                  height: widthPercentageToDP(12),
                  width: widthPercentageToDP(12),
                  left: '0%',
                  resizeMode: 'contain'
                }}
                source={WishlistImg}
              />
              <View
                style={{
                  height: '100%',
                  width: '85%',

                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginRight: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: '400',
                    color: '#333',
                    marginBottom: 2
                  }}
                  ellipsizeMode="tail"
                >
                  {'Wish List'}
                </Text>

                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: '300',
                    color: '#333',
                    textAlign: 'right',
                    marginTop: 3
                  }}
                  ellipsizeMode="tail"
                >
                  {'items('}
                  {wishListLength}
                  {')'}
                </Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(0.1),
            backgroundColor: '#000'
          }}
        />
        <View style={{ flexDirection: 'row' }}>
          <TouchableHighlight onPress={() => navigate('OrdersListScreen')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: widthPercentageToDP(14)
              }}
            >
              <Image
                style={{
                  height: widthPercentageToDP(12),
                  width: widthPercentageToDP(12),
                  left: '0%',
                  resizeMode: 'contain'
                }}
                source={OrderImg}
              />
              <View
                style={{
                  height: '100%',
                  width: '85%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginRight: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: '400',
                    color: '#333',
                    marginBottom: 2
                  }}
                  ellipsizeMode="tail"
                >
                  {'Orders'}
                </Text>

                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: '300',
                    color: '#333',
                    marginTop: 3
                  }}
                  ellipsizeMode="tail"
                >
                  {orders_qty} {'orders'}
                </Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(0.1),
            backgroundColor: '#000'
          }}
        />
        <View style={{ flexDirection: 'row' }}>
          <TouchableHighlight onPress={() => navigate('address')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: widthPercentageToDP(14)
              }}
            >
              <Image
                style={{
                  height: widthPercentageToDP(12),
                  width: widthPercentageToDP(12),
                  left: '0%',
                  resizeMode: 'contain'
                }}
                source={AddressImg}
              />
              <View
                style={{
                  height: '100%',
                  width: '85%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginRight: 5
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: '400',
                    color: '#333',
                    marginBottom: 2
                  }}
                  // numberOfLines={2}
                  ellipsizeMode="tail"
                >
                  {'Address'}
                </Text>

                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: '300',
                    color: '#333',
                    marginTop: 3
                    //marginLeft:10
                  }}
                  //numberOfLines={2}
                  ellipsizeMode="tail"
                >
                  Edit
                </Text>
              </View>
            </View>
          </TouchableHighlight>
        </View>
        <View
          style={{
            width: widthPercentageToDP(100),
            height: heightPercentageToDP(0.1),
            backgroundColor: '#000'
          }}
        />
        {phone !== undefined && (
          <View
            style={{
              width: widthPercentageToDP(100)
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                disabled={verified}
                onPress={() => this.toggle(true)}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    height: widthPercentageToDP(14)
                  }}
                >
                  <Image
                    style={{
                      height: widthPercentageToDP(12),
                      width: widthPercentageToDP(12),
                      left: '0%',
                      resizeMode: 'contain'
                    }}
                    source={verified ? VerifiedImg : UnverifiedImg}
                  />
                  <View
                    style={{
                      height: '100%',
                      width: '85%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginRight: 5
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: '400',
                        color: '#333',
                        marginBottom: 2
                      }}
                      ellipsizeMode="tail"
                    >
                      {'+' + phone}
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: '300',
                        color: '#333',
                        marginTop: 3
                        //marginLeft:10
                      }}
                      ellipsizeMode="tail"
                    >
                      {verified ? 'Verified' : 'Verify'}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(0.1),
                backgroundColor: '#000'
              }}
            />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { USER } = state;
  const {
    firstName,
    lastName,
    orders_qty,
    wishListLength,
    profileDP,
    address,
    userDetailAvailable,
    phone,
    verified
  } = USER;
  return {
    firstName,
    lastName,
    orders_qty,
    wishListLength,
    profileDP,
    address,
    userDetailAvailable,
    phone,
    verified
  };
};
export default connect(
  mapStateToProps,
  { Logout, resendSignUp, otpVerifyPhone }
)(Profile);
