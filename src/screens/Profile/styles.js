import { StyleSheet, StatusBar } from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
export default StyleSheet.create({
  cont: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center"
  },

  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#FFF",
    width: "100%"
  },
  otherThenStatusBarView: { flex: 1, backgroundColor: "#FFF", width: "100%" },
  OtherTHenLOGOView: {
    height: "85%",
    width: widthPercentageToDP(100),
    backgroundColor: "#FFF"
  }
});
