"use strict";
import React from "react";
import { View, Text, StatusBar, FlatList } from "react-native";
import CartItems from "../../component/CartItems";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Stepper from "../../component/Stepper";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import ParentHeaders from "../../component/Header";
import EmptyCart from "../EmptyCart";
import { ADDorREMOVECART } from "../../redux/action";
class Cart extends React.PureComponent {
  static propTypes = {
    user: PropTypes.object,
    ADDorREMOVECART: PropTypes.func
  };

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const { navigation, user, ADDorREMOVECART } = this.props;
    const { cart, token } = user;
    let length = 0,
      Quantity = 0,
      TotalPrice = 0;
    if (cart) {
     // console.log("CART", cart);
      const { items, qty, subtotal } = cart;
      if (items) {
        length = items.length;
        Quantity = qty;
        TotalPrice = subtotal;
      }
    }

    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Cart"
        />
        {length === 0 ? (
          <EmptyCart navigation={navigation} />
        ) : (
          <View style={{ flex: 1 }}>
            <View style={{ height: "5%" }} />
            <Stepper
              stepOneBgColor={"#000"}
              stepOnetextColor={"#fff"}
              stepTwoBgColor={"#fff"}
              stepTwotextColor={"#000"}
              stepThreeBgColor={"#fff"}
              stepThreetextColor={"#000"}
              stepFourBgColor={"#fff"}
              stepFourtextColor={"#000"}
            />
            <View style={{ height: heightPercentageToDP(48) }}>
              {length !== 0 && (
                <FlatList
                  //contentContainerStyle={{ alignItems: 'center' }}
                  style={{ marginTop: 5, marginBottom: 5 }}
                  keyExtractor={this._keyExtractor}
                  showsVerticalScrollIndicator={false}
                  data={cart.items}
                  renderItem={({ item }) => {
                    const { product, variant, qty } = item;
                    const { img, name } = product;
                    const { mrp, price } = variant;
                    return (
                      <CartItems
                        pid={product._id}
                        vid={variant._id}
                        navigation={navigation}
                        productName={name}
                        productAmount={mrp}
                        productDetailAmount={price}
                        QTY={qty}
                        IMG={img.medium}
                        itemsQtyFunc={ADDorREMOVECART}
                        token={token}
                      />
                    );
                  }}
                />
              )}
            </View>
            <View
              style={{
                flex: 1,
                borderTopWidth: heightPercentageToDP(0.1),
                borderTopColor: "#000",
                width: "100%",
                position: "absolute",
                left: 0,
                right: 0,
                bottom: 0
              }}
            >
              <View
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {"Total Amount"}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {TotalPrice + "PKR"}
                </Text>
              </View>
              <View
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {"Price Discount"}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {"0 PKR"}
                </Text>
              </View>
              <View
                style={{
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginLeft: 10,
                  marginRight: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {"To be paid"}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "400",
                    color: "#000",
                    textAlign: "left"
                  }}
                >
                  {"PKR " + TotalPrice}
                </Text>
              </View>

              <View
                style={{
                  alignItems: "center",
                  marginBottom: heightPercentageToDP(2)
                }}
              >
                <Button
                  title="CHECKOUT"
                  titleStyle={{
                    color: "#000",
                    textAlign: "center",
                    fontSize: 20
                  }}
                  onPress={() => navigation.navigate("checkOutScreenDecider")}
                  buttonStyle={{
                    width: widthPercentageToDP(80),
                    height: heightPercentageToDP(8),
                    backgroundColor: "#e3e627",
                    borderRadius: widthPercentageToDP(30),
                    elevation: 3
                  }}
                />
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(
  mapStateToProps,
  {
    ADDorREMOVECART
  }
)(Cart);
