"use strict";
import React from "react";
import {
  View,
  Alert,
  StyleSheet,
  Modal,
  TouchableOpacity,
  Image
} from "react-native";
import PropTypes from "prop-types";
import {
  heightPercentageToDP,
  widthPercentageToDP
} from "../../modules/MakeMeResponsive";
import { connect } from "react-redux";
import { Loader } from "../../redux/action";
import { EvilIcons as ICON } from "@expo/vector-icons";
class ChartSize extends React.Component {
  static propTypes = {
    imgURL: PropTypes.string,
    closeFunc: PropTypes.func,
    Loader: PropTypes.func,
    Visible: PropTypes.bool
  };
  render() {
    const { imgURL, closeFunc, Visible, Loader } = this.props;
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={Visible}
        onRequestClose={closeFunc}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              width: widthPercentageToDP(100),
              height: heightPercentageToDP(8),
              flexDirection: "row",
              justifyContent: "space-between",
              paddingHorizontal: "2%",
              alignItems: "center",
              borderBottomWidth: StyleSheet.hairlineWidth
            }}
          >
            <TouchableOpacity onPress={closeFunc}>
              <ICON
                name="close"
                size={widthPercentageToDP(10)}
                color="#000000"
              />
            </TouchableOpacity>
          </View>
          {Visible && (
            <Image
              //loadingIndicatorSource={[1.2, 3, 4, 5]}
              onLoadStart={() => {
                Loader(true);
              }}
              // onLoadEnd={({}) => {
              //   //  Loader(false);
              //   console.log("Fin");
              // }}
              onLoad={() => {
                Loader(false);
              }}
              onError={({ nativeEvent }) => {
                Alert.alert("Missing!", "We are sorry size chart is Missing");
                Loader(false);
                closeFunc();
              }}
              source={{ uri: imgURL }}
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(92)
              }}
              resizeMode={"contain"}
            />
          )}
        </View>
      </Modal>
    );
  }
}
export default connect(
  null,
  { Loader }
)(ChartSize);
