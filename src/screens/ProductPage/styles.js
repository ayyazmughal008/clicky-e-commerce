import { StyleSheet, StatusBar } from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
export default StyleSheet.create({
  cont: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center"
  },

  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#FFF",
    width: "100%"
  },
  otherThenStatusBarView: {
    flex: 1,
    backgroundColor: "#FFF",
    width: "100%"
  },
  TopTitleBar: {
    paddingHorizontal: "2%",
    width: widthPercentageToDP(100),
    height: "8%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: "#E2E2E2"
  },
  DetailscrollView: {
    width: widthPercentageToDP(100),
    height: "94%"
  },
  BottomBar: {
    height: "6%",
    width: widthPercentageToDP(100),
    borderTopWidth: 0.5,
    borderTopColor: "#E2E2E2",
    flexDirection: "row"
  },
  ModalPrice: {
    color: "#ED5447",
    fontSize: widthPercentageToDP(4.5),
    fontWeight:'bold'
  },
  ModalProductName: { fontSize: widthPercentageToDP(4) },
  TopTitleBarText: {
    fontSize: widthPercentageToDP(8),
    color: "#000",
    fontWeight: "bold"
  },
  FilterAndSortView: {
    width: widthPercentageToDP(100),
    height: "6%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "#FBFBFB"
  },
  FilterAndSortViewText: {
    fontSize: widthPercentageToDP(3),
    color: "#7c7c7c"
  },
  leftTopBarIcon: {
    left: "2%",
    position: "absolute"
  },
  ProductImagesSliderView: {
    width: widthPercentageToDP(100),
    height: "55%",
    backgroundColor: "#543"
  },
  Indicator: {
    position: "absolute",
    top: "50%",
    left: "45%",
    zIndex: 3
  },
  contentContainer: {
    flexGrow: 1,
    width: widthPercentageToDP(100)
  },
  DotStyle: {
    width: widthPercentageToDP(2.5),
    height: widthPercentageToDP(2.5),
    borderRadius: widthPercentageToDP(1.25)
  },
  SWiperView: {
    backgroundColor: "#FFF",
    width: "100%",
    height: heightPercentageToDP(50)
  },
  slideV: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF"
  },
  ProductName: {
    fontSize: widthPercentageToDP(4.5),
    fontWeight: "bold",
    textAlign: "center"
  },
  ProductNo: { fontSize: widthPercentageToDP(3), color: "#A6A6A6" },
  ProductSmallDetailView: {
    width: "100%",
    height: heightPercentageToDP(25),
    justifyContent: "space-around",
    alignItems: "center"
  },
  ProductSmallDetailViewPartial2: {
    width: "65%",
    height: "50%",

    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  ProductSmallDetailViewPartial2Inner: {
    width: "35%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  ContentImagePH: {
    width: "100%",
    height: "100%"
  },
  ProductSmallDetailViewPartial2InnerC1: {
    width: widthPercentageToDP(16),
    height: widthPercentageToDP(16),
    borderRadius: widthPercentageToDP(8),
    borderColor: "#F35343",
    borderWidth: widthPercentageToDP(0.5),
    alignItems: "center",
    justifyContent: "center"
  },
  CircleSubText: {
    fontSize: widthPercentageToDP(2.5)
  },
  ProductSmallDetailViewPartial2InnerC1Text: { color: "#F35343" },
  ProductSmallDetailViewPartial2InnerC2: {
    width: widthPercentageToDP(16),
    height: widthPercentageToDP(16),
    borderRadius: widthPercentageToDP(8),
    borderColor: "#5DA4D2",
    borderWidth: widthPercentageToDP(0.5),
    alignItems: "center",
    justifyContent: "center"
  },
  ProductSmallDetailViewPartial2InnerC2Text: { color: "#5DA4D2" },
  ViewNum: {
    fontSize: widthPercentageToDP(5.5)
  },
  ViewPrice: {
    fontSize: widthPercentageToDP(6),
    fontWeight: "bold"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  BottomBarBtn1: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row"
  },
  BottomBarBtn2: {
    borderLeftColor: "#E2E2E2",
    borderLeftWidth: 0.5,
    width: "25%",
    height: "100%",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row"
  },
  BottomBarBtn3: {
    width: "27.5%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#444444"
  },

  BottomBarBtn4: {
    width: "27.5%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F15441"
  },
  B_C_TEXT: {
    fontSize: widthPercentageToDP(4),
    fontWeight: "bold",
    color: "#FFFFFF"
  }
});
