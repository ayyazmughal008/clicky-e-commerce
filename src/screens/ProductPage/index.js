'use strict';
import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  Alert,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Sections from '../../component/SectionOpener';
import SizeChart from './SizeChartModal';
import { templateCofig } from '../../config.json';
import PropTypes from 'prop-types';
import Swiper from 'react-native-swiper';
import TextComponent from '../../component/TextComponent';
import { Button, Icon } from 'react-native-elements';
import ListThumb from '../../component/CategoryCard/PullandBear/ListThumb';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import { connect } from 'react-redux';
import { APIFuncs } from '../../modules/APIs';
import { checkWishlistItemExist, onShareItem } from '../../modules/utilts';
import {
  ADDorREMOVECART,
  AddorRemoveWishList,
  BusyFetchingData
} from '../../redux/action';
import Header from '../../component/Header/headerCart';
import { ProgressiveImage } from '../../component/progressive';
const PlaceHolder = require('../../../assets/images/placeholderl.png');
class ProductPage extends React.Component {
  static propTypes = {
    AddorRemoveWishList: PropTypes.func,
    ADDorREMOVECART: PropTypes.func,
    user: PropTypes.object
  };
  constructor(props) {
    super(props);
    const { ProductID } = props.navigation.state.params;
    this.state = {
      ProductNo: ProductID,

      selectedVarientID: '',
      SKU: '',
      ProductName: '',
      ProductID: '',
      SelectedSize: '',
      ChartSizeImage: '',

      ProductFullDetail: null,
      selectedVarient: null,

      Price: 0,
      PerOff: 0,
      MRP: 0,
      Stock: 0,
      quantity: 1,

      Loading: true,
      modalOn: false,
      BuyNow: false,
      isModalVisible: false,
      ChartSizeModal: false,
      ItemAdded: false,

      ProductImages: [],
      variants: [],
      GroupData: [],
      similarProduct: []
    };
  }
  componentDidMount = () => {
    const { ProductID } = this.props.navigation.state.params;
    this.GetProductDetailByID(ProductID);
  };
  MakeInitialSomeStates = () => {
    this.setState({
      quantity: 1,
      modalOn: false,
      BuyNow: false,
      SelectedSize: '',
      ChartSizeModal: false,
      ChartSizeImage: ''
    });
  };
  GetProductDetailByID = ProductID => {
    this.MakeInitialSomeStates();
    //console.log("ProductID", ProductID);
    const { BusyFetchingData } = this.props;
    BusyFetchingData(true);
    APIFuncs.GetProductDetail(ProductID).then(RES => {
      // console.log("WholeDetail", RES);
      if (RES) {
        const { variants, group, categories } = RES;
        if (group) {
          APIFuncs.getGroupDetail(group).then(GroupData => {
            //console.log("groupCalled", group);
            this.setState({ GroupData });
          });
        } else {
          this.setState({ GroupData: [] });
        }
        if (Array.isArray(categories)) {
          if (categories.length !== 0) {
            const LastIndex = categories.length - 1;
            const similarProductID = categories[LastIndex]._id;
            APIFuncs.getSimilarProducts(similarProductID).then(
              similarProduct => {
                if (Array.isArray(similarProduct)) {
                  this.setState({
                    similarProduct
                  });
                }
              }
            );
          }
        }
        const { price, mrp, stock } = variants[0];
        const PerOff = (((mrp - price) / mrp) * 100).toFixed(0);
        this.setState({
          ProductFullDetail: RES,
          ChartSizeImage: RES.sizechart,
          ProductID,
          SKU: RES.sku,
          ProductName: RES.name,
          variants: RES.variants,
          Price: price.toFixed(0),
          MRP: mrp.toFixed(0),
          Stock: stock,
          isModalVisible: false,
          ProductImages: RES.imgA,
          PerOff,
          Loading: false,
          ItemAdded: false
        });
        if (this.ScrollContRef) {
          this.ScrollContRef.scrollTo({ x: 0, y: 0, animated: true });
          this.Section1.setState({ Opened: false });
        }
      }
      BusyFetchingData(false);
    });
  };

  onSizeChartModal = SwitchModal =>
    this.setState({
      ChartSizeModal: SwitchModal
    });
  getDataForSelectedGroupMember = ProductID => {
    const { BusyFetchingData } = this.props;
    BusyFetchingData(true);
    this.setState({
      Loading: false
    });
    APIFuncs.GetProductDetail(ProductID).then(RES => {
      const { variants } = RES;
      const { price, mrp, stock } = variants[0];
      const PerOff = (((mrp - price) / mrp) * 100).toFixed(0);
      this.setState({
        selectedVarientID: '',
        ChartSizeImage: RES.sizechart,
        ProductID,
        SKU: RES.sku,
        ProductFullDetail: RES,
        ProductName: RES.name,
        variants: RES.variants,
        Price: price.toFixed(0),
        MRP: mrp.toFixed(0),
        Stock: stock,
        isModalVisible: false,
        ProductImages: RES.imgA,
        PerOff,
        Loading: false,
        ItemAdded: false
      });
      BusyFetchingData(false);
    });
  };
  _keyExtractor = (item, index) => 'unique' + index;
  render() {
    const { getDataForSelectedGroupMember, props, state } = this;
    const { listingTemplate } = templateCofig;
    const { listingItems } = listingTemplate;
    const { design } = listingItems;
    const {
      navigation,
      WishlistTouch,
      AddorRemoveWishList,
      ADDorREMOVECART,
      user
    } = props;
    const {
      Loading,
      BuyNow,
      modalOn,
      Stock,
      selectedVarient,
      SKU,
      ProductName,
      ProductNo,
      Price,
      PerOff,
      MRP,
      ProductImages,
      variants,
      quantity,
      ProductFullDetail,
      GroupData,
      ProductID,
      selectedVarientID,
      isModalVisible,
      ItemAdded,
      ChartSizeModal,
      ChartSizeImage,
      similarProduct
    } = state;
    let { cart, wishlist, token, isLoggedIn } = user;
    let cartQuantity = 0;
    let itemFoundAtInWishList = -1;
    if (cart) {
      cartQuantity = cart.qty;
    }
    //const IsInWishlist = wishlist.includes(ProductNo);
    if (variants.length)
      itemFoundAtInWishList = checkWishlistItemExist(wishlist, {
        product: {
          _id: ProductNo
        },
        variant: {
          _id: variants[0]._id
        }
      });
    return (
      <View style={{ flex: 1 }}>
        <SizeChart
          Visible={ChartSizeModal}
          closeFunc={() => this.onSizeChartModal(false)}
          imgURL={ChartSizeImage}
        />
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={isModalVisible}
          onRequestClose={() =>
            this.setState({ isModalVisible: false, selectedVarientID: '' })
          }
        >
          <TouchableWithoutFeedback
            onPress={() =>
              this.setState({ isModalVisible: false, selectedVarientID: '' })
            }
          >
            <View
              style={{
                height: heightPercentageToDP(40),
                width: widthPercentageToDP(100)
              }}
            />
          </TouchableWithoutFeedback>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              position: 'absolute',
              bottom: '10%'
            }}
          >
            <View
              style={{
                height: heightPercentageToDP(40),
                width: widthPercentageToDP(100),
                backgroundColor: '#fff',
                opacity: 0.9,
                shadowRadius: 2,
                borderRadius: 5,
                shadowColor: '#cccccc'
              }}
            >
              <View
                style={{
                  borderBottomWidth: heightPercentageToDP(0.1),
                  borderBottomColor: '#000',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  padding: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'Metropolis_Regular',
                    fontWeight: '300'
                  }}
                >
                  SELECT A SIZE
                </Text>
                <TouchableOpacity onPress={() => this.onSizeChartModal(true)}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: 'Metropolis_Regular',
                      fontWeight: '300'
                    }}
                  >
                    Size guide
                  </Text>
                </TouchableOpacity>
              </View>

              <FlatList
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 5, marginBottom: 5 }}
                keyExtractor={this._keyExtractor}
                showsHorizontalScrollIndicator={false}
                extraData={this.state}
                data={variants}
                renderItem={({ item }) => {
                  const { selectedVarientID } = this.state;
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        if (item.stock)
                          this.setState({
                            selectedVarientID: item._id
                          });
                        else {
                          Alert.alert('Sorry', 'OUT OF STOCK!!');
                        }
                      }}
                    >
                      <TextComponent
                        makeMeBold={selectedVarientID === item._id}
                        text1={item.size}
                      />
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              height: heightPercentageToDP(10),
              width: widthPercentageToDP(100),
              backgroundColor: '#fff',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Button
              title="Add to Cart"
              titleStyle={{ color: '#fff', textAlign: 'center', fontSize: 20 }}
              onPress={() => {
                if (this.state.selectedVarientID) {
                  ADDorREMOVECART(
                    {
                      pid: ProductID,
                      vid: selectedVarientID,
                      qty: quantity
                    },
                    token
                  ).then(() => {
                    this.setState({
                      isModalVisible: false,
                      selectedVarientID: '',
                      ItemAdded: true
                    });
                  });
                }
              }}
              buttonStyle={{
                width: widthPercentageToDP(95),
                height: heightPercentageToDP(7),
                backgroundColor: this.state.selectedVarientID
                  ? '#4EC98F'
                  : '#000',
                marginBottom: 5
              }}
              icon={
                <Icon
                  name="shopping-cart"
                  size={15}
                  color="#fff"
                  iconStyle={{ marginRight: 15 }}
                />
              }
            />
          </View>
        </Modal>
        <StatusBar
          animated={true}
          backgroundColor={'#FFF'}
          barStyle={'dark-content'}
        />
        <Header
          leftClickHandler={() => navigation.goBack()}
          leftIcon="arrow-back"
          centerText="CLICKY"
          right1ClickHandler={() => navigation.navigate('Cart')}
          right1Icon="shopping-cart"
          right2ClickHandler={() => {
            onShareItem(this.state.ProductID);
          }}
          right2Icon="share"
          cart={cart}
        />
        <ScrollView
          ref={ScrRef => (this.ScrollContRef = ScrRef)}
          style={{ flex: 1 }}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <View
            style={{
              height: heightPercentageToDP(60),
              width: widthPercentageToDP(100)
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(5),
                justifyContent: 'center',
                padding: widthPercentageToDP(2.5),
                alignItems: 'center',
                backgroundColor: '#FFF'
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000',
                  textAlign: 'center'
                }}
              >
                {ProductName}
              </Text>
            </View>
            {ProductImages.length !== 0 && (
              <Swiper
                activeDotColor={'#F35343'}
                dotColor={'#FFFFFF'}
                autoplay={false}
                loop={false}
              >
                {ProductImages.map((item, index) => (
                  <View
                    key={'Swiper' + index}
                    style={{
                      width: widthPercentageToDP(100),
                      height: heightPercentageToDP(55),
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#FFF'
                    }}
                  >
                    <ProgressiveImage
                      style={{
                        width: widthPercentageToDP(100),
                        height: heightPercentageToDP(55)
                      }}
                      thumbnailSource={PlaceHolder}
                      resizeMode="cover"
                      source={{ uri: item.large }}
                    />
                  </View>
                ))}
              </Swiper>
            )}
            {isLoggedIn && (
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: '1%',
                  right: '5%',
                  height: widthPercentageToDP(8),
                  width: widthPercentageToDP(8)
                }}
                onPress={() => {
                  const Tempwishlist = wishlist;
                  console.log('wishlist', Tempwishlist);
                  console.log('ItemFound At', itemFoundAtInWishList);
                  if (itemFoundAtInWishList !== -1) {
                    Tempwishlist.splice(itemFoundAtInWishList, 1);
                    AddorRemoveWishList(
                      {
                        product: {
                          _id: ProductNo,
                          name: ProductName,
                          imgA: ProductImages
                        },
                        variant: {
                          _id: variants[0]._id,
                          price: Price
                        }
                      },
                      Tempwishlist,
                      token
                    );
                    console.log('UnChecked');
                  } else {
                    Tempwishlist.push({
                      product: {
                        _id: ProductNo,
                        name: ProductName,
                        imgA: ProductImages
                      },
                      variant: {
                        _id: variants[0]._id,
                        price: Price
                      }
                    });
                    AddorRemoveWishList(
                      {
                        product: {
                          _id: ProductNo,
                          name: ProductName,
                          imgA: ProductImages
                        },
                        variant: {
                          _id: variants[0]._id,
                          price: Price
                        }
                      },
                      Tempwishlist,
                      token
                    );
                    console.log('Checked');
                  }
                }}
              >
                <Image
                  source={
                    itemFoundAtInWishList !== -1
                      ? require('../../../assets/images/active_wish.png')
                      : require('../../../assets/images/inactive_wish.png')
                  }
                  resizeMode="contain"
                  style={{
                    height: widthPercentageToDP(8),
                    width: widthPercentageToDP(8)
                  }}
                />
              </TouchableOpacity>
            )}
          </View>

          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center'
            }}
          >
            <FlatList
              style={{
                marginTop: 5,
                marginBottom: 5,
                width: widthPercentageToDP(50)
              }}
              keyExtractor={this._keyExtractor}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={GroupData}
              extraData={this.state}
              ItemSeparatorComponent={() => (
                <View
                  style={{
                    width: widthPercentageToDP(2),
                    height: widthPercentageToDP(1)
                  }}
                />
              )}
              renderItem={({ item }) => {
                const { ProductID } = this.state;
                const { _id, imgA } = item;
                return (
                  <TouchableOpacity
                    onPress={() => {
                      getDataForSelectedGroupMember(_id);
                    }}
                    style={{
                      width: widthPercentageToDP(11),
                      height: widthPercentageToDP(11),
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth:
                        ProductID === _id ? widthPercentageToDP(1) : 0,
                      borderColor: '#F15441'
                    }}
                  >
                    <Image
                      source={{ uri: imgA[0].small }}
                      style={{
                        width: widthPercentageToDP(10),
                        height: widthPercentageToDP(10)
                      }}
                      resizeMode={'contain'}
                    />
                  </TouchableOpacity>
                );
              }}
            />
            {MRP !== Price && (
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  color: 'red',
                  textDecorationLine: 'line-through',
                  marginRight: widthPercentageToDP(2)
                }}
              >
                {MRP + ' PKR'}
              </Text>
            )}

            <Text
              style={{
                fontSize: widthPercentageToDP(6),
                fontWeight: '400',
                marginRight: widthPercentageToDP(5)
              }}
            >
              {Price + ' PKR'}
            </Text>
          </View>

          {/* Bottom Button */}

          <View
            style={{
              flex: 1,
              marginTop:
                GroupData.length === 0
                  ? heightPercentageToDP(10)
                  : heightPercentageToDP(8),
              width: widthPercentageToDP(100),
              alignItems: 'center'
            }}
          >
            <View
              style={{
                width: widthPercentageToDP(100),
                height: heightPercentageToDP(5),
                paddingHorizontal: widthPercentageToDP(2.5)
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000',
                  textAlign: 'left',
                  justifyContent: 'center'
                }}
              >
                SKU: {SKU}
              </Text>
            </View>

            <Button
              title={ItemAdded ? 'Checkout' : 'Add to Cart'}
              titleStyle={{
                color: '#fff',
                textAlign: 'center',
                fontSize: 20
              }}
              onPress={() => {
                if (ItemAdded) {
                  navigation.navigate('Cart');
                } else {
                  design === 'PullandBear'
                    ? this.setState({ isModalVisible: true })
                    : this.setState({
                        modalOn: true,
                        BuyNow: false
                      });
                }
              }}
              buttonStyle={{
                width: widthPercentageToDP(95),
                height: heightPercentageToDP(7),
                backgroundColor: ItemAdded ? '#4EC98F' : '#000',
                marginBottom: heightPercentageToDP(2)
              }}
              icon={
                <Icon
                  name={ItemAdded ? 'playlist-add-check' : 'shopping-cart'}
                  size={15}
                  color="#fff"
                  iconStyle={{ marginRight: 15 }}
                />
              }
            />

            <Sections
              ref={SecRef => (this.Section1 = SecRef)}
              title={'Product Information'}
              style={{
                width: widthPercentageToDP(95)
              }}
            >
              <View
                style={{
                  padding: 10,
                  marginTop: heightPercentageToDP(2)
                }}
              >
                {ProductFullDetail !== null && (
                  <Text>{ProductFullDetail.metaDescription}</Text>
                )}
              </View>
            </Sections>
            <View
              style={{
                justifyContent: 'center',
                marginTop: heightPercentageToDP(2)
              }}
            >
              <Text
                style={{
                  fontSize: widthPercentageToDP(4),
                  fontWeight: '500',
                  color: '#000',
                  padding: widthPercentageToDP(4)
                }}
              >
                Clicky Recommends
              </Text>
            </View>
            <FlatList
              style={{ marginLeft: 5, marginRight: 5, marginTop: 5 }}
              keyExtractor={this._keyExtractor}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              data={similarProduct}
              renderItem={({ item, index }) => {
                const { price, imgA, name, mrp, _id } = item;
                let ImageAvaliable = imgA !== undefined ? true : false;
                if (ImageAvaliable) {
                  ImageAvaliable = imgA.length !== 0 ? true : false;
                  if (ImageAvaliable) {
                    ImageAvaliable = imgA[0].large !== undefined ? true : false;
                  }
                }
                return (
                  <ListThumb
                    key={'similar' + index}
                    productID={_id}
                    navigation={navigation}
                    sourceImage={
                      ImageAvaliable ? { uri: imgA[0].large } : PlaceHolder
                    }
                    title={name}
                    heading={name}
                    realAmount={mrp}
                    discountAmount={price}
                    disoff={Math.ceil(((mrp - price) / mrp) * 100)}
                    clickHandler={() => {
                      this.GetProductDetailByID(_id);
                    }}
                  />
                );
              }}
            />
          </View>
          {/*////////////////////////////////////////////////////////////////////////////////////////////////*/}
        </ScrollView>
        {/* </View> */}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  user: state.USER
});
export default connect(
  mapStateToProps,
  { BusyFetchingData, ADDorREMOVECART, AddorRemoveWishList }
)(ProductPage);
