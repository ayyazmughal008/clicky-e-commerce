import { StyleSheet, StatusBar } from "react-native";
import { widthPercentageToDP } from "../../modules/MakeMeResponsive";
export default StyleSheet.create({
  cont: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center"
  },

  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#FFF",
    width: "100%"
  },
  otherThenStatusBarView: {
    // flex: 1,
    // backgroundColor: "#FFF",
    // width: "100%"
  },
  TopTitleBar: {
    paddingHorizontal: "2%",
    width: widthPercentageToDP(100),
    height: "8%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: "#E2E2E2"
  },
  FlatList: {
    height: "84%",
    width: "100%"
  },
  ScreenBottomView: {
    height: "8%",
    width: "100%",
    backgroundColor: "#FBFBFB",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  ScreenBottomViewLeft: {
    height: "98%",
    width: "50%",
    alignItems: "center",
    justifyContent: "center"
  },
  ScreenBottomViewRight: {
    height: "98%",
    width: "48%",
    alignItems: "flex-end"
  },
  leftTopBarIcon: {
    left: "2%",
    position: "absolute"
  },
  TopTitleBarText: {
    fontSize: widthPercentageToDP(8),
    color: "#000",
    fontWeight: "bold"
  },
  PreviousPrice: {
    textDecorationLine: "line-through",
    fontSize: widthPercentageToDP(3),
    color: "#999999"
  },
  Prices: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "40%",
    height: "100%",
    alignItems: "center"
  },
  CurrentPrice: {
    color: "#F15441",
    fontSize: widthPercentageToDP(3.5),
    fontWeight: "bold"
  },
  ItemBottomView: {
    flexDirection: "row",
    width: "100%",
    height: "30%"
  },
  ItemTopView: { width: "100%", height: "50%" },
  ItemMidView: { width: "100%", height: "20%" },
  Quantity: {
    flexDirection: "row",
    justifyContent: "flex-end",
    width: "60%",
    height: "100%",
    alignItems: "center"
  },
  ScreenBottomViewRightButton: {
    backgroundColor: "#f15441",
    borderRadius: widthPercentageToDP(2),
    alignItems: "center",
    justifyContent: "center",
    width: "70%",
    height: "100%"
  },
  ScreenBottomViewRightButtonText: {
    color: "#FFFFFF",
    fontSize: widthPercentageToDP(3.5)
  },
  ScreenBottomViewLeftViewDown: {
    width: "100%",
    height: "43%"
  },
  ScreenBottomViewLeftViewUP: {
    flexDirection: "row",
    width: "100%",
    height: "55%",
    alignItems: "center"
  },
  ScreenBottomViewLeftViewUPPrice: {
    fontSize: widthPercentageToDP(3.5),
    color: "#F15444"
  },
  ScreenBottomViewLeftViewUPTitle: {
    fontSize: widthPercentageToDP(3.5),
    color: "#888888",
    fontWeight: "bold"
  }
});
