import React, { Component } from "react";
import { View, Text, StatusBar, FlatList } from "react-native";
import Style from "./styles";
//import { Button } from "react-native-elements";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../modules/MakeMeResponsive";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { AddorRemoveWishList } from "../../redux/action";
import ParentHeaders from "../../component/Header";
import WishListItems from "../../component/WishListItems";
import EmptyHeart from "../../../assets/animation/heart.json";
import Lottie from "lottie-react-native";
import { ClickyButton } from "../../component/Buttons";
class wishlistScreen extends Component {
  static propTypes = {
    user: PropTypes.object,
    AddorRemoveWishList: PropTypes.func
  };

  _keyExtractor = (item, index) => "wishlist" + index;

  componentDidMount() {
    const { length } = this.props.user.wishlist;
    if (length === 0) this.animation.play();
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { length } = this.props.user.wishlist;
    if (length === 0) this.animation.play();
  }
  render() {
    const { navigation, user } = this.props;
    const { wishlist, token } = user;
    const { length } = wishlist;

    console.log("wishlistScreen: ", JSON.stringify(wishlist));

    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />
        <ParentHeaders
          backgroundColor="#014b68"
          leftClickHandler={() => navigation.goBack()}
          LeftIcon="arrow-back"
          title="Wish List"
        />
        <View style={Style.statusBarBackground} />
        <View style={Style.otherThenStatusBarView}>
          {length !== 0 ? (
            <FlatList
              style={Style.FlatList}
              extraData={this.props}
              keyExtractor={this._keyExtractor}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              data={wishlist}
              renderItem={({ item }) => (
                <WishListItems
                  navigation={this.props.navigation}
                  wishlist={this.props.user.wishlist}
                  token={this.props.user.token}
                  RemoveFunc={this.props.AddorRemoveWishList}
                  Product={item.product}
                  Variant={item.variant}
                />
              )}
            />
          ) : (
            <View
              style={{
                flex: 1
              }}
            >
              <View
                style={{
                  marginTop: heightPercentageToDP(15),
                  alignItems: "center"
                }}
              >
                <Lottie
                  ref={animation => {
                    this.animation = animation;
                  }}
                  loop={false}
                  style={{
                    width: widthPercentageToDP(90),
                    height: widthPercentageToDP(60),
                    backgroundColor: "#fff"
                  }}
                  source={EmptyHeart}
                />
              </View>
              <View
                style={{
                  alignItems: "center",
                  marginTop: heightPercentageToDP(7)
                }}
              >
                <Text
                  style={{
                    fontSize: widthPercentageToDP(7),
                    color: "#000",
                    fontWeight: "500",
                    marginTop: 5
                  }}
                >
                  Whoops!
                </Text>
                <Text
                  style={{
                    fontSize: widthPercentageToDP(4),
                    color: "#000",
                    fontWeight: "300",
                    marginTop: 5
                  }}
                >
                  Your WishList is Empty
                </Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  marginTop: heightPercentageToDP(3)
                }}
              >
                <ClickyButton
                  style={{ marginBottom: 5 }}
                  onPress={()=>navigation.navigate("HomeScreen")}
                  text={"Continue Shopping"}
                  
                />
                {/* <Button
                  title="Continue Shopping"
                  titleStyle={{
                    color: "#fff",
                    textAlign: "center",
                    fontSize: 20
                  }}
                  onPress={() => this.props.navigation.navigate("HomeScreen")}
                  buttonStyle={{
                    width: widthPercentageToDP(90),
                    height: heightPercentageToDP(8),
                    backgroundColor: "#000",
                    marginBottom: 5
                  }}
                /> */}
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.USER
});
export default connect(
  mapStateToProps,
  { AddorRemoveWishList }
)(wishlistScreen);
