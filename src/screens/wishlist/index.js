"use strict";
import React from "react";
import WISHLIST from "./authorizedWishlist";
import LOGIN from "../LoginHome";
import PropTypes from "prop-types";
import { connect } from "react-redux";
class ScreenDecider extends React.Component {
  static propTypes = {
    user: PropTypes.object
  };
  Params = this.props.navigation.state.params;
  render() {
    const { user, navigation } = this.props;
    const { isLoggedIn } = user;
    return isLoggedIn ? (
      <WISHLIST navigation={navigation} />
    ) : (
      <LOGIN navigation={navigation} />
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.USER
  };
};
export default connect(mapStateToProps)(ScreenDecider);
