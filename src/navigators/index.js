import { createAppContainer } from "react-navigation";
import DRAWER from "./drawerNavigation";
import navigationService from "./navigationService";
export default createAppContainer(DRAWER);
export { navigationService };
