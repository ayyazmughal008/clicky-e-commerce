import React from "react";
import { Text } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import CategoriesScreen from "../../screens/categories";
import AccountScreen from "../../screens/Account";
import CartScreen from "../../screens/tabCart";
import { AntDesign as ICON } from "@expo/vector-icons";

export default createBottomTabNavigator(
  {
    Categories: {
      path: "/Categories",
      screen: CategoriesScreen,
      navigationOptions: {
        tabBarLabel: ({ focused, tintColor }) => (
          <Text style={{ color: tintColor, alignSelf: "center" }}>
            {"Categories"}
          </Text>
        ),
        tabBarIcon: ({ focused, tintColor }) => (
          <ICON name="minussquareo" color={tintColor} size={25} />
        ),
        tabBarVisible: true,
        header: null
      }
    },
    Cart: {
      path: "/Cart",
      screen: CartScreen,
      navigationOptions: {
        tabBarLabel: ({ focused, tintColor }) => (
          <Text style={{ color: tintColor, alignSelf: "center" }}>
            {"Cart"}
          </Text>
        ),
        tabBarIcon: ({ focused, tintColor }) => (
          <ICON name="shoppingcart" color={tintColor} size={25} />
        ),
        tabBarVisible: true,
        header: null
      }
    },
    Account: {
      path: "/Account",
      screen: AccountScreen,
      navigationOptions: {
        tabBarLabel: ({ focused, tintColor }) => (
          <Text style={{ color: tintColor, alignSelf: "center" }}>
            {"Account"}
          </Text>
        ),
        tabBarIcon: ({ focused, tintColor }) => (
          <ICON name="user" color={tintColor} size={25} />
        ),
        tabBarVisible: true,
        header: null
      }
    }
  },
  { initialRouteName: "Categories", backBehavior: true },
  {
    tabBarOptions: {
      activeTintColor: "#5C4DB5",
      inactiveTintColor: "grey",
      style: {
        backgroundColor: "white",
        borderTopWidth: 0,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: "black",
        shadowOpacity: 0.5,
        elevation: 5
      }
    }
  }
);
