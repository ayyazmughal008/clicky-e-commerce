import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';
//import { DrawerItems } from "react-navigation";
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import { Button } from 'react-native-elements';
import {
  heightPercentageToDP,
  widthPercentageToDP
} from '../../modules/MakeMeResponsive';
import DrawerImage from './drawerImage';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Logout } from '../../redux/action';
import { onRate, onShare } from '../../modules/utilts';
import Constants from 'expo-constants';
class DrawerContent extends Component {
  static propTypes = {
    DrawerContentData: PropTypes.array,
    Logout: PropTypes.func,
    LogedIn: PropTypes.bool
  };

  state = { searchString: '' };

  _keyExtractor = (item, index) => 'MyKey' + index;

  SearchFunc = () => {
    const { searchString } = this.state;
    const { navigate, toggleDrawer } = this.props.navigation;
    if (searchString) {
      toggleDrawer();
      navigate('CategoryPage', { searchString });
      setTimeout(
        () =>
          this.setState({
            searchString: ''
          }),
        700
      );
    } else {
      Alert.alert('Nothing To Search !');
    }
  };

  render() {
    const { props } = this;
    const { DrawerContentData, navigation, Logout, LogedIn } = props;
    const { version } = Constants.manifest;
    return (
      <ScrollView
        style={{ width: '100%', height: heightPercentageToDP(100) }}
        contentContainerStyle={{
          flexGrow: 1,
          backgroundColor: '#fff',
          paddingVertical: heightPercentageToDP(3)
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontWeight: '500',
            padding: 10,
            marginTop: 5
          }}
        >
          Shop by Category
        </Text>
        {/* <View
          style={{
            alignItems: "center",
            flexDirection: "row",
            marginLeft: 15,
            marginTop: 5,
            marginBottom: 20
          }}
        >
          <Icon name="search" color="#000" />
          <TextInput
            style={{ marginLeft: 10 }}
            placeholder="I am looking for"
            placeholderTextColor="#cccccc"
            value={this.state.searchString}
            onChangeText={searchString => this.setState({ searchString })}
            returnKeyType="search"
            onSubmitEditing={this.SearchFunc}
          />
        </View> */}
        {DrawerContentData.map((item, index) => {
          const { Name, ID, CoverImage, ToSearch } = item;
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('CategoryPage', { slug: ToSearch });
              }}
              key={'D' + index}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10
              }}
            >
              <DrawerImage
                ImageWidth={widthPercentageToDP(60)}
                imageHeight={heightPercentageToDP(15)}
                imageSource={{ uri: CoverImage }}
                text={Name}
              />
            </TouchableOpacity>
          );
        })}

        <DrawerNavigatorItems {...props} />

        <Button
          title="Share us"
          titleStyle={{
            color: '#000',
            fontSize: 15
          }}
          onPress={onShare}
          buttonStyle={{
            width: '80%',
            marginLeft: '4%',
            height: heightPercentageToDP(7),
            backgroundColor: '#fff',
            justifyContent: 'flex-start'
          }}
        />
        <Button
          title="Rate us"
          titleStyle={{
            color: '#000',
            fontSize: 15
          }}
          onPress={onRate}
          buttonStyle={{
            width: '80%',
            marginLeft: '4%',
            height: heightPercentageToDP(7),
            backgroundColor: '#fff',
            justifyContent: 'flex-start'
          }}
        />
        {LogedIn && (
          <Button
            title="Logout"
            titleStyle={{
              color: '#000',
              fontSize: 15
            }}
            onPress={() => {
              setTimeout(() => navigation.toggleDrawer(), 700);
              Logout(navigation);
            }}
            buttonStyle={{
              width: '80%',
              marginLeft: '4%',
              height: heightPercentageToDP(7),
              backgroundColor: '#fff',
              justifyContent: 'flex-start'
            }}
          />
        )}
        <View
          style={{
            width: '100%',
            height: heightPercentageToDP(7),
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text
            style={{
              fontSize: 15,
              color: 'grey',
              fontWeight: 'bold'
            }}
          >
            {'Version: ' + version}
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const { MEGAMENU, USER } = state;
  return {
    DrawerContentData: MEGAMENU.DrawerContentData,
    LogedIn: USER.isLoggedIn
  };
};
export default connect(
  mapStateToProps,
  { Logout }
)(DrawerContent);
