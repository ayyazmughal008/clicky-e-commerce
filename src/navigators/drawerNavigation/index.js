import React from "react";
//import { createDrawerNavigator } from "react-navigation";
import { createDrawerNavigator } from 'react-navigation-drawer';
import DrawerFirstSCreen from "../stackNavigation";
import { MaterialIcons as Icon } from "@expo/vector-icons";
import DrawerContent from "./DrawerContent";
import wishListScreen from "../../screens/wishlist";
import OrderListScreen from "../../screens/Orders";
import AccountScreen from "../../screens/Account";
import Cart from "../../screens/Cart";
import { widthPercentageToDP } from "../../modules/MakeMeResponsive";
const IconSize = 6;
export default createDrawerNavigator(
  {
    DrawerFirstScreen: {
      screen: DrawerFirstSCreen,
      navigationOptions: {
        header: null,
        drawerLabel: "Home",
        drawerIcon: <Icon name="home" size={widthPercentageToDP(IconSize)} />
      }
    },
    AccountScreen: {
      screen: AccountScreen,
      navigationOptions: {
        header: null,
        drawerLabel: "Account",
        drawerIcon: (
          <Icon
            name="supervisor-account"
            size={widthPercentageToDP(IconSize)}
          />
        )
      }
    },
    Cart: {
      screen: Cart,
      navigationOptions: {
        header: null,
        drawerLabel: "Cart",
        drawerIcon: (
          <Icon name="shopping-cart" size={widthPercentageToDP(IconSize)} />
        )
      }
    },
    wishListScreen: {
      screen: wishListScreen,
      navigationOptions: {
        header: null,
        drawerLabel: "Wish List",
        drawerIcon: (
          <Icon name="favorite" size={widthPercentageToDP(IconSize)} />
        )
      }
    },
    OrderListScreen: {
      screen: OrderListScreen,
      navigationOptions: {
        header: null,
        drawerLabel: "Orders",
        drawerIcon: (
          <Icon name="local-shipping" size={widthPercentageToDP(IconSize)} />
        )
      }
    }
  },
  {
    drawerWidth: widthPercentageToDP(75),
    drawerPosition: "left",
    initialRouteName: "DrawerFirstScreen",
    drawerType: "slide",
    contentComponent: props => <DrawerContent {...props} />
  }
);
