import React, { Component } from "react";
import { View, ActivityIndicator, Text, Image } from "react-native";

export default class DrawerImage extends Component {
  constructor(props) {
    super(props);
    this.state = { imageStatus: true };
  }

  render() {
    return (
      <View>
        <Image
          style={{
            borderRadius: 15,
            marginRight: this.props.MarginRight,
            width: this.props.ImageWidth,
            height: this.props.imageHeight
          }}
          resizeMode="cover"
          source={this.props.imageSource}
          onLoadStart={() => this.setState({ imageStatus: true })}
          onLoadEnd={() => this.setState({ imageStatus: false })}
        />
        <Text
          style={{
            fontSize: 15,
            fontFamily: "Metropolis_Regular",
            fontWeight: "300",
            padding: 3,
            textAlign: "left"
          }}
        >
          {this.props.text}
        </Text>

        {this.state.imageStatus && (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              opacity: 0.7,
              backgroundColor: "transparent"
            }}
          >
            <ActivityIndicator size="large" color="#fff" />
          </View>
        )}
      </View>
    );
  }
}
