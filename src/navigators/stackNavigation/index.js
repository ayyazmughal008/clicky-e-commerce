//import { createStackNavigator } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import checkOutScreenDecider from '../../screens/ScreenDeciderForCheckout';
import wishListScreen from '../../screens/wishlist';
import PaymentmethodScreen from '../../screens/PaymentMethod';
import OrderSuccessScreen from '../../screens/PaymentSuccess';
import OrderListScreen from '../../screens/Orders';
import OrderDetailScreen from '../../screens/OrderDetail';
import HomeScreen from '../../screens/HomePage';
import CategoryPage from '../../screens/categoryContent';
import ProductPage from '../../screens/ProductPage';
import AccountScreen from '../../screens/Account';
import Freebies from '../../screens/freebiesContent';
import FreebiesDetails from '../../screens/freebieDetail';
import Address from '../../screens/AddressNewDesign';
import NewDesignPaymentMethods from '../../screens/NewDesignPaymentMethods';
import DeliveryMethodConfirmationScreen from '../../screens/DeliveryMethodConfirmation';
import Easypay from '../../screens/EasyPay';
import Cart from '../../screens/Cart';
export default createStackNavigator(
  {
    //==================================================
    HomeScreen: {
      screen: HomeScreen
    },
    ProductPage: {
      screen: ProductPage,
      path: 'product/:id'
    },
    CategoryPage: {
      screen: CategoryPage,
      navigationOptions: {
        header: null
      }
    },
    AccountScreen: {
      screen: AccountScreen
    },
    Cart: {
      screen: Cart
    },
    AddressNewDesign: {
      screen: Address
    },
    NewDesignPaymentMethods: {
      screen: NewDesignPaymentMethods
    },

    //===================================================
    wishList: {
      screen: wishListScreen,
      navigationOptions: {
        header: null
      }
    },
    checkOutScreenDecider: {
      screen: checkOutScreenDecider,
      navigationOptions: {
        header: null
      }
    },
    // checkOut: {
    //   screen: checkOutScreen,
    //   navigationOptions: {
    //     header: null
    //   }
    // },
    address: {
      screen: Address,
      navigationOptions: {
        header: null
      }
    },
    paymentmethod: {
      screen: PaymentmethodScreen,
      navigationOptions: {
        header: null
      }
    },
    orderput: {
      screen: OrderSuccessScreen,
      navigationOptions: {
        header: null
      }
    },

    OrdersListScreen: {
      screen: OrderListScreen,
      navigationOptions: {
        header: null
      }
    },
    OrderDetail: {
      screen: OrderDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    DeliveryMethodConfirmationScreen: {
      screen: DeliveryMethodConfirmationScreen,
      navigationOptions: {
        header: null
      }
    },
    Easypay: {
      screen: Easypay,
      navigationOptions: {
        header: null
      }
    },
    Freebies: {
      screen: Freebies,
      navigationOptions: {
        header: null
      }
    },
    FreebiesDetail: {
      screen: FreebiesDetails,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'HomeScreen',
    headerMode: 'none'
  }
);
